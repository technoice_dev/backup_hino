<?php

class DsoMasterPriceItemController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
//	public function filters()
//	{
//		return array(
//			'accessControl', // perform access control for CRUD operations
//			'postOnly + delete', // we only allow deletion via POST request
//		);
//	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
//	public function accessRules()
//	{
//		return array(
//			array('allow',  // allow all users to perform 'index' and 'view' actions
//				'actions'=>array('index','view'),
//				'users'=>array('@'),
//			),
//			array('allow', // allow authenticated user to perform 'create' and 'update' actions
//				'actions'=>array('create','update','create_upload','updatedata'),
//				'users'=>array('@'),
//			),
//			array('allow', // allow admin user to perform 'admin' and 'delete' actions
//				'actions'=>array('admin','delete'),
//				'users'=>array('@'),
//			),
//			array('deny',  // deny all users
//				'users'=>array('*'),
//			),
//		);
//	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new DsoMasterPriceItem;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['DsoMasterPriceItem']))
		{
			$model->attributes=$_POST['DsoMasterPriceItem'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}
	
	//FUNCTION UPLOAD PRICE FORECAST
public function actionCreate_upload()
	{
		$model=new DsoMasterPriceItem;
		
		if(isset($_POST['DsoMasterPriceItem']))
		{
			$model->attributes=$_POST['DsoMasterPriceItem'];
			
					$model->created_by = Yii::app()->user->id;
					$model->created_date = date('Y-m-d h:i:s');
					$model->modified_by = Yii::app()->user->id;
					$model->modified_date = date('Y-m-d h:i:s');
					
			$file_source = CUploadedFile::getInstance($model, 'file_upload');
			$filename = date('ymd_his').'.xls';
			
			$path = 'uploads/dso_part_price/price'.$filename;
			if(isset($file_source)){
				$file_source->saveAs($path);
				$data = new Spreadsheet_Excel_Reader($path);
				$data->setOutputEncoding('CP1251');
				
				for($j = 1; $j <= $data->sheets[0]['numRows'] - 1; $j++)
				{	
				
				
					$part_code =  str_replace("'","",trim($data->sheets[0]['cells'][$j+1][1]));
					$price =  str_replace("'","",trim($data->sheets[0]['cells'][$j+1][2]));
					$top =  str_replace("'","",trim($data->sheets[0]['cells'][$j+1][3]));
					$disc_code =  str_replace("'","",trim($data->sheets[0]['cells'][$j+1][4]));
					$disc_rate =  str_replace("'","",trim($data->sheets[0]['cells'][$j+1][5]));
					$price_after_disc =  str_replace("'","",trim($data->sheets[0]['cells'][$j+1][6]));
					$period_start =  date("Y-m-d", strtotime(date(trim($data->sheets[0]['cells'][$j+1][7]))));
					$period_end = date("Y-m-d", strtotime(date(trim($data->sheets[0]['cells'][$j+1][8]))));
					
					
					
					$sql_dso = "INSERT INTO dso_master_price_item(id, part_code, price, top,disc_code,disc_rate, price_after_disc,period_start,period_end,created_date,created_by,modified_date,modified_by) VALUES('','" .$part_code . "','" .$price . "','" .$top . "','" .$disc_code . "','" .$disc_rate . "','" .$price_after_disc . "','" .$period_start . "','" .$period_end . "','".$model->created_date."',
										'".$model->created_by."',
										'".$model->modified_date."',
										'".$model->modified_by."')
					ON DUPLICATE KEY UPDATE 
					price= '".$price."', 
					top= '".$top."',
					disc_code='".$disc_code."',
					disc_rate='".$disc_rate."',price_after_disc='".$price_after_disc."',period_end='".$period_end."'";
					
					$exc_update = Yii::app()->db->createCommand($sql_dso);
					$exc_update->execute();
					
					$del_nol = "DELETE FROM dso_master_price_item WHERE part_code is null";
					$exc_del_nol = Yii::app()->db->createCommand($del_nol);
					$exc_del_nol->execute();
					
				}
			}

		$this->redirect(array('index'));
			
		}

		$this->render('create_upload',array(
			'model'=>$model,
			
		));
	}

	public function actionUpdatedata()
	{
		$model=new DsoMasterPriceItem;

		$connection = Yii::app()->db;
		
		if(isset($_POST['DsoMasterPriceItem']))
		{
	
			$model->attributes=$_POST['DsoMasterPriceItem'];
			
			$created_date=$model -> created_date;
			
			if($model->save())
				
			$created_date=$model -> created_date;
			
			$connection = Yii::app()->db;
			$sqlDelete = 'delete from dso_master_price_item where created_date like "%'.$model->created_date.'%";';
			$commandDelete=$connection->createCommand($sqlDelete);
			$commandDelete->execute();
		
			
				$this->redirect(array('index'));
		}

		$this->render('updatedata',array(
			'model'=>$model,
			'dealer'=>$dealer,
		));
	}
	

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['DsoMasterPriceItem']))
		{
			$model->attributes=$_POST['DsoMasterPriceItem'];
			if($model->save())
				$this->redirect(array('index'));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		/*$dataProvider=new CActiveDataProvider('DsoMasterPriceItem');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));*/
		
		$model=new DsoMasterPriceItem('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['DsoMasterPriceItem']))
			$model->attributes=$_GET['DsoMasterPriceItem'];

		$this->render('index',array(
			'model'=>$model,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new DsoMasterPriceItem('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['DsoMasterPriceItem']))
			$model->attributes=$_GET['DsoMasterPriceItem'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return DsoMasterPriceItem the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=DsoMasterPriceItem::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param DsoMasterPriceItem $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='dso-master-price-item-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
