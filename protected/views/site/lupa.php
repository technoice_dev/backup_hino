<?php

$this->pageTitle=Yii::app()->name . ' - Lupa Password';
$this->breadcrumbs=array(
	'Login',
);
?>
<style type="text/css">
    .login-form {
        margin-top: 120px;
        border-style: solid;
    }
</style>

<div class="container">
    <div class="row">
        <div class="panel col-sm-4 col-md-4 col-md-offset-4 login-form text-center">
            <div class="panel-body">
                <h1 class="">Lupa Password</h1>
                <div class="form">
                    <?php $form = $this->beginWidget('CActiveForm', array(
                            'id'=>'login-form',
                            'enableClientValidation'=>true,
                            'clientOptions'=>array(
							'validateOnSubmit'=>true,
                                                    ),
                    )); ?>
                    <div class="form-group row ">

                        <div class="col text-center">
                            <?php echo $form->textField($model,'username', array('class'=>'form-control', 'placeholder'=>'Masukan Email')); ?>
                            <?php echo $form->error($model,'username'); ?>
                        </div>
                    </div>

                    <div class="form-group row buttons">
                        <?php echo CHtml::submitButton('Reset Password', array('class'=>'btn btn-warning btn-login')); ?>
                    </div>
                    <div>
                    <a class="" href="<?php echo Yii::app()->baseUrl; ?>/site/login"><i
                    class="fa fa-cog fa-fw"></i> <?php echo Yii::t('menu', 'Login'); ?>
                    </a>
                    </div>
                </div><!-- form -->
            </div>
        </div>
    </div>
</div>
<?php $this->endWidget(); ?>