<?php
/* @var $this SiteController */

/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle=Yii::app()->name . ' - Login';
$this->breadcrumbs=array(
	'Login',
);
?>
<style type="text/css">
    .login-form {
        margin-top: 120px;
        border-style: solid;
    }
</style>

<div class="container">
    <div class="row">
        <div class="panel col-sm-4 col-md-4 col-md-offset-4 login-form text-center">
            <div class="panel-body">
                <h1 class="">Login</h1>
                <div class="form">
                    <?php $form = $this->beginWidget('CActiveForm', array(
                        				'id'=>'login-form',
                                			'enableClientValidation'=>true,
                                        		'clientOptions'=>array(
							'validateOnSubmit'=>true,
                                                    ),
                    )); ?>
                    <div class="form-group row ">

                        <div class="col text-center">
                            <?php echo $form->textField($model,'username', array('class'=>'form-control', 'placeholder'=>'Username')); ?>
                            <?php echo $form->error($model,'username'); ?>
                        </div>
                    </div>

                    <div class="form-group row">

                        <div class="col">
                            <?php echo $form->passwordField($model,'password', array('class'=>'form-control', 'placeholder'=>'Password')); ?>
                            <?php echo $form->error($model,'password'); ?>
                        </div>
                    </div>
                    <div class="form-group row buttons">
                        <?php echo CHtml::submitButton('Login',	array('class'=>'btn btn-success btn-login')); ?>
                        <!-- <a class="btn btn-link" href="<?php //$this->renderPartial('index', array('model'=>$model)); ?>"> -->
                        <a class="" href="<?php echo Yii::app()->baseUrl; ?>/site/lupa"><i class="fa fa-cog fa-fw"></i> <?php echo Yii::t('menu', 'Lupa Password!'); ?>
                                    
                                </a>
                    </div>
                </div><!-- form -->
            </div>
        </div>
    </div>
</div>
<?php $this->endWidget(); ?>