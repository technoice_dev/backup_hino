<?php
/* @var $this InvestmentController */
/* @var $model Investment */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'investment-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'inv_item'); ?>
		<?php echo $form->textField($model,'inv_item',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'inv_item'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'inv_price'); ?>
		<?php echo $form->textField($model,'inv_price'); ?>
		<?php echo $form->error($model,'inv_price'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'inv_qty'); ?>
		<?php echo $form->textField($model,'inv_qty'); ?>
		<?php echo $form->error($model,'inv_qty'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'inv_category'); ?>
		<?php echo $form->textField($model,'inv_category',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'inv_category'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->