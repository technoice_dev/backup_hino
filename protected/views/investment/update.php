<?php
/* @var $this InvestmentController */
/* @var $model Investment */

$this->breadcrumbs=array(
	'Investments'=>array('index'),
	$model->inv_id=>array('view','id'=>$model->inv_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Investment', 'url'=>array('index')),
	array('label'=>'Create Investment', 'url'=>array('create')),
	array('label'=>'View Investment', 'url'=>array('view', 'id'=>$model->inv_id)),
	array('label'=>'Manage Investment', 'url'=>array('admin')),
);
?>

<h1>Update Investment <?php echo $model->inv_id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>