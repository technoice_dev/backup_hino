<?php
/* @var $this CustomerController */
/* @var $data Customer */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_cust')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_cust), array('view', 'id'=>$data->id_cust)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cust_name')); ?>:</b>
	<?php echo CHtml::encode($data->cust_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('addres')); ?>:</b>
	<?php echo CHtml::encode($data->addres); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('phone')); ?>:</b>
	<?php echo CHtml::encode($data->phone); ?>
	<br />


</div>