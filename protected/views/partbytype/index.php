<?php
/* @var $this PartbytypeController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Partbytypes',
);

$this->menu=array(
	array('label'=>'Create Partbytype', 'url'=>array('create')),
	array('label'=>'Manage Partbytype', 'url'=>array('admin')),
);
?>

<h1>Partbytypes</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
