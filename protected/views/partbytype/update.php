<?php
/* @var $this PartbytypeController */
/* @var $model Partbytype */

$this->breadcrumbs=array(
	'Partbytypes'=>array('index'),
	$model->typemtc_id=>array('view','id'=>$model->typemtc_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Partbytype', 'url'=>array('index')),
	array('label'=>'Create Partbytype', 'url'=>array('create')),
	array('label'=>'View Partbytype', 'url'=>array('view', 'id'=>$model->typemtc_id)),
	array('label'=>'Manage Partbytype', 'url'=>array('admin')),
);
?>

<h1>Update Partbytype <?php echo $model->typemtc_id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>