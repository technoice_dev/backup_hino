<?php
/* @var $this MasterDealerController */
/* @var $model MasterDealer */

$this->breadcrumbs=array(
	'Master Dealers'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List MasterDealer', 'url'=>array('index')),
	array('label'=>'Create MasterDealer', 'url'=>array('create')),
	array('label'=>'View MasterDealer', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage MasterDealer', 'url'=>array('admin')),
);
?>

<h1>Update MasterDealer <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>