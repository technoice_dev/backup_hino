<?php
/* @var $this MasterDealerController */
/* @var $model MasterDealer */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'master-dealer-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'dealer_id'); ?>
		<?php echo $form->textField($model,'dealer_id',array('size'=>9,'maxlength'=>9)); ?>
		<?php echo $form->error($model,'dealer_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'dealer_name'); ?>
		<?php echo $form->textField($model,'dealer_name',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'dealer_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'cabang_id'); ?>
		<?php echo $form->textField($model,'cabang_id'); ?>
		<?php echo $form->error($model,'cabang_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'facility'); ?>
		<?php echo $form->textField($model,'facility',array('size'=>25,'maxlength'=>25)); ?>
		<?php echo $form->error($model,'facility'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'dealer_type'); ?>
		<?php echo $form->textField($model,'dealer_type',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'dealer_type'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'serial_code'); ?>
		<?php echo $form->textField($model,'serial_code'); ?>
		<?php echo $form->error($model,'serial_code'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'main_dealer'); ?>
		<?php echo $form->textField($model,'main_dealer',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'main_dealer'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'region_id'); ?>
		<?php echo $form->textField($model,'region_id',array('size'=>1,'maxlength'=>1)); ?>
		<?php echo $form->error($model,'region_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'address'); ?>
		<?php echo $form->textField($model,'address',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'address'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'no_phone'); ?>
		<?php echo $form->textField($model,'no_phone',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'no_phone'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'fax'); ?>
		<?php echo $form->textField($model,'fax',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'fax'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'establishment_date'); ?>
		<?php echo $form->textField($model,'establishment_date'); ?>
		<?php echo $form->error($model,'establishment_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'outlet_name'); ?>
		<?php echo $form->textField($model,'outlet_name',array('size'=>25,'maxlength'=>25)); ?>
		<?php echo $form->error($model,'outlet_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'sales_prefix'); ?>
		<?php echo $form->textField($model,'sales_prefix',array('size'=>5,'maxlength'=>5)); ?>
		<?php echo $form->error($model,'sales_prefix'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'website'); ?>
		<?php echo $form->textField($model,'website',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'website'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'region_id_hcs'); ?>
		<?php echo $form->textField($model,'region_id_hcs'); ?>
		<?php echo $form->error($model,'region_id_hcs'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->