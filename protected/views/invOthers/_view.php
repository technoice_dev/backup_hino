<?php
/* @var $this InvOthersController */
/* @var $data InvOthers */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('inv_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->inv_id), array('view', 'id'=>$data->inv_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('inv_item')); ?>:</b>
	<?php echo CHtml::encode($data->inv_item); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('inv_price')); ?>:</b>
	<?php echo CHtml::encode($data->inv_price); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('inv_qty')); ?>:</b>
	<?php echo CHtml::encode($data->inv_qty); ?>
	<br />


</div>