<?php
/* @var $this InvOthersController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Inv Others',
);

$this->menu=array(
	array('label'=>'Create InvOthers', 'url'=>array('create')),
	array('label'=>'Manage InvOthers', 'url'=>array('admin')),
);
?>

<h1>Inv Others</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
