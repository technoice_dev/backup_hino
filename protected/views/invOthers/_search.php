<?php
/* @var $this InvOthersController */
/* @var $model InvOthers */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'inv_id'); ?>
		<?php echo $form->textField($model,'inv_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'inv_item'); ?>
		<?php echo $form->textField($model,'inv_item',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'inv_price'); ?>
		<?php echo $form->textField($model,'inv_price'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'inv_qty'); ?>
		<?php echo $form->textField($model,'inv_qty'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->