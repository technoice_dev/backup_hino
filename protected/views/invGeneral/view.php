<?php
/* @var $this InvGeneralController */
/* @var $model InvGeneral */

$this->breadcrumbs=array(
	'Inv Generals'=>array('index'),
	$model->inv_id,
);

$this->menu=array(
	array('label'=>'List InvGeneral', 'url'=>array('index')),
	array('label'=>'Create InvGeneral', 'url'=>array('create')),
	array('label'=>'Update InvGeneral', 'url'=>array('update', 'id'=>$model->inv_id)),
	array('label'=>'Delete InvGeneral', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->inv_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage InvGeneral', 'url'=>array('admin')),
);
?>

<h1>View InvGeneral #<?php echo $model->inv_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'inv_id',
		'inv_item',
		'inv_price',
		'inv_qty',
	),
)); ?>
