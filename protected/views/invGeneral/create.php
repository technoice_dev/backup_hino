<?php
/* @var $this InvGeneralController */
/* @var $model InvGeneral */

$this->breadcrumbs=array(
	'Inv Generals'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List InvGeneral', 'url'=>array('index')),
	array('label'=>'Manage InvGeneral', 'url'=>array('admin')),
);
?>

<h1>Create InvGeneral</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>