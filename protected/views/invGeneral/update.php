<?php
/* @var $this InvGeneralController */
/* @var $model InvGeneral */

$this->breadcrumbs=array(
	'Inv Generals'=>array('index'),
	$model->inv_id=>array('view','id'=>$model->inv_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List InvGeneral', 'url'=>array('index')),
	array('label'=>'Create InvGeneral', 'url'=>array('create')),
	array('label'=>'View InvGeneral', 'url'=>array('view', 'id'=>$model->inv_id)),
	array('label'=>'Manage InvGeneral', 'url'=>array('admin')),
);
?>

<h1>Update InvGeneral <?php echo $model->inv_id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>