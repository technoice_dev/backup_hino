<?php
/* @var $this InvGeneralController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Inv Generals',
);

$this->menu=array(
	array('label'=>'Create InvGeneral', 'url'=>array('create')),
	array('label'=>'Manage InvGeneral', 'url'=>array('admin')),
);
?>

<h1>Inv Generals</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
