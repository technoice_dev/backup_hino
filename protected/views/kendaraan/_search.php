<?php
/* @var $this KendaraanController */
/* @var $model Kendaraan */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'no_kendaraan'); ?>
		<?php echo $form->textField($model,'no_kendaraan'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tipe_kendaraan'); ?>
		<?php echo $form->textField($model,'tipe_kendaraan',array('size'=>60,'maxlength'=>150)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'km_kendaraan'); ?>
		<?php echo $form->textField($model,'km_kendaraan',array('size'=>60,'maxlength'=>150)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->