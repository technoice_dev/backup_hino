<?php
/* @var $this KonsumenController */
/* @var $model Konsumen */

$this->breadcrumbs=array(
	'Konsumens'=>array('index'),
	$model->id_konsumen,
);

$this->menu=array(
	array('label'=>'List Konsumen', 'url'=>array('index')),
	array('label'=>'Create Konsumen', 'url'=>array('create')),
	array('label'=>'Update Konsumen', 'url'=>array('update', 'id'=>$model->id_konsumen)),
	array('label'=>'Delete Konsumen', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_konsumen),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Konsumen', 'url'=>array('admin')),
);
?>

<h1>View Konsumen #<?php echo $model->id_konsumen; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_konsumen',
		'nama',
		'no_telp',
		'alamat',
	),
)); ?>
