<?php
/* @var $this KonsumenController */
/* @var $model Konsumen */

$this->breadcrumbs=array(
	'Konsumens'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Konsumen', 'url'=>array('index')),
	array('label'=>'Manage Konsumen', 'url'=>array('admin')),
);
?>

<h1>Create Konsumen</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>