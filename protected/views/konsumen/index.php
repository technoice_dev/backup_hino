<?php
/* @var $this KonsumenController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Konsumens',
);

$this->menu=array(
	array('label'=>'Create Konsumen', 'url'=>array('create')),
	array('label'=>'Manage Konsumen', 'url'=>array('admin')),
);
?>

<h1>Konsumens</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
