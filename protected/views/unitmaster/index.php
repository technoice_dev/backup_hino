<?php
/* @var $this UnitmasterController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Unitmasters',
);

$this->menu=array(
	array('label'=>'Create Unitmaster', 'url'=>array('create')),
	array('label'=>'Manage Unitmaster', 'url'=>array('admin')),
);
?>

<h1>Unitmasters</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
