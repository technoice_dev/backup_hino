<?php
/* @var $this UnitmasterController */
/* @var $model Unitmaster */

$this->breadcrumbs=array(
	'Unitmasters'=>array('index'),
	$model->unit_id=>array('view','id'=>$model->unit_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Unitmaster', 'url'=>array('index')),
	array('label'=>'Create Unitmaster', 'url'=>array('create')),
	array('label'=>'View Unitmaster', 'url'=>array('view', 'id'=>$model->unit_id)),
	array('label'=>'Manage Unitmaster', 'url'=>array('admin')),
);
?>

<h1>Update Unitmaster <?php echo $model->unit_id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>