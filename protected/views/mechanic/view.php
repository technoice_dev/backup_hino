<?php
/* @var $this MechanicController */
/* @var $model Mechanic */

$this->breadcrumbs=array(
	'Mechanics'=>array('index'),
	$model->nik,
);

$this->menu=array(
	array('label'=>'List Mechanic', 'url'=>array('index')),
	array('label'=>'Create Mechanic', 'url'=>array('create')),
	array('label'=>'Update Mechanic', 'url'=>array('update', 'id'=>$model->nik)),
	array('label'=>'Delete Mechanic', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->nik),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Mechanic', 'url'=>array('admin')),
);
?>

<h1>View Mechanic #<?php echo $model->nik; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'nik',
		'nama',
		'alamat',
		'no_telp',
		'jabatan',
	),
)); ?>
