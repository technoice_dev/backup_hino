<?php
/* @var $this ManpowerController */
/* @var $data Manpower */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('mp_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->mp_id), array('view', 'id'=>$data->mp_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bsc_sallary')); ?>:</b>
	<?php echo CHtml::encode($data->bsc_sallary); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pph')); ?>:</b>
	<?php echo CHtml::encode($data->pph); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('laundry')); ?>:</b>
	<?php echo CHtml::encode($data->laundry); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('transport')); ?>:</b>
	<?php echo CHtml::encode($data->transport); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('meal')); ?>:</b>
	<?php echo CHtml::encode($data->meal); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('medical')); ?>:</b>
	<?php echo CHtml::encode($data->medical); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('mobile_phone')); ?>:</b>
	<?php echo CHtml::encode($data->mobile_phone); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bpjs')); ?>:</b>
	<?php echo CHtml::encode($data->bpjs); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('thr')); ?>:</b>
	<?php echo CHtml::encode($data->thr); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('uni_safety')); ?>:</b>
	<?php echo CHtml::encode($data->uni_safety); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hardship')); ?>:</b>
	<?php echo CHtml::encode($data->hardship); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('overtime')); ?>:</b>
	<?php echo CHtml::encode($data->overtime); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kategory')); ?>:</b>
	<?php echo CHtml::encode($data->kategory); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_dealer')); ?>:</b>
	<?php echo CHtml::encode($data->id_dealer); ?>
	<br />

	*/ ?>

</div>