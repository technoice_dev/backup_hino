<?php
/* @var $this ManpowerController */
/* @var $model Manpower */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'manpower-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'bsc_sallary'); ?>
		<?php echo $form->textField($model,'bsc_sallary'); ?>
		<?php echo $form->error($model,'bsc_sallary'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'pph'); ?>
		<?php echo $form->textField($model,'pph'); ?>
		<?php echo $form->error($model,'pph'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'laundry'); ?>
		<?php echo $form->textField($model,'laundry'); ?>
		<?php echo $form->error($model,'laundry'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'transport'); ?>
		<?php echo $form->textField($model,'transport'); ?>
		<?php echo $form->error($model,'transport'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'meal'); ?>
		<?php echo $form->textField($model,'meal'); ?>
		<?php echo $form->error($model,'meal'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'medical'); ?>
		<?php echo $form->textField($model,'medical'); ?>
		<?php echo $form->error($model,'medical'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'mobile_phone'); ?>
		<?php echo $form->textField($model,'mobile_phone'); ?>
		<?php echo $form->error($model,'mobile_phone'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'bpjs'); ?>
		<?php echo $form->textField($model,'bpjs'); ?>
		<?php echo $form->error($model,'bpjs'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'thr'); ?>
		<?php echo $form->textField($model,'thr'); ?>
		<?php echo $form->error($model,'thr'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'uni_safety'); ?>
		<?php echo $form->textField($model,'uni_safety'); ?>
		<?php echo $form->error($model,'uni_safety'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'hardship'); ?>
		<?php echo $form->textField($model,'hardship'); ?>
		<?php echo $form->error($model,'hardship'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'overtime'); ?>
		<?php echo $form->textField($model,'overtime'); ?>
		<?php echo $form->error($model,'overtime'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'kategory'); ?>
		<?php echo $form->textField($model,'kategory',array('size'=>6,'maxlength'=>6)); ?>
		<?php echo $form->error($model,'kategory'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'id_dealer'); ?>
		<?php echo $form->textField($model,'id_dealer'); ?>
		<?php echo $form->error($model,'id_dealer'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->