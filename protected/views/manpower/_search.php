<?php
/* @var $this ManpowerController */
/* @var $model Manpower */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'mp_id'); ?>
		<?php echo $form->textField($model,'mp_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'bsc_sallary'); ?>
		<?php echo $form->textField($model,'bsc_sallary'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'pph'); ?>
		<?php echo $form->textField($model,'pph'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'laundry'); ?>
		<?php echo $form->textField($model,'laundry'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'transport'); ?>
		<?php echo $form->textField($model,'transport'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'meal'); ?>
		<?php echo $form->textField($model,'meal'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'medical'); ?>
		<?php echo $form->textField($model,'medical'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'mobile_phone'); ?>
		<?php echo $form->textField($model,'mobile_phone'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'bpjs'); ?>
		<?php echo $form->textField($model,'bpjs'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'thr'); ?>
		<?php echo $form->textField($model,'thr'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'uni_safety'); ?>
		<?php echo $form->textField($model,'uni_safety'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'hardship'); ?>
		<?php echo $form->textField($model,'hardship'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'overtime'); ?>
		<?php echo $form->textField($model,'overtime'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'kategory'); ?>
		<?php echo $form->textField($model,'kategory',array('size'=>6,'maxlength'=>6)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'id_dealer'); ?>
		<?php echo $form->textField($model,'id_dealer'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->