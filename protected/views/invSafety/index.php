<?php
/* @var $this InvSafetyController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Inv Safeties',
);

$this->menu=array(
	array('label'=>'Create InvSafety', 'url'=>array('create')),
	array('label'=>'Manage InvSafety', 'url'=>array('admin')),
);
?>

<h1>Inv Safeties</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
