<?php
/* @var $this InvPowerController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Inv Powers',
);

$this->menu=array(
	array('label'=>'Create InvPower', 'url'=>array('create')),
	array('label'=>'Manage InvPower', 'url'=>array('admin')),
);
?>

<h1>Inv Powers</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
