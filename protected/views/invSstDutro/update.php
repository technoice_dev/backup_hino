<?php
/* @var $this InvSstDutroController */
/* @var $model InvSstDutro */

$this->breadcrumbs=array(
	'Inv Sst Dutros'=>array('index'),
	$model->inv_id=>array('view','id'=>$model->inv_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List InvSstDutro', 'url'=>array('index')),
	array('label'=>'Create InvSstDutro', 'url'=>array('create')),
	array('label'=>'View InvSstDutro', 'url'=>array('view', 'id'=>$model->inv_id)),
	array('label'=>'Manage InvSstDutro', 'url'=>array('admin')),
);
?>

<h1>Update InvSstDutro <?php echo $model->inv_id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>