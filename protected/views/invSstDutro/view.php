<?php
/* @var $this InvSstDutroController */
/* @var $model InvSstDutro */

$this->breadcrumbs=array(
	'Inv Sst Dutros'=>array('index'),
	$model->inv_id,
);

$this->menu=array(
	array('label'=>'List InvSstDutro', 'url'=>array('index')),
	array('label'=>'Create InvSstDutro', 'url'=>array('create')),
	array('label'=>'Update InvSstDutro', 'url'=>array('update', 'id'=>$model->inv_id)),
	array('label'=>'Delete InvSstDutro', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->inv_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage InvSstDutro', 'url'=>array('admin')),
);
?>

<h1>View InvSstDutro #<?php echo $model->inv_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'inv_id',
		'inv_item',
		'inv_price',
		'inv_qty',
	),
)); ?>
