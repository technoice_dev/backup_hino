<?php
/* @var $this InvSstDutroController */
/* @var $model InvSstDutro */

$this->breadcrumbs=array(
	'Inv Sst Dutros'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List InvSstDutro', 'url'=>array('index')),
	array('label'=>'Manage InvSstDutro', 'url'=>array('admin')),
);
?>

<h1>Create InvSstDutro</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>