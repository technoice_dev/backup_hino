<?php
/* @var $this DsoMasterPriceItemController */
/* @var $model DsoMasterPriceItem */

$this->breadcrumbs=array(
	'Dso Master Price Items'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List DsoMasterPriceItem', 'url'=>array('index')),
	array('label'=>'Create DsoMasterPriceItem', 'url'=>array('create')),
	array('label'=>'Update DsoMasterPriceItem', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete DsoMasterPriceItem', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage DsoMasterPriceItem', 'url'=>array('admin')),
);
?>

<h1>View DsoMasterPriceItem #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'part_code',
		'price',
		'top',
		'disc_code',
		'price_after_disc',
		'period_start',
		'period_end',
		'created_by',
		'created_date',
		'modified_by',
		'modified_date',
	),
)); ?>
