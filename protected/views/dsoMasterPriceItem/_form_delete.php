<?php
/* @var $this DsoPartUploadController */
/* @var $model DsoPartUpload */
/* @var $form CActiveForm */
?>
<style>
<!--#customername{display:none;}-->
.customername {
    display:none;
}
</style>
<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'dso-part-upload-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'htmlOptions'=> array('enctype'=>'multipart/form-data'),
)); ?>

	<!--<p class="note">Fields with <span class="required">*</span> are required.</p>-->

	<?php echo $form->errorSummary($model); ?>

	<div class="panel panel-success">
		<div class="panel-body">
		
			
			<input type="hidden" id="urlimport" value="<?php echo Yii::app()->createUrl('/DsoPartUpload/partupload', array("id"=>$id)); ?>">
			<div class="panel panel-default">
				<div class="panel-body">
					<table border="0" align="center" class="table-responsive">
					<p>Notes : Data will be deleted based on Upload Data Date which input below</p>
						<!-- <tr>
							<td><?php echo $form->labelEx($model,'created_date', array('class'=>'created_date', 'id'=>'created_date')); ?></td>
							<td>
								<?php echo $form->textField($model,'created_date',array('maxlength'=>200,'class'=>'form-control created_date', 'id'=>'created_date')); ?>
							</td>
						</tr>-->
						
						<tr>
							<td><?php echo $form->labelEx($model,'Upload Date'); ?></td>
							<td>
							<?php
								$this->widget('zii.widgets.jui.CJuiDatePicker', array(
									'model' => $model,
									//'value' => $model->sppd_id,
									'attribute' => 'created_date',
									'id' => 'created_date',
									'language' => 'en',
									'options' => array(
										//'dateFormat' => 'yy/mm/dd', --the real one
										'dateFormat' => 'yy-mm-dd',
										'showButtonPanel'=>true,
										'changeYear' => true,           // can change year
										'changeMonth' => true,          // can change month
										//'minDate'=>0,
										//'maxDate'=>"+1M +5D",
									),
									'htmlOptions' => array(
										'class' => 'form-control input-sm',
										'tabindex' => 2,
										'style'=>'width:350px'
									),
								));
							?>
							</td>
						</tr>
						
						<tr>
							<td></td>
							<td><?php echo CHtml::submitButton('Delete', array('class' => 'btn btn-primary btn-sm')); ?></td>
							
						</tr>
					</table>
				</div>
			</div>
			
			<br/><br/>
			
			
		</div>
	</div>

	<!--<div class="row buttons">
		<?php //echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>-->

<?php $this->endWidget(); ?>

</div><!-- form -->


<script>
function confirmationrevision()
{
	var answer = confirm("Are you sure for delete ?");
	if (answer)
	{
		//document.getElementById("dso-part-upload-form").action=document.getElementById('urlimport').value;
		document.getElementById("dso-part-upload-form").submit();
		return true;
	} else {
		if (window.event) // True with IE, false with other browsers
		{
			window.event.returnValue=false; //IE specific
		} else {
			return false
		}
	}
}

/*$('td select[name=uploadtype]').change(function(e){
  if ($('td select[name=uploadtype]').val() == 'PPC'){
    $('#customername').show();
  }else{
    $('#customername').hide();
  }
});*/



$("select").change(function () {
    $("select option:selected").each(function () {
        var uploadtype = $(".uploadtype"), customername = $(".customername");
        if ($(this).val() == "PPC") {
            customername.fadeIn();
            
        } else if($(this).val() == "DSO") {
			customername().fadeout();
		}
		else {
            customername.hide()
        }
    });
});

$('#deletedata').click(function() {
        
        var response_text = $("#response_text").val();
           
        if (confirm('Are you sure ?')) {
            deletedatax();
        }
    });
	
function deletedatax()
    {
     
          var created_date = '<?php echo $model->created_date; ?>'; 
          var baseUrl = '<?php echo Yii::app()->request->baseUrl; ?>/index.php?r=dsopartupload/deletedatax';

          $.ajax({
               type: 'POST',
               dataType:'html',
               url: baseUrl,
               data:'created_date='+created_date+',
               beforeSend: function(data)
                {
                    $('#loading').show();
                    
                },
               success:function(data){
                    $('#loading').hide();
                    
                    	alert("Success Delete");
                    	
                    	window.location = '<?php echo Yii::app()->request->baseUrl; ?>/index.php ';

               },
               error: function(data) { //IF ERROR OCCURED
                     alert("Error occured.please try again");
                     //alert(data);
                },
             
          });
     
    }

</script>