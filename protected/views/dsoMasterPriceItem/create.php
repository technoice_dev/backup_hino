<?php
/* @var $this DsoMasterPriceItemController */
/* @var $model DsoMasterPriceItem */

$this->breadcrumbs=array(
	'Dso Master Price Items'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List DsoMasterPriceItem', 'url'=>array('index')),
	array('label'=>'Manage DsoMasterPriceItem', 'url'=>array('admin')),
);
?>

<h1>Create Price Part</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>