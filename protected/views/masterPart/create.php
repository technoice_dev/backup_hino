<?php
/* @var $this MasterPartController */
/* @var $model MasterPart */

$this->breadcrumbs=array(
	'Master Parts'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List MasterPart', 'url'=>array('index')),
	array('label'=>'Manage MasterPart', 'url'=>array('admin')),
);
?>

<h1>Create MasterPart</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>