<?php
/* @var $this MasterPartController */
/* @var $model MasterPart */

$this->breadcrumbs=array(
	'Master Parts'=>array('index'),
	$model->part_id,
);

$this->menu=array(
	array('label'=>'List MasterPart', 'url'=>array('index')),
	array('label'=>'Create MasterPart', 'url'=>array('create')),
	array('label'=>'Update MasterPart', 'url'=>array('update', 'id'=>$model->part_id)),
	array('label'=>'Delete MasterPart', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->part_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage MasterPart', 'url'=>array('admin')),
);
?>

<h1>View MasterPart #<?php echo $model->part_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'part_id',
		'part_name',
		'part_no',
		'pricelist',
		'km1th',
	),
)); ?>
