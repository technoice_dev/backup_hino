<?php
/* @var $this MasterPartController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Master Parts',
);

$this->menu=array(
	array('label'=>'Create MasterPart', 'url'=>array('create')),
	array('label'=>'Manage MasterPart', 'url'=>array('admin')),
);
?>

<h1>Master Parts</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
