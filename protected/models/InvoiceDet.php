<?php

/**
 * This is the model class for table "invoice_det".
 *
 * The followings are the available columns in table 'invoice_det':
 * @property integer $id
 * @property integer $id_invoice
 * @property integer $unit_id
 * @property integer $kontrak
 * @property integer $km_day
 * @property integer $discount
 * @property integer $preventive_cost
 * @property integer $periodic_cost
 * @property integer $labor_cost
 * @property integer $invest_cost
 * @property integer $jml_senior
 * @property integer $jml_junior
 * @property integer $work_day
 * @property integer $unit_qty
 */
class InvoiceDet extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'invoice_det';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id, id_invoice, unit_id, kontrak, km_day, discount, preventive_cost, periodic_cost, labor_cost, invest_cost, jml_senior, jml_junior, work_day, unit_qty', 'required'),
			array('id, id_invoice, unit_id, kontrak, km_day, discount, preventive_cost, periodic_cost, labor_cost, invest_cost, jml_senior, jml_junior, work_day, unit_qty', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_invoice, unit_id, kontrak, km_day, discount, preventive_cost, periodic_cost, labor_cost, invest_cost, jml_senior, jml_junior, work_day, unit_qty', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_invoice' => 'Id Invoice',
			'unit_id' => 'Unit',
			'kontrak' => 'Kontrak',
			'km_day' => 'Km Day',
			'discount' => 'Discount',
			'preventive_cost' => 'Preventive Cost',
			'periodic_cost' => 'Periodic Cost',
			'labor_cost' => 'Labor Cost',
			'invest_cost' => 'Invest Cost',
			'jml_senior' => 'Jml Senior',
			'jml_junior' => 'Jml Junior',
			'work_day' => 'Work Day',
			'unit_qty' => 'Unit Qty',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_invoice',$this->id_invoice);
		$criteria->compare('unit_id',$this->unit_id);
		$criteria->compare('kontrak',$this->kontrak);
		$criteria->compare('km_day',$this->km_day);
		$criteria->compare('discount',$this->discount);
		$criteria->compare('preventive_cost',$this->preventive_cost);
		$criteria->compare('periodic_cost',$this->periodic_cost);
		$criteria->compare('labor_cost',$this->labor_cost);
		$criteria->compare('invest_cost',$this->invest_cost);
		$criteria->compare('jml_senior',$this->jml_senior);
		$criteria->compare('jml_junior',$this->jml_junior);
		$criteria->compare('work_day',$this->work_day);
		$criteria->compare('unit_qty',$this->unit_qty);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return InvoiceDet the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
