<?php

/**
 * This is the model class for table "master_dealer".
 *
 * The followings are the available columns in table 'master_dealer':
 * @property integer $id
 * @property string $dealer_id
 * @property string $dealer_name
 * @property integer $cabang_id
 * @property string $facility
 * @property string $dealer_type
 * @property integer $serial_code
 * @property string $main_dealer
 * @property string $region_id
 * @property string $address
 * @property string $no_phone
 * @property string $fax
 * @property string $establishment_date
 * @property string $outlet_name
 * @property string $sales_prefix
 * @property string $website
 * @property integer $region_id_hcs
 */
class MasterDealer extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'master_dealer';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('dealer_id, dealer_name, cabang_id, facility, dealer_type, serial_code, main_dealer, region_id, address, no_phone, fax, establishment_date, outlet_name, sales_prefix, website', 'required'),
			array('cabang_id, serial_code, region_id_hcs', 'numerical', 'integerOnly'=>true),
			array('dealer_id', 'length', 'max'=>9),
			array('dealer_name', 'length', 'max'=>255),
			array('facility, outlet_name', 'length', 'max'=>25),
			array('dealer_type, main_dealer, no_phone, fax', 'length', 'max'=>20),
			array('region_id', 'length', 'max'=>1),
			array('address', 'length', 'max'=>100),
			array('sales_prefix', 'length', 'max'=>5),
			array('website', 'length', 'max'=>50),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, dealer_id, dealer_name, cabang_id, facility, dealer_type, serial_code, main_dealer, region_id, address, no_phone, fax, establishment_date, outlet_name, sales_prefix, website, region_id_hcs', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'dealer_id' => 'Dealer',
			'dealer_name' => 'Dealer Name',
			'cabang_id' => 'Cabang',
			'facility' => 'Facility',
			'dealer_type' => 'Dealer Type',
			'serial_code' => 'Serial Code',
			'main_dealer' => 'Main Dealer',
			'region_id' => 'Region',
			'address' => 'Address',
			'no_phone' => 'No Phone',
			'fax' => 'Fax',
			'establishment_date' => 'Establishment Date',
			'outlet_name' => 'Outlet Name',
			'sales_prefix' => 'Sales Prefix',
			'website' => 'Website',
			'region_id_hcs' => 'Region Id Hcs',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('dealer_id',$this->dealer_id,true);
		$criteria->compare('dealer_name',$this->dealer_name,true);
		$criteria->compare('cabang_id',$this->cabang_id);
		$criteria->compare('facility',$this->facility,true);
		$criteria->compare('dealer_type',$this->dealer_type,true);
		$criteria->compare('serial_code',$this->serial_code);
		$criteria->compare('main_dealer',$this->main_dealer,true);
		$criteria->compare('region_id',$this->region_id,true);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('no_phone',$this->no_phone,true);
		$criteria->compare('fax',$this->fax,true);
		$criteria->compare('establishment_date',$this->establishment_date,true);
		$criteria->compare('outlet_name',$this->outlet_name,true);
		$criteria->compare('sales_prefix',$this->sales_prefix,true);
		$criteria->compare('website',$this->website,true);
		$criteria->compare('region_id_hcs',$this->region_id_hcs);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return MasterDealer the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
