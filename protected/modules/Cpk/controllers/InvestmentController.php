<?php

class InvestmentController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	//Function Untuk Mengaktifkan Access Control
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations

		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	//Hak akses untuk controller investment
	public function accessRules()
	{
            $level = isset(Yii::app()->user->level) ? Yii::app()->user->level : 'c';
		return array(
//			array('allow',  // allow all users to perform 'index' and 'view' actions
//				'actions'=>array('index','view'),
//				'users'=>array('*'),
//			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','index','delete'),
				'expression'=>'"'.$level.'" =="dealer" || "'.$level.'" =="sales"',
			),
                        array('deny',  // deny all users
				'expression'=>'Yii::app()->user->isGuest ',
			),
			array('deny',  // deny all users
				'expression'=> '"'.$level.'" == "super" ',
			),
		);
	}
	//Hak akses untuk controller investment
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	//Function Untuk Menampilkan detail Investment
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}
	//Function Untuk Menampilkan detail Investment
	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */

	//Function Create Investment
	public function actionCreate()
	{
		$model=new Investment;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		//Ketika Dibuka menggunakan method GET maka akan menampilkan form create Investment
		//Ketika Mengirim Request Menggunakan POST maka akan masuk ke insert Investment
		if(isset($_POST['Investment']))
		{
			$model->attributes=$_POST['Investment'];
			$real_price = str_replace(',','',$model->inv_price);
			$model->inv_price = $real_price;
			$model->id_dealer = Yii::app()->user->dealer;
			if($model->save())
				//Ketika Data Lolos Validasi Model investment Maka data akan dimasukan ke database dan ,akan redirect ke detail investment
                Yii::app()->user->setFlash('success', "Data berhasil disimpan");
				$this->redirect(array('view','id'=>$model->inv_id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		
		//Ketika Dibuka menggunakan method GET maka akan menampilkan form create Investment
		//Ketika Mengirim Request Menggunakan POST maka akan masuk ke update Investment
		if(isset($_POST['Investment']))
		{
			
			$model->attributes=$_POST['Investment'];
			$real_price = str_replace(',','',$model->inv_price);
			$model->inv_price = $real_price;
			$model->id_dealer = Yii::app()->user->dealer;
			if($model->save())
								//Ketika Data Lolos Validasi Model investment Maka data akan dimasukan ke database dan ,akan redirect ke detail investment
                                Yii::app()->user->setFlash('success', "Data berhasil diubah");
				$this->redirect(array('view','id'=>$model->inv_id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	//Function Delete Investment
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();
		//memanggil function load model untuk mencari data part berdasarkan id dan menghapus data part;
		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
                        Yii::app()->user->setFlash('success', "Data berhasil dihapus");
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
			$criteria = new CDbCriteria();
			//inisialisasi criteria database
			$search = Yii::app()->request->getQuery('search') ? Yii::app()->request->getQuery('search') : null;
			//cek jika ada filter search
			$sortBy = Yii::app()->request->getQuery('sortBy') ? Yii::app()->request->getQuery('sortBy') : null;
			//cek jika ada sorting data berdasarkan kolom
			$sortType = Yii::app()->request->getQuery('sortType') ? Yii::app()->request->getQuery('sortType') : 'asc';
			//cek tipe sorting data antara ascending atau descending
            
            if($search){
				//jika ada filter search maka data akan di cari di semua kolom pada tabel investment
                $criteria->addCondition('inv_id like "%'.$search.'%" ','OR');
                $criteria->addCondition('inv_item like "%'.urldecode($search).'%" ','OR');
                $criteria->addCondition('inv_price like "%'.$search.'%" ','OR');
                $criteria->addCondition('inv_qty like "%'.$search.'%"','OR');
                $criteria->addCondition('inv_category like "%'.urldecode($search).'%"','OR');
			}
			$criteria->addCondition('id_dealer = '.Yii::app()->user->dealer);
			//Mencari data berdasarkan dealer yang sedang login
            if($sortBy !== null){
				//jika ada sorting data maka akan menambahkan ordering data
                $criteria->order = $sortBy.' '.$sortType;
            }
			$count = Investment::model()->count($criteria);
			//menghitung jumlah data Hasil Filter 
            
            $pages = new CPagination($count);
            $pages->pageSize=10;
			$pages->applyLimit($criteria);
			//membuat page dengan plugin bawaan yii
            
            
			//mencari list data part berdasarkan kriteria yang ditentukan
			$data = Investment::model()->findAll($criteria);
			//mencari list data part berdasarkan kriteria yang ditentukan
		
            $this->render('index',array( //mengirim data ke view index
		'data'=>$data,
                'pages'=>$pages,
                'search'=>$search
            ));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Investment('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Investment']))
			$model->attributes=$_GET['Investment'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Investment the loaded model
	 * @throws CHttpException
	 */
	//Function untuk mecari data berdasarkan primary key
	public function loadModel($id)
	{
		//Mencari data berdasarkang primary key
		$model=Investment::model()->findByPk($id);
		if($model===null)
			//jika data tidak ditemukan maka akan menghasilkan error 404
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Investment $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='investment-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
