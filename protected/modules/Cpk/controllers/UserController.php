<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

class UserController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
            $level = isset(Yii::app()->user->level) ? Yii::app()->user->level : 'c';
		return array(
//			array('allow',  // allow all users to perform 'index' and 'view' actions
//				'actions'=>array('index','view'),
//				'users'=>array('*'),
//			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','index','delete'),
				'expression'=>'Yii::app()->user->level =="super" ',
			),
//			array('allow', // allow admin user to perform 'admin' and 'delete' actions
//				'actions'=>array('admin','delete'),
//				'users'=>array('admin'),
//			),
                        array('deny',  // deny all users
				'expression'=>'Yii::app()->user->isGuest',
			),
			array('deny',  // deny all users
				'expression'=> '"'.$level.'" !== "super" ',
			),
                        
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new User;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['User']))
		{
                    $model->attributes=$_POST['User'];
                    $pass = md5($model->password);
                    $model->password = $pass;
                    $email = User::model()->findByAttributes(['email'=>$model->email]) ? User::model()->findByAttributes(['email'=>$model->email]) : null;
                    $username = User::model()->findByAttributes(['username'=>$model->username]) ? User::model()->findByAttributes(['username'=>$model->username]) : null;
                    if(isset($username)){
                        Yii::app()->user->setFlash('error', "<strong>Username</strong> Sudah Digunakan");
                        Yii::app()->user->setFlash('username', "<strong>Username</strong> Sudah Digunakan");
                        $this->redirect(Yii::app()->request->urlReferrer);
                    }else if(isset($email)){
                        Yii::app()->user->setFlash('error', "<strong>Email</strong> Sudah Digunakan");
                        Yii::app()->user->setFlash('email', "<strong>Email</strong> Sudah Digunakan");
                        $this->redirect(Yii::app()->request->urlReferrer);
                    } else {
                        if($model->save()){
                            Yii::app()->user->setFlash('success', "Data berhasil disimpan");
                            $this->redirect(array('view','id'=>$model->id_user));
                        } else {
                            Yii::app()->user->setFlash('error', "error");
                            if($model->username == null){
                                Yii::app()->user->setFlash('username', "<strong>Username</strong> tidak boleh kosong");
                            }
                            if($model->password == null){
                                Yii::app()->user->setFlash('password', "<strong>Password</strong> tidak boleh kosong");
                            }
                            if($model->level == null){
                                Yii::app()->user->setFlash('level', "<strong>Level</strong> tidak boleh kosong");
                            }
                            if($model->email == null){
                                Yii::app()->user->setFlash('email', "<strong>Email</strong> tidak boleh kosong");
                            }
                            if($model->id_dealer == null){
                                Yii::app()->user->setFlash('id_dealer', "<strong>Dealer</strong> tidak boleh kosong");
                            }
                        
                            $this->redirect(Yii::app()->request->urlReferrer);
                        }
                    } 
		}

		$this->render('create',array(
			'model'=>$model,
                        'optionDealer'=>$this->getDealer()
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
                
		if(isset($_POST['User']))
		{
					$model->username=$_POST['User']['username'];

					if ($_POST['User']['password'] !== null || $_POST['User']['password'] !== ''){
						$pass = md5($_POST['User']['password']);
                    	$model->password = $pass;	
					}
					$model->email = $_POST['User']['email'];
					$model->level = $_POST['User']['level'];
					$model->id_dealer = Yii::app()->user->dealer;
                    
                    if($model->save()){
                        Yii::app()->user->setFlash('success', "Data berhasil diubah");
			$this->redirect(array('view','id'=>$model->id_user));
                    } else {
                        Yii::app()->user->setFlash('error', "error");
                        if($model->username == null){
                            Yii::app()->user->setFlash('username', "<strong>Username</strong> tidak boleh kosong");
                        }
                        if($model->password == null){
                            Yii::app()->user->setFlash('password', "<strong>Password</strong> tidak boleh kosong");
                        }
                        if($model->level == null){
                            Yii::app()->user->setFlash('level', "<strong>Level</strong> tidak boleh kosong");
                        }
                        if($model->email == null){
                            Yii::app()->user->setFlash('email', "<strong>Email</strong> tidak boleh kosong");
                        }
                        if($model->id_dealer == null){
                            Yii::app()->user->setFlash('id_dealer', "<strong>Dealer</strong> tidak boleh kosong");
                        }
                        
                        $this->redirect(Yii::app()->request->urlReferrer);
                    }
		}
		$model->password = '';
		$this->render('update',array(
			'model'=>$model,
                        'optionDealer'=>$this->getDealer()
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
        public function beforeSave($insert)
        {
            $pass = md5($this->password);
            $this->password = $pass;
            return parent::beforeSave($insert);
        }
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
                        Yii::app()->user->setFlash('success', "Data berhasil dihapus");
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
            $criteria = new CDbCriteria();
			$search = Yii::app()->request->getQuery('search') ? Yii::app()->request->getQuery('search') : null;
			$sortBy = Yii::app()->request->getQuery('sortBy') ? Yii::app()->request->getQuery('sortBy') : null;
            $sortType = Yii::app()->request->getQuery('sortType') ? Yii::app()->request->getQuery('sortType') : 'asc';
            if($search){
                $criteria->addCondition('username like "%'.urldecode($search).'%" ','OR');
                $criteria->addCondition('level like "%'.$search.'%" ','OR');
                $criteria->addCondition('email like "%'.urldecode($search).'%" ','OR');
                $criteria->addCondition('id_dealer like "%'.$search.'%"','OR');
			}
			if($sortBy !== null){
                $criteria->order = $sortBy.' '.$sortType;
			}
			
            $count = User::model()->with('dealer')->count($criteria);
            
            $pages = new CPagination($count);
            $pages->pageSize=10;
            $pages->applyLimit($criteria);
            
            
            
            $data = User::model()->with('dealer')->findAll($criteria);
            $this->render('index',array(
				'data'=>$data,
                'pages'=>$pages,
                'search'=>$search
       	    ));
	}
        public function getDealer()
        {
            $criteria=new CDbCriteria;
            $criteria->order = 'dealer_name DESC';
            return CHtml::listData(MasterDealer::model()->findAll($criteria),'id','dealer_name');
        }

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new User('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['User']))
			$model->attributes=$_GET['User'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return User the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=User::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param User $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='user-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	
	public function actionSendLupaPassword()
	{
		// $email = $_POST['email'];
		$email = 'blackdesirewx9@gmail.com';
		$mail = new PHPMailer(true);
		try {
			//Server settings
			$mail->SMTPDebug = SMTP::DEBUG_SERVER;                      // Enable verbose debug output
			$mail->isSMTP();                                            // Send using SMTP
			$mail->Host       = 'smtp.gmail.com';                    // Set the SMTP server to send through
			$mail->SMTPAuth   = true;
			$mail->SMTPSecure = 'tls';         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
			$mail->Port       = 587;                                    // Enable SMTP authentication
			$mail->Username   = 'icetech.official@gmail.com';                     // SMTP username
			$mail->Password   = 'technoiced3v';                               // SMTP password
			                                   // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above
	
			//Recipients
			$mail->setFrom('icetech.official@gmail.com', 'Technoice Developer');
			$mail->addAddress($email, 'Dicky Nursalim');     // Add a recipient
			
			
	
			// Attachments
		// Optional name
	
			// Content
			$mail->isHTML(true);                                  // Set email format to HTML
			$mail->Subject = 'Here is the subject';
			$mail->Body    = '<a href="'.$_SERVER['HTTP_HOST'].Yii::app()->baseUrl.'/Cpk/user/pageGantiPassword"> Ini Link Ganti Password</a>';
			$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';
	
			$mail->send();
			echo 'Message has been sent';
		} catch (Exception $e) {
			echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
		}
	}
}