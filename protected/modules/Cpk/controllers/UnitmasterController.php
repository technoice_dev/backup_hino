<?php

class UnitmasterController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
            $level = isset(Yii::app()->user->level) ? Yii::app()->user->level : 'c';
		return array(
//			array('allow',  // allow all users to perform 'index' and 'view' actions
//				'actions'=>array('index','view'),
//				'users'=>array('*'),
//			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','index','delete'),
				'expression'=>'Yii::app()->user->level =="super" ',
			),
                        array('deny',  // deny all users
				'expression'=>'Yii::app()->user->isGuest ',
			),
			array('deny',  // deny all users
				'expression'=> '"'.$level.'" !== "super" ',
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Unitmaster;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Unitmaster']))
		{
					$model->attributes=$_POST['Unitmaster'];
					$unit_name = $model->unit_name;
					$unit_type = $model->unit_type;
					$unitData = UnitMaster::model()->findAllByAttributes(['unit_name' => $unit_name]);
					$unitTypeData = UnitMaster::model()->findAllByAttributes(['unit_type' => $unit_type]);
					if(isset($unitData) || isset($unitTypeData)){
						if (count($unitData) > 0){
							Yii::app()->user->setFlash('error', "error");
							if (count($unitData) > 0){
								Yii::app()->user->setFlash('unit_name', "<strong>Nama Unit</strong> sudah ada");	
							}

							$this->redirect(Yii::app()->request->urlReferrer);
						}
						
					}
					
                    if($model->save()){
                        Yii::app()->user->setFlash('success', "Data berhasil diubah");
                        $this->redirect(array('view','id'=>$model->unit_id));
                    } else {
                        Yii::app()->user->setFlash('error', "error");
                        if($model->unit_type == null){
                            Yii::app()->user->setFlash('unit_type', "<strong>Unit Type</strong> tidak boleh kosong");
                        }
                        if($model->unit_name == null){
                            Yii::app()->user->setFlash('unit_name', "<strong>Unit Name</strong> tidak boleh kosong");
                        }
                        $this->redirect(Yii::app()->request->urlReferrer);
                    }
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Unitmaster']))
		{
					$model->attributes=$_POST['Unitmaster'];
                    if($model->save()){
                        Yii::app()->user->setFlash('success', "Data berhasil diubah");
                        $this->redirect(array('view','id'=>$model->unit_id));
                    } else {
                        Yii::app()->user->setFlash('error', "error");
                        if($model->unit_type == null){
                            Yii::app()->user->setFlash('unit_type', "<strong>Unit Type</strong> tidak boleh kosong");
                        }
                        if($model->unit_name == null){
                            Yii::app()->user->setFlash('unit_name', "<strong>Unit Name</strong> tidak boleh kosong");
                        }
                        $this->redirect(Yii::app()->request->urlReferrer);
                    }
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
                        Yii::app()->user->setFlash('success', "Data berhasil dihapus");
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
            $criteria = new CDbCriteria();
            $search = Yii::app()->request->getQuery('search') ? Yii::app()->request->getQuery('search') : null;
            if($search){
                $criteria->addCondition('unit_type like "%'.urldecode($search).'%" ','OR');
                $criteria->addCondition('unit_name like "%'.urldecode($search).'%" ','OR');
                
			}
            $count = Unitmaster::model()->with('partbytype','invDet')->count($criteria);
            
            $pages = new CPagination($count);
            $pages->pageSize=10;
            $pages->applyLimit($criteria);
            $model = Unitmaster::model()->with('partbytype','invDet')->findAll($criteria);
            $this->render('index',array(
		'model'=>$model,
                'pages'=>$pages,
                'search'=>$search,
            ));
	}

	
	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Unitmaster('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Unitmaster']))
			$model->attributes=$_GET['Unitmaster'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Unitmaster the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Unitmaster::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Unitmaster $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='unitmaster-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
