<?php

class PartbytypeController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	//Mengaktifkan access Control
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	//Function Untuk Hak akses PartByTypeController
	public function accessRules()
	{
            $level = isset(Yii::app()->user->level) ? Yii::app()->user->level : 'c';
			return array(
//			array('allow',  // allow all users to perform 'index' and 'view' actions
//				'actions'=>array('index','view'),
//				'users'=>array('*'),
//			),
			
                        array('deny',  // deny all users
				'expression'=>'Yii::app()->user->isGuest ',
			),
			array('deny',  // deny all users
				'expression'=> '"'.$level.'" == "super" ',
			),
                        
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	//Function untuk detail data PartByType
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	//Function Create PartByType
	public function actionCreate()
	{
		$model=new Partbytype;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Partbytype']))
		{
			$model->attributes=$_POST['Partbytype'];
			$model->id_dealer = Yii::app()->user->dealer;
			
			$dataPbt = Partbytype::model()->findAllByAttributes(['unit_id' => $model->unit_id, 'part_id' => $model->part_id,'id_dealer' => Yii::app()->user->dealer]);
			if (count($dataPbt) > 0){
				Yii::app()->user->setFlash('error', "Data error");
				Yii::app()->user->setFlash('part-by-type', "Data Part untuk unit yang di pilih sudah ada!");
				$this->redirect(array('create'));
			}
			
                    if($model->save()){
                        Yii::app()->user->setFlash('success', "Data berhasil disimpan");
                        $this->redirect(array('view','id'=>$model->typemtc_id));
                    } else {
                        Yii::app()->user->setFlash('error', "Data error");
                        if($model->unit_id == null){
                            Yii::app()->user->setFlash('Unit', "<strong>Unit</strong> tidak boleh kosong");
                        }
                        if($model->part_id == null){
                            Yii::app()->user->setFlash('Part', "<strong>Part</strong> tidak boleh kosong");
                        }
                        if($model->qty == null){
                            Yii::app()->user->setFlash('Qty', "<strong>Qty</strong> tidak boleh kosong");
                        }
                        if($model->kminterval == null){
                            Yii::app()->user->setFlash('Km Interval', "<strong>Km Interval</strong> tidak boleh kosong");
                        }
                        if($model->probabilitas == null){
                            Yii::app()->user->setFlash('Probabilitas', "<strong>Probabilitas</strong> tidak boleh kosong");
                        }
                        $this->redirect(array('create'));
                        
                    }
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Partbytype']))
		{
										$model->attributes=$_POST['Partbytype'];
										$model->id_dealer = Yii::app()->user->dealer;
                    if($model->save()){
                        Yii::app()->user->setFlash('success', "Data berhasil diubah");
                        $this->redirect(array('view','id'=>$model->typemtc_id));
                    } else {
                        Yii::app()->user->setFlash('error', "Data error");
                        if($model->unit_id == null){
                            Yii::app()->user->setFlash('Unit', "<strong>Unit</strong> tidak boleh kosong");
                        }
                        if($model->part_id == null){
                            Yii::app()->user->setFlash('Part', "<strong>Part</strong> tidak boleh kosong");
                        }
                        if($model->qty == null){
                            Yii::app()->user->setFlash('Qty', "<strong>Qty</strong> tidak boleh kosong");
                        }
                        if($model->kminterval == null){
                            Yii::app()->user->setFlash('Km Interval', "<strong>Km Interval</strong> tidak boleh kosong");
                        }
                        if($model->probabilitas == null){
                            Yii::app()->user->setFlash('Probabilitas', "<strong>Probabilitas</strong> tidak boleh kosong");
                        }
                        $this->redirect(Yii::app()->request->urlReferrer);
                    }
		}
		$dataPart = $this->getPart($model->part_id);
		$dataUnit = $this->getUnit($model->unit_id);
		$this->render('update',array(
				'model'			=> $model,
				'partData'	=> $dataPart,
				'unitData'	=> $dataUnit,
        'optionUnit'=> $dataPart,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
                        Yii::app()->user->setFlash('success', "Data berhasil dihapus");
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
            $criteria = new CDbCriteria();
			$search = Yii::app()->request->getQuery('search') ? Yii::app()->request->getQuery('search') : null;
			$sortBy = Yii::app()->request->getQuery('sortBy') ? Yii::app()->request->getQuery('sortBy') : null;
            $sortType = Yii::app()->request->getQuery('sortType') ? Yii::app()->request->getQuery('sortType') : 'asc';
            $criteria->with = array(
                            'part',
                            'unit'
                            );
            if($search){
                $criteria->addCondition('part.part_name like "%'.$search.'%" ','OR');
                $criteria->addCondition('unit.unit_name like "%'.$search.'%" ','OR');
                $criteria->addCondition('probabilitas like "%'.$search.'%" ','OR');
                $criteria->addCondition('kminterval like "%'.$search.'%"','OR');
                $criteria->addCondition('qty like "%'.$search.'%"','OR');
			}
			$criteria->addCondition('t.id_dealer = '.Yii::app()->user->dealer);
            if($sortBy !== null){
                $criteria->order = $sortBy.' '.$sortType;
            }
            $count = Partbytype::model()->count($criteria);
            
            $pages = new CPagination($count);
            $pages->pageSize=10;
            $pages->applyLimit($criteria);
            
            
            
            $data = Partbytype::model()->findAll($criteria);
            $this->render('index',array(
		'data'=>$data,
                'pages'=>$pages,
                'search'=>$search
       	    ));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Partbytype('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Partbytype']))
			$model->attributes=$_GET['Partbytype'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}
	public function actionGetUnit()
	{
		$criteria=new CDbCriteria;
		$criteria->select = "unit_id,CONCAT(unit_name,' ','(',unit_type,')') as unit_name";
		$search = Yii::app()->request->getQuery('search') ? Yii::app()->request->getQuery('search') : null;
		if($search){
			$criteria->addCondition('unit_name like "%'.urldecode($search).'%" ','OR');
			$criteria->addCondition('unit_type like "%'.$search.'%" ','OR');
		}
		$criteria->order = 'unit_name ASC';
		$criteria->limit = 10;
		$unit = Unitmaster::model()->findAll($criteria);
		$hasil = array();
		
		foreach($unit as $row){
			$hasil[] = ['id'=>$row->unit_id,'text'=>$row->unit_name];
		}
		echo CJSON::encode($hasil);
	}
	public function getUnit($unit_id)
	{
		return Unitmaster::model()->findAllByAttributes(['unit_id' => $unit_id]);
	}
        public function getPart($part_id)
        {
            $criteria=new CDbCriteria;
			$criteria->select = "part_id,CONCAT(part_name,' ','(',part_no,')') as part_name";
			$criteria->addCondition('part_id ='.$part_id);
            $criteria->order = 'part_name DESC';
            return MasterPart::model()->findAll($criteria);
        }
		public function actionGetPart()
        {
            $criteria=new CDbCriteria;
			$criteria->select = "part_id,CONCAT(part_name,' ','(',part_no,')') as part_name";
			$search = Yii::app()->request->getQuery('search') ? Yii::app()->request->getQuery('search') : null;
			if($search){
                $criteria->addCondition('part_name like "%'.urldecode($search).'%" ','OR');
                $criteria->addCondition('part_no like "%'.$search.'%" ','OR');
            }
			$criteria->order = 'part_name ASC';
			$criteria->limit = 10;
			$part = MasterPart::model()->findAll($criteria);
			$hasil = array();
			
			foreach($part as $row){
				$hasil[] = ['id'=>$row->part_id,'text'=>$row->part_name];
			}
			echo CJSON::encode($hasil);
        }
	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Partbytype the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Partbytype::model()->with(array('part','unit'))->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Partbytype $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='partbytype-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
