<?php

class PenawaranController extends Controller
{
	public function accessRules()
	{
        $level = isset(Yii::app()->user->level) ? Yii::app()->user->level : 'c';
		return array(
            array('deny',  // deny all users
				'expression'=>'Yii::app()->user->isGuest ',
			),
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'expression'=> '"'.$level.'" == "sales" ',
			),
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'expression'=> '"'.$level.'" == "dealer" ',
			),         
			array('deny',  // deny all users
				'users'=> array('*'),
			),
		);
	}
	public function actionIndex()
	{
			$criteria = new CDbCriteria();
			$search = Yii::app()->request->getQuery('search') ? Yii::app()->request->getQuery('search') : null;
			$sortBy = Yii::app()->request->getQuery('sortBy') ? Yii::app()->request->getQuery('sortBy') : null;
			$sortType = Yii::app()->request->getQuery('sortType') ? Yii::app()->request->getQuery('sortType') : 'asc';
			$from = Yii::app()->request->getQuery('from_date') ? Yii::app()->request->getQuery('from_date') : null;
			$to = Yii::app()->request->getQuery('to_date') ? Yii::app()->request->getQuery('to_date') : null;
            if($search){
                $criteria->addCondition('(customer.cust_name like "%'.urldecode($search).'%" ','OR');
                $criteria->addCondition('id_invoice like "%'.$search.'%" ',') AND');
			}
			if($sortBy !== null){
                $criteria->order = $sortBy.' '.$sortType;
			}
			if($from !== null){
				$criteria->addCondition('tgl >= "'.$from.'"','AND');
			}
			if($to !== null){
				$criteria->addCondition('tgl <= "'.$to.'"','AND');
			}
			$criteria->addCondition('t.id_dealer = '.Yii::app()->user->dealer);
            $count = Invoice::model()->with('invDet','customer','user')->count($criteria);
            
            $pages = new CPagination($count);
            $pages->pageSize=10;
            $pages->applyLimit($criteria);
            
            
            
            $data = Invoice::model()->with('invDet','customer','user')->findAll($criteria);
            $this->render('index',array(
				'data'=>$data,
                'pages'=>$pages,
				'search'=>$search,
				'from'=>$from,
				'to'=>$to
       	    ));
	}
	public function actioninvList()
        {
            $criteria = new CDbCriteria();
			$search = Yii::app()->request->getQuery('search') ? Yii::app()->request->getQuery('search') : null;
			$id_invoice = Yii::app()->request->getQuery('invID') ? Yii::app()->request->getQuery('invID') : null;
			$criteria->addCondition('id_invoice = '.$id_invoice);
            if($search){
				
                $criteria->addCondition('unit.unit_name like "%'.urldecode($search).'%" ','AND');
                $criteria->addCondition('preventive_cost like "%'.$search.'%" ','OR');
                $criteria->addCondition('periodic_cost like "%'.urldecode($search).'%" ','OR');
				$criteria->addCondition('labor_cost like "%'.$search.'%"','OR');
				$criteria->addCondition('invest_cost like "%'.$search.'%"','OR');
				$criteria->addCondition('work_day like "%'.$search.'%"','OR');
				$criteria->addCondition('km_day like "%'.$search.'%"','OR');
				$criteria->addCondition('unit_qty like "%'.$search.'%"','OR');
				

			}
			
			$invoice = Invoice::model()->findByAttributes(['id_invoice' => $id_invoice]);
            $count = InvoiceDet::model()->with('unit')->count($criteria);
            
            $pages = new CPagination($count);
            $pages->pageSize=10;
            $pages->applyLimit($criteria);
            
            
            
            $data = InvoiceDet::model()->with('unit')->findAll($criteria);
            $this->render('invList',array(
				'data'=>$data,
                'pages'=>$pages,
				'search'=>$search,
				'invoice' => $invoice
				
       	    ));
		}
		public function actionCpkDetail()
		{
			$id_penawaran = Yii::app()->request->getQuery('idPenawaran') ? Yii::app()->request->getQuery('idPenawaran') : null;
			$invDet = InvoiceDet::model()->with('unit')->findByAttributes(['id'=>$id_penawaran]);
			$invoice = Invoice::model()->with('invDet')->findByAttributes(['id_invoice' => $invDet->id_invoice]);
			$allInvoiceDet = InvoiceDet::model()->findAllByAttributes(['id_invoice' => $invDet->id_invoice]);
			$unitType = isset($invDet->unit->unit_name) ?  $invDet->unit->unit_name : '';
            $diskonPrice = $invDet->discount;
			$R = 100 - $diskonPrice;
			$unit_qty = $km_day = $workDay = $kontrak = 0;
			
			if (count($allInvoiceDet) > 0){
				foreach($allInvoiceDet as $invDet){
					if ($invDet->kontrak > $kontrak){
						$kontrak = $invDet->kontrak;
					}
					$unit_qty += $invDet->unit_qty;
					if ($invDet->work_day > $workDay){
						$workDay = $invDet->work_day;
					}
					if ($invDet->km_day > $km_day){
						$km_day += $invDet->km_day;    
					}
				}
				$jarakTempuh = $km_day * $workDay;
			}
			
			
            $unitQty = $unit_qty;
            $kmPerHari = $km_day;
            $hariKerja = $workDay;
            $jarakTempuh = ($kmPerHari) * ($hariKerja);
            $lamaKontrak = $kontrak;
            $mechSenior = $invoice->jml_senior;
			$mechJunior = $invoice->jml_junior;
			$jumlahForeman = $invoice->jml_foreman;
			$jumlahStaffWh = $invoice->jml_staff;
			$jumlahLeader = $invoice->jml_leader;
			$jumlahOther = $invoice->jml_other;
            $manpower = $invoice->labor_cost;
			$invTotal = $invoice->invest_cost;			
			$cpk_calculation_inv = (12*$lamaKontrak*$unitQty*$jarakTempuh);
            $inv_cpk = intVal($invTotal) > 0 && intVal($cpk_calculation_inv) > 0 ? $invTotal / (12*$lamaKontrak*$unitQty*$jarakTempuh) : 0;
            
            
            $periodic = $invDet->periodic_cost;
			$preventive = $invDet->preventive_cost;
			$invDetPart = InvoiceDetPart::model()->with('part')->byCategory()->byPartName()->findAllByAttributes(['id_invoice'=>$invDet->id_invoice,'unit_id'=>$invDet->unit_id]);
			$part_total = $periodic + $preventive;
			$cpk_calculation = (12*$lamaKontrak*$jarakTempuh);
            $part_cpk = $part_total != 0  && $cpk_calculation != 0 ? $part_total / $cpk_calculation : 0;
            
            
            
            
            
            $this->render('cpkDet',['tipe'=>$unitType,'diskon'=>$diskonPrice,'jml_unit'=>$unitQty,'mileage'=>$jarakTempuh
                    ,'lamaKontrak'=>$lamaKontrak,'seniorMechanic'=>$mechSenior,'juniorMechanic'=>$mechJunior,'invCpk'=>$inv_cpk
                    ,'periodic'=>$periodic,'preventive'=>$preventive,'manpower'=>$manpower
					,'partTotal'=>$part_total,'partCpk'=>$part_cpk,'invTotal'=>$invTotal,'days'=>$hariKerja,'kmPerHari'=>$kmPerHari,'invDetPart'=>$invDetPart
					,'jumlahForeman'=>$jumlahForeman,'jumlahStaffWh'=>$jumlahStaffWh,'jumlahLeader'=>$jumlahLeader,'jumlahOther'=>$jumlahOther]);
		}

		public function actionDelete($id)
		{
			$invoice = Invoice::model()->findByPk($id);
			$invoiceDet = InvoiceDet::model()->deleteAll('',['id_invoice' => $id]);
			$invoiceDetPart = InvoiceDetPart::model()->deleteAll('',['id_invoice' => $id]);
			$invoice->delete();
	
			
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
		}
		public function actionDeleteInvDet()
		{
						$id_penawaran = Yii::app()->request->getQuery('idPenawaran') ? Yii::app()->request->getQuery('idPenawaran') : null;
						$invoice = InvoiceDet::model()->findByPk($id_penawaran);
						$id_invoice = $invoice->id_invoice;
						$invoiceDetPart = InvoiceDetPart::model()->deleteAll('',['id_invoice' => $invoice->id_invoice , 'unit_id' => $invoice->unit_id]);
						$invoice->delete();
						$this->recalculateInvoiceCost($id_invoice);
				
	
			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(Yii::app()->request->urlReferrer);
		}

		public function recalculateInvoiceCost($id_invoice)
		{
			$id_invoice = Yii::app()->request->getQuery('invID') ? Yii::app()->request->getQuery('invID') : null;
			if ($id_invoice !== null){
				$invoice = Invoice::model()->findByAttributes(['id_invoice'=>$id_invoice]);
				$invoiceDet = InvoiceDet::model()->findAllByAttributes(['id_invoice'=>$id_invoice]);
				$lamaKontrak = 0;
				$unit_qty = 0;
				$jarakTempuh = 0;
				$workDay = 0;
				$km_day = 0;
				$mechJunior = $invoice->jml_junior;
				$mechSenior = $invoice->jml_senior;
				$mechForeman = $invoice->jml_foreman;
				$mechStaffWh = $invoice->jml_staff;
				$mechLeader = $invoice->jml_leader;
				$mechOther = $invoice->jml_other;
				$invoiceDet = InvoiceDet::model()->findAllByAttributes(['id_invoice'=>$id_invoice]);
				if (count($invoiceDet) > 0){
					foreach($invoiceDet as $invDet){
						if ($invDet->kontrak > $lamaKontrak){
							$lamaKontrak = $invDet->kontrak;
						}
						$unit_qty += $invDet->unit_qty;
						if ($invDet->work_day > $workDay){
							$workDay = $invDet->work_day;
						}
						if ($invDet->km_day > $km_day){
							$km_day += $invDet->km_day;    
						}
					}
					$jarakTempuh = $km_day * $workDay;
				}
				$unitQty = $unit_qty;
				$manpower = $this->manpower($mechJunior,$mechSenior,$mechForeman,$mechStaffWh,$mechLeader,$mechOther,$lamaKontrak,$unitQty,$jarakTempuh);
				$invoice->labor_cost = $manpower['total'];
				$invoice->jml_senior = $mechSenior;
				$invoice->jml_junior = $mechJunior;
				$invoice->jml_foreman = $mechForeman;
				$invoice->jml_staff = $mechStaffWh;
				$invoice->jml_leader = $mechLeader;
				$invoice->jml_other = $mechOther;
				$save_invoice = $invoice->save();
			}
			
		}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}