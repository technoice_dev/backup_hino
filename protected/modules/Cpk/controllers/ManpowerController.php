<?php

class ManpowerController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations

		);
	}
//
//	/**
//	 * Specifies the access control rules.
//	 * This method is used by the 'accessControl' filter.
//	 * @return array access control rules
//	 */
	public function accessRules()
	{
            $level = isset(Yii::app()->user->level) ? Yii::app()->user->level : 'c';
		return array(
//			array('allow',  // allow all users to perform 'index' and 'view' actions
//				'actions'=>array('index','view'),
//				'users'=>array('*'),
//			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','index','delete'),
				'expression'=>'"'.$level.'" =="dealer" || "'.$level.'" =="sales"',
			),
                        array('deny',  // deny all users
				'expression'=>'Yii::app()->user->isGuest ',
			),
			array('deny',  // deny all users
				'expression'=> '"'.$level.'" == "super" ',
			),
                        
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	//Function Create Customer
	public function actionCreate()
	{
		$model=new Manpower;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		//Ketika Dibuka menggunakan method GET maka akan menampilkan form create Customer
		//Ketika Mengirim Request Menggunakan POST maka akan masuk ke insert Customer
		if(isset($_POST['Manpower']))
		{
			//Menghilangkan koma agar value angka bisa masuk database
			$model->attributes=$_POST['Manpower'];
			$bsc_sallary = str_replace(',','',$_POST['Manpower']['bsc_sallary']);
			$pph = str_replace(',','',$_POST['Manpower']['pph']);
			$laundry = str_replace(',','',$_POST['Manpower']['laundry']);
			$transport = str_replace(',','',$_POST['Manpower']['transport']);
			$meal = str_replace(',','',$_POST['Manpower']['meal']);
			$medical = str_replace(',','',$_POST['Manpower']['medical']);
			$mobile_phone = str_replace(',','',$_POST['Manpower']['mobile_phone']);
			$bpjs = str_replace(',','',$_POST['Manpower']['bpjs']);
			$thr = str_replace(',','',$_POST['Manpower']['thr']);
			$uni_safety = str_replace(',','',$_POST['Manpower']['uni_safety']);
			$hardship = str_replace(',','',$_POST['Manpower']['hardship']);
			$overtime = str_replace(',','',$_POST['Manpower']['overtime']);
			$other_pay = str_replace(',','',$_POST['Manpower']['other_pay']);
			
			$model->attributes=$_POST['Manpower'];
			$model->bsc_sallary = $bsc_sallary;
			$model->pph = $pph;
			$model->laundry = $laundry;
			$model->transport = $transport;
			$model->meal = $meal;
			$model->medical = $medical;
			$model->mobile_phone = $mobile_phone;
			$model->bpjs = $bpjs;
			$model->thr = $thr;
			$model->uni_safety = $uni_safety;
			$model->hardship = $hardship;
			$model->overtime = $overtime;
			$model->other_pay = $other_pay;
			$model->id_dealer = Yii::app()->user->dealer;
			$checkManpowerData = Manpower::model()->findAllByAttributes(['kategory' => $model->kategory , 'id_dealer' => $model->id_dealer ]);
			//Validasi Data Manpower yang sudah ada
			if (count($checkManpowerData) > 0){
				//Jika sudah ada maka data tidak akan di save dan akan di kembalikan ke form create manpower
				Yii::app()->user->setFlash('manpower-error','Data Manpower Sudah ada');
				$this->redirect(Yii::app()->request->urlReferrer);
			}
			if($model->save())
				//Save Data Manpower Jika sudah lulus validasi
				$this->redirect(array('view','id'=>$model->mp_id));
		}

		$this->render('create',array(
			'model'=>$model,
                        
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	//Function Update Manpower
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Manpower']))
		{
			//Menghilangkan koma pada angka agar value bisa masuk database
			$model->attributes=$_POST['Manpower'];
			$bsc_sallary = str_replace(',','',$_POST['Manpower']['bsc_sallary']);
			$pph = str_replace(',','',$_POST['Manpower']['pph']);
			$laundry = str_replace(',','',$_POST['Manpower']['laundry']);
			$transport = str_replace(',','',$_POST['Manpower']['transport']);
			$meal = str_replace(',','',$_POST['Manpower']['meal']);
			$medical = str_replace(',','',$_POST['Manpower']['medical']);
			$mobile_phone = str_replace(',','',$_POST['Manpower']['mobile_phone']);
			$bpjs = str_replace(',','',$_POST['Manpower']['bpjs']);
			$thr = str_replace(',','',$_POST['Manpower']['thr']);
			$uni_safety = str_replace(',','',$_POST['Manpower']['uni_safety']);
			$hardship = str_replace(',','',$_POST['Manpower']['hardship']);
			$overtime = str_replace(',','',$_POST['Manpower']['overtime']);
			$other_pay = str_replace(',','',$_POST['Manpower']['other_pay']);
			
			$model->bsc_sallary = $bsc_sallary;
			$model->pph = $pph;
			$model->laundry = $laundry;
			$model->transport = $transport;
			$model->meal = $meal;
			$model->medical = $medical;
			$model->mobile_phone = $mobile_phone;
			$model->bpjs = $bpjs;
			$model->thr = $thr;
			$model->uni_safety = $uni_safety;
			$model->hardship = $hardship;
			$model->overtime = $overtime;
			$model->other_pay = $other_pay;
            $model->id_dealer = Yii::app()->user->dealer;
			if($model->save())
				//Jika data sudah lolos validasi model data akan di save dan akan di arahkan ke detail manpower
				$this->redirect(array('view','id'=>$model->mp_id));
		}

		$this->render('update',array(
			'model'=>$model,
                        
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	//Fuction Delete Manpower
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete(); //Mencari data manpower berdasarkan id dan menghapus data manpower

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
			$criteria = new CDbCriteria();
			//inisialisasi kriteria untuk database
			$search = Yii::app()->request->getQuery('search') ? Yii::app()->request->getQuery('search') : null;
			//cek jika ada filter search
			$sortBy = Yii::app()->request->getQuery('sortBy') ? Yii::app()->request->getQuery('sortBy') : null;
			//cek jika ada sorting data berdasarkan kolom
			$sortType = Yii::app()->request->getQuery('sortType') ? Yii::app()->request->getQuery('sortType') : 'asc';
			//cek tipe sorting data ascending atau descending , jika belum ada maka akan otomatis menjadi ascending
			
            $criteria->with = array( // menambahkan relasi dealer pada kriteria database
                            'dealer',
                            );
            if($search){
				//Filter Search data pada semua kolom
                $criteria->addCondition('mp_id like "%'.$search.'%" ','OR');
                $criteria->addCondition('bsc_sallary like "%'.$search.'%" ','OR');
                $criteria->addCondition('pph like "%'.$search.'%" ','OR');
                $criteria->addCondition('laundry like "%'.$search.'%"','OR');    
                $criteria->addCondition('transport like "%'.$search.'%"','OR');
                $criteria->addCondition('meal like "%'.$search.'%"','OR');
                $criteria->addCondition('medical like "%'.$search.'%"','OR');
                $criteria->addCondition('mobile_phone like "%'.$search.'%"','OR');
                $criteria->addCondition('bpjs like "%'.$search.'%"','OR');
                $criteria->addCondition('thr like "%'.$search.'%"','OR');
                $criteria->addCondition('uni_safety like "%'.$search.'%"','OR');
                $criteria->addCondition('hardship like "%'.$search.'%"','OR');
                $criteria->addCondition('overtime like "%'.$search.'%"','OR');
                $criteria->addCondition('kategory like "%'.$search.'%"','OR');
                $criteria->addCondition('dealer.dealer_name like "%'.urlencode($search).'%"','OR');
			}
			if($sortBy !== null){
				//Jika ada sorting data makan akan menjadi order by
                $criteria->order = $sortBy.' '.$sortType;
            }
			$criteria->addCondition('id_dealer = '.Yii::app()->user->dealer);
			//Mencari data berdasarkan Dealer yang sedang login
			$count = Manpower::model()->count($criteria);
			//Menghitung jumlah data yang sudah diberi kriteria
            
            $pages = new CPagination($count);
            $pages->pageSize=10;
			$pages->applyLimit($criteria);
			//membuat pagination dengan plugin bawaan Yii
            
            
            
			$data = Manpower::model()->findAll($criteria);
			//Mencari data manpower dengan kriteria yang sudah di tetukan

            $this->render('index',array( // mengirim data ke view index
		'data'=>$data,
                'pages'=>$pages,
                'search'=>$search
       	    ));
	}
		//function untuk list dealer ketika membuka form manpower
        public function getDealer()
        {
            $criteria=new CDbCriteria;
            $criteria->order = 'id DESC';
            return CHtml::listData(MasterDealer::model()->findAll($criteria),'id','dealer_name');
        }

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Manpower('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Manpower']))
			$model->attributes=$_GET['Manpower'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Manpower the loaded model
	 * @throws CHttpException
	 */
	//Function Untuk mencari data berdasarkan primary key
	public function loadModel($id)
	{
		$model=Manpower::model()->findByPk($id); //mencari data berdasarkan primary key
		if($model===null)
			//jika tidak ditemukan maka akan mengirim status 404
			throw new CHttpException(404,'The requested page does not exist.');
		return $model; // jika data ditemukan maka data akan menjadi return value
	}

	/**
	 * Performs the AJAX validation.
	 * @param Manpower $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='manpower-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
