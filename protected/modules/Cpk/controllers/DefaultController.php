<?php

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Reader\Csv;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
class DefaultController extends Controller
{
        public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
        $level = isset(Yii::app()->user->level) ? Yii::app()->user->level : 'c';
		return array(
            array('deny',  // deny all users
				'expression'=>'Yii::app()->user->isGuest ',
			),
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('cpkCalculate','process','cetakInvoice','destroyInvoice'),
				'expression'=> '"'.$level.'" == "sales" ',
            ),
            array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('cpkCalculate','process','cetakInvoice','destroyInvoice','editCost','editCostInvoice','index','getUnit','getCustomer'),
				'expression'=> '"'.$level.'" == "dealer" ',
			),
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('printInvoice'),
				'expression'=> '"'.$level.'" == "dealer" ',
            ),
            array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('printInvoice'),
				'expression'=> '"'.$level.'" == "sales" ',
            ),
            array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('importFromExcel','importDataPart'),
				'expression'=> '"'.$level.'" == "super" ',
            ),
			array('deny',  // deny all users
				'users'=> array('*'),
			),
		);
    }
    
        public function actionIndex()
        {
            $this->render('index');
        }
        public function actionCpkCalculate()
        {
            $id_invoice = Yii::app()->request->getQuery('id_invoice') ? Yii::app()->request->getQuery('id_invoice') : null;
            if($id_invoice !== null){
                $invoice = Invoice::model()->findByAttributes(['id_invoice'=>$id_invoice]);
                $session = Yii::app()->session;
                $session['invId'] = $invoice->id_invoice;
                $session['customer'] = $invoice->id_cust;
            }
            $unit = Unitmaster::model()->with('partbytype')->findAll();
            $customer = Customer::model()->findAllByAttributes(['id_dealer'=>Yii::app()->user->dealer]);
            $this->render('cpk_calculate',['unit'=>$unit,'customer'=>$customer]);
        }
        public function actionProcess()
        {
            $unitType = isset($_POST['tipe']) ? $_POST['tipe'] : '';
            $diskonPrice = isset($_POST['disc']) ? $_POST['disc'] : 0;
            $R = 100 - $diskonPrice;
            $unitQty = isset($_POST['jumlah']) ? $_POST['jumlah'] : 0 ;
            $kmPerHari = isset($_POST['jarak']) ? $_POST['jarak'] : 0;
            $hariKerja = isset($_POST['day']) ? $_POST['day'] : 0 ;
            $jarakTempuh = ($kmPerHari) * ($hariKerja);
            $lamaKontrak = isset($_POST['kontrak']) ? $_POST['kontrak'] : 0 ;
            $mechSenior = isset($_POST['senior']) ? $_POST['senior'] : 0;
            $mechJunior = isset($_POST['junior']) ? $_POST['junior'] : 0 ;
            $mechForeman = isset($_POST['foreman']) ? $_POST['foreman'] : 0;
            $mechStaffWh = isset($_POST['staffWh']) ? $_POST['staffWh'] : 0;
            $mechLeader = isset($_POST['leader']) ? $_POST['leader'] : 0;
            $mechOther = isset($_POST['other']) ? $_POST['other'] : 0;
            $customer = isset($_POST['customer']) ? $_POST['customer'] : null ;
            $investment = isset($_POST['inv']) ? $_POST['inv'] : null ;
            $manpower = $this->manpower($mechJunior,$mechSenior,$mechForeman,$mechStaffWh,$mechLeader,$mechOther,$lamaKontrak,$unitQty,$jarakTempuh);
            $invTotal = isset($investment) && count($investment) > 0 ? $this->investment($investment) : 0;
            $totalMileage = $jarakTempuh * 12;

            $cpk_calculation = (12*$lamaKontrak*$unitQty*$jarakTempuh);
            if($invTotal == 0) {
                $inv_cpk = 0;
            } else {
                $inv_cpk = $cpk_calculation !== 0 ? ( $invTotal / $cpk_calculation ) : 0;
            }
            $allPeriodic = $this->periodic($unitType,$lamaKontrak,$diskonPrice,($jarakTempuh * 12));
            $allPreventive = $this->preventive($unitType,$lamaKontrak,$diskonPrice,($jarakTempuh * 12));
            $periodic = $allPeriodic['total'];
            $preventive = $allPreventive['total'];
            $dataPeriodic = $allPeriodic['data'];
            $dataPreventive = $allPreventive['data'];
            $part_total = $periodic + $preventive;
            $part_cpk = $part_total == 0 ? 0 : $part_total/(12*$lamaKontrak*$jarakTempuh);
            
            
            
            
            
            $this->render('hasil',['tipe'=>$unitType,'diskon'=>$diskonPrice,'jml_unit'=>$unitQty,'mileage'=>$jarakTempuh
                    ,'lamaKontrak'=>$lamaKontrak,'seniorMechanic'=>$mechSenior,'juniorMechanic'=>$mechJunior,'invCpk'=>$inv_cpk
                    ,'periodic'=>$periodic,'preventive'=>$preventive,'manpower'=>$manpower,'customer'=>$customer
                    ,'partTotal'=>$part_total,'partCpk'=>$part_cpk,'invTotal'=>$invTotal,'days'=>$_POST['day'],
                    'dataPeriodic'=>$dataPeriodic,'dataPreventive'=>$dataPreventive,'kmPerHari'=>$kmPerHari,'frek_ganti'
                    ,'jumlahForeman'=> $mechForeman, 'jumlahStaffWh' => $mechStaffWh, 'jumlahLeader' => $mechLeader , 'jumlahOther' => $mechOther]);
            
        }
        public function actionCetakinvoice()
        {
            $unit_qty = $_POST['unit'];
            $unit_type = $_POST['tipe'];
            $mileage = $_POST['mileage'];
            $preventive = intVal($_POST['preventive']);
            $periodic = intVal($_POST['periodic']);
            $diskon = $_POST['diskon'];
            $kontrak = $_POST['kontrak'];
            $day = $_POST['day'];
            $km_day = $_POST['km_day'];
            $customer= isset($_POST['customer']) ? $_POST['customer'] : null ;
            $criteria = new CDbCriteria();
            $criteria->with = array('part','unit');
            $criteria->addCondition('unit.unit_id = '.$unit_type);
            $criteria->addCondition('part.category = "preventive" ');
            $criteria2 = new CDbCriteria();
            $criteria2->with = array('part','unit');
            $criteria2->addCondition('unit.unit_id = '.$unit_type);
            $criteria2->addCondition('part.category = "periodic" ');
            $mileage = $km_day * $day * 12;
            $dataPreventive = Partbytype::model()->with()->findAll($criteria);
            $dataPeriodic = Partbytype::model()->with()->findAll($criteria2);
            $session = Yii::app()->session;
            $type_qty = 0;
            if(!isset($session['customer'])){
                $session['customer'] = $customer;
            } else {
                $session['customer'] = $session['customer'];
            }
            if($session['invId']){
                $invId = $session['invId'];
            } else{
                $invoice = new Invoice;
                $invId = date('Ymds');
                $invoice->id_invoice = $invId;
                $invoice->tgl = date('Y-m-d');
                $invoice->id_cust = $session['customer'];
                $invoice->id_user = Yii::app()->user->id_user;
                $invoice->id_dealer = Yii::app()->user->dealer;
                $invoice->labor_cost = 0;
                $invoice->invest_cost = 0;
                $invoice->jml_senior = 0;
                $invoice->jml_junior = 0;
                $invoice->jml_foreman = 0;
                $invoice->jml_staff = 0;
                $invoice->jml_leader = 0;
                $invoice->jml_other = 0;
                $invoice->save();
                $session['invId'] = $invId;
            
            }
            
            if($session['invId'] !== null){
                //$criteria->order = 'tgl DESC';
                $invoiceNew = Invoice::model()->findByAttributes(['id_invoice'=>$invId]);
                $invoice_det = new InvoiceDet;
                $invoice_det->id_invoice = $invId;
                $invoice_det->unit_id = $unit_type;
                $invoice_det->kontrak = $kontrak;
                $invoice_det->km_day = floatVal($km_day);
                $invoice_det->discount = $diskon;
                $invoice_det->preventive_cost = $preventive;
                $invoice_det->periodic_cost = $periodic;
                // $invoice_det->labor_cost = $manpower;
                // $invoice_det->invest_cost = $inv;
                // $invoice_det->jml_senior = $senior;
                // $invoice_det->jml_junior = $junior;
                // $invoice_det->jml_foreman = $foreman;
                // $invoice_det->jml_staff = $staffWh;
                // $invoice_det->jml_leader = $leader;
                // $invoice_det->jml_other = $other;
                $invoice_det->work_day = $_POST['day'];
                $invoice_det->unit_qty = $unit_qty;
                if($invoice_det->save()){
                    if($periodic !== 0){
                        foreach($dataPeriodic as $row){
                            $invoice_det_part = new InvoiceDetPart;
                            $invoice_det_part->id_invoice = $invId;
                            $invoice_det_part->unit_id = $row->unit_id;
                            $invoice_det_part->part_id = $row->part_id;
                            $invoice_det_part->qty = floatval($row->qty);
                            $invoice_det_part->pricelist = $row->part->pricelist;
                            $invoice_det_part->kminterval = $row->kminterval;
                            $invoice_det_part->mileage = $mileage;
                            $invoice_det_part->frek_ganti  = round(($mileage / $row->kminterval),1);
                            $invoice_det_part->probability = $row->probabilitas;
                            $save_inv_det_per = $invoice_det_part->save();
                            if ($save_inv_det_per){

                            }
                            
                        }
                    }
                    if($preventive !== 0){
                        foreach($dataPreventive as $row){
                            $invoice_det_part = new InvoiceDetPart;
                            $invoice_det_part->id_invoice = $invId;
                            $invoice_det_part->unit_id = $row->unit_id;
                            $invoice_det_part->part_id = $row->part_id;
                            $invoice_det_part->qty = floatval($row->qty);
                            $invoice_det_part->pricelist = $row->part->pricelist;
                            $invoice_det_part->kminterval = $row->kminterval;
                            $invoice_det_part->mileage = $mileage;
                            $invoice_det_part->frek_ganti  = round(($mileage / $row->kminterval),1);
                            $invoice_det_part->probability = $row->probabilitas;
                            $save_inv_det_prev = $invoice_det_part->save();
                            if ($save_inv_det_prev){
                                
                            }
                        }
                    }
                    $session['type_qty'] = $session['type_qty'] ? $session['type_qty']+1 : 1;
                    $this->recalculateInvoiceCost($invId);
                    $this->redirect(Yii::app()->baseUrl.'/Cpk/default/cpkCalculate');
                }
            }
        }
        public function investment($investment)
        {
            $inv = $investment ? $investment : null;
            $criteria = new CDbCriteria;
            $invList = "";
            foreach($inv as $row2){
                $invList .= "'$row2',";
            }
            $invCondition = substr($invList,0,-1);
            $criteria->addCondition('inv_category IN ('.$invCondition.')');
            $criteria->addCondition('id_dealer = '.Yii::app()->user->dealer);
            $model = Investment::model()->findAll($criteria);
            $total = 0;
            foreach($model as $row){
                $price = $row->inv_price * $row->inv_qty;
                $total += $price;
            }
            return $total;
        }
        public function manpower($jml_junior,$jml_senior,$jml_foreman,$jml_staffWh,$jml_leader,$jml_other,$lamaKontrak,$uio,$mileage)
        {
            if($lamaKontrak > 0 && $uio > 0 &&  $mileage > 0){
                $no = 1; 
                $limit = $lamaKontrak + 1; 
                $seniorSub = 0 ; 
                $juniorSub = 0 ; 
                $foremanSub = 0 ; 
                $staffWhSub = 0 ; 
                $leaderSub = 0 ; 
                $otherSub = 0 ; 
                $allMech = 0;
                if($jml_junior > 0){
                    $model_junior = Manpower::model()->findByAttributes(['kategory'=>'junior','id_dealer'=>Yii::app()->user->dealer]);
                    $juniorSalary = isset($model_junior->bsc_sallary)? $model_junior->bsc_sallary:0;
                    $juniorMeal = isset($model_junior->meal)? $model_junior->meal:0;
                    $juniorTHR = isset($model_junior->thr)? $model_junior->thr:0;
                    $juniorMedical = isset($model_junior->medical)? $model_junior->medical:0;
                    $juniorPPh = isset($model_junior->pph)? $model_junior->pph:0;
                    $juniorLaundry = isset($model_junior->laundry)? $model_junior->laundry:0;
                    $juniorHardship = isset($model_junior->hardship)? $model_junior->hardship:0;
                    $juniorOvertime = isset($model_junior->overtime)? $model_junior->overtime:0;
                    $juniorBPJS = isset($model_junior->bpjs)? $model_junior->bpjs:0;
                    $juniorUniSafety = isset($model_junior->uni_safety)? $model_junior->uni_safety:0;
                    $juniorTransport = isset($model_junior->transport)? $model_junior->transport:0;
                    $juniorMobile = isset($model_junior->mobile_phone)? $model_junior->mobile_phone : 0;
                    $juniorCost = ($juniorSalary+$juniorMeal+$juniorHardship+$juniorLaundry+$juniorMedical+$juniorOvertime+$juniorTransport+$juniorUniSafety+$juniorTHR+$juniorBPJS+$juniorPPh+$juniorMobile);
                    $juniorSub = ($juniorCost * (120/100) );
                    $juniorSub = intVal($juniorSub) *  $jml_junior;
                }
                
                if ($jml_senior > 0){
                    $model_senior = Manpower::model()->findByAttributes(['kategory'=>'senior','id_dealer'=>Yii::app()->user->dealer]);
                    $seniorSalary = isset($model_senior->bsc_sallary)? $model_senior->bsc_sallary:0;
                    $seniorMeal = isset($model_senior->meal)? $model_senior->meal:0;
                    $seniorTHR = isset($model_senior->thr)? $model_senior->thr:0;
                    $seniorMedical = isset($model_senior->medical)? $model_senior->medical:0;
                    $seniorPPh = isset($model_senior->pph)? $model_senior->pph:0;
                    $seniorLaundry = isset($model_senior->laundry)? $model_senior->laundry:0;
                    $seniorBPJS = isset($model_senior->bpjs)? $model_senior->bpjs:0;
                    $seniorHardship = isset($model_senior->hardship)? $model_senior->hardship:0;
                    $seniorOvertime = isset($model_senior->overtime)? $model_senior->overtime:0;
                    $seniorUniSafety = isset($model_senior->uni_safety)? $model_senior->uni_safety:0;
                    $seniorTransport = isset($model_senior->transport)? $model_senior->transport:0;
                    $seniorMobile = isset($model_senior->mobile_phone)? $model_senior->mobile_phone : 0;
                    $seniorCost = ($seniorSalary+$seniorMeal+$seniorHardship+$seniorLaundry+$seniorMedical+$seniorOvertime+$seniorTransport+$seniorUniSafety+$seniorTHR+$seniorBPJS+$seniorPPh+$seniorMobile) ;
                    $seniorSub = intVal($seniorCost) * (120/100) * $jml_senior;
                }
                if ($jml_foreman > 0){
                    $model_foreman = Manpower::model()->findByAttributes(['kategory'=>'foreman','id_dealer'=>Yii::app()->user->dealer]);
                    $foremanSalary = isset($model_foreman->bsc_sallary)? $model_foreman->bsc_sallary:0;
                    $foremanMeal = isset($model_foreman->meal)? $model_foreman->meal:0;
                    $foremanTHR = isset($model_foreman->thr)? $model_foreman->thr:0;
                    $foremanMedical = isset($model_foreman->medical)? $model_foreman->medical:0;
                    $foremanPPh = isset($model_foreman->pph)? $model_foreman->pph:0;
                    $foremanLaundry = isset($model_foreman->laundry)? $model_foreman->laundry:0;
                    $foremanHardship = isset($model_foreman->hardship)? $model_foreman->hardship:0;
                    $foremanOvertime = isset($model_foreman->overtime)? $model_foreman->overtime:0;
                    $foremanBPJS = isset($model_foreman->bpjs)? $model_foreman->bpjs:0;
                    $foremanUniSafety = isset($model_foreman->uni_safety)? $model_foreman->uni_safety:0;
                    $foremanTransport = isset($model_foreman->transport)? $model_foreman->transport:0;
                    $foremanMobile = isset($model_foreman->mobile_phone)? $model_foreman->mobile_phone : 0;
                    $foremanCost = ($foremanSalary+$foremanMeal+$foremanHardship+$foremanLaundry+$foremanMedical+$foremanOvertime + $foremanTransport+$foremanUniSafety+$foremanTHR+$foremanBPJS+$foremanPPh+$foremanMobile) * $jml_foreman;
                    $foremanSub = $foremanCost * 1.2;
                }

                if($jml_staffWh > 0){
                    $model_staff_wh = Manpower::model()->findByAttributes(['kategory'=>'staff','id_dealer'=>Yii::app()->user->dealer]);
                    $staffWhSalary = isset($model_staff_wh->bsc_sallary)? $model_staff_wh->bsc_sallary:0;
                    $staffWhMeal = isset($model_staff_wh->meal)? $model_staff_wh->meal:0;
                    $staffWhTHR = isset($model_staff_wh->thr)? $model_staff_wh->thr:0;
                    $staffWhMedical = isset($model_staff_wh->medical)? $model_staff_wh->medical:0;
                    $staffWhPPh = isset($model_staff_wh->pph)? $model_staff_wh->pph:0;
                    $staffWhLaundry = isset($model_staff_wh->laundry)? $model_staff_wh->laundry:0;
                    $staffWhHardship = isset($model_staff_wh->hardship)? $model_staff_wh->hardship:0;
                    $staffWhOvertime = isset($model_staff_wh->overtime)? $model_staff_wh->overtime:0;
                    $staffWhBPJS = isset($model_staff_wh->bpjs)? $model_staff_wh->bpjs:0;
                    $staffWhUniSafety = isset($model_staff_wh->uni_safety)? $model_staff_wh->uni_safety:0;
                    $staffWhTransport = isset($model_staff_wh->transport)? $model_staff_wh->transport:0;
                    $staffWhMobile = isset($model_staff_wh->mobile_phone)? $model_staff_wh->mobile_phone : 0;
                    $staffWhCost = ($staffWhSalary+$staffWhMeal+$staffWhHardship+$staffWhLaundry+$staffWhMedical+$staffWhOvertime + $staffWhTransport+$staffWhUniSafety+$staffWhTHR+$staffWhBPJS+$staffWhPPh+$staffWhMobile) * $jml_staffWh;
                    $staffWhSub = $staffWhCost * 1.2;
                }

                if ($jml_leader > 0){
                    $model_leader = Manpower::model()->findByAttributes(['kategory'=>'leader','id_dealer'=>Yii::app()->user->dealer]);
                    $leaderSalary = isset($model_leader->bsc_sallary)? $model_leader->bsc_sallary:0;
                    $leaderMeal = isset($model_leader->meal)? $model_leader->meal:0;
                    $leaderTHR = isset($model_leader->thr)? $model_leader->thr:0;
                    $leaderMedical = isset($model_leader->medical)? $model_leader->medical:0;
                    $leaderPPh = isset($model_leader->pph)? $model_leader->pph:0;
                    $leaderLaundry = isset($model_leader->laundry)? $model_leader->laundry:0;
                    $leaderHardship = isset($model_leader->hardship)? $model_leader->hardship:0;
                    $leaderOvertime = isset($model_leader->overtime)? $model_leader->overtime:0;
                    $leaderBPJS = isset($model_leader->bpjs)? $model_leader->bpjs:0;
                    $leaderUniSafety = isset($model_leader->uni_safety)? $model_leader->uni_safety:0;
                    $leaderTransport = isset($model_leader->transport)? $model_leader->transport:0;
                    $leaderMobile = isset($model_leader->mobile_phone)? $model_leader->mobile_phone : 0;
                    $leaderCost = ($leaderSalary+$leaderMeal+$leaderHardship+$leaderLaundry+$leaderMedical+$leaderOvertime + $leaderTransport+$leaderUniSafety+$leaderTHR+$leaderBPJS+$leaderPPh+$leaderMobile) * $jml_leader;
                    $leaderSub = $leaderCost * 1.2;
                }

                if($jml_other > 0){
                    $model_other = Manpower::model()->findByAttributes(['kategory'=>'other','id_dealer'=>Yii::app()->user->dealer]);
                    $otherSalary = isset($model_other->bsc_sallary)? $model_other->bsc_sallary:0;
                    $otherMeal = isset($model_other->meal)? $model_other->meal:0;
                    $otherTHR = isset($model_other->thr)? $model_other->thr:0;
                    $otherMedical = isset($model_other->medical)? $model_other->medical:0;
                    $otherPPh = isset($model_other->pph)? $model_other->pph:0;
                    $otherLaundry = isset($model_leader->other)? $model_other->laundry:0;
                    $otherHardship = isset($model_other->hardship)? $model_other->hardship:0;
                    $otherOvertime = isset($model_leader->overtime)? $model_leader->overtime:0;
                    $otherBPJS = isset($model_other->bpjs)? $model_other->bpjs:0;
                    $otherUniSafety = isset($model_other->uni_safety)? $model_other->uni_safety:0;
                    $otherTransport = isset($model_other->transport)? $model_other->transport:0;
                    $otherMobile = isset($model_other->mobile_phone)? $model_other->mobile_phone : 0;
                    $otherCost = ($otherSalary+$otherMeal+$otherHardship+$otherLaundry+$otherMedical+$otherOvertime + $otherTransport+$otherUniSafety+$otherTHR+$otherBPJS+$otherPPh+$otherMobile) * $jml_other;
                    $otherSub = $otherCost * 1.2;
                }

                $allSub = ($juniorSub + $seniorSub + $foremanSub + $staffWhSub + $leaderSub + $otherSub);
                $total = $allSub * 12 *$lamaKontrak ;
                $cpk_calculation = ((($mileage*12)*$lamaKontrak*$uio));
                $CPK = $total !== 0 && $cpk_calculation !== 0 ? ($total / $cpk_calculation) : 0 ;
                $hasil['total'] = $total;
                $hasil['junior'] = $juniorSub;
                $hasil['senior'] = $seniorSub;
                $hasil['foreman'] = $foremanSub;
                $hasil['staffWh'] = $staffWhSub;
                $hasil['leader'] = $leaderSub;
                $hasil['other'] = $otherSub;
                $hasil['CPK'] = $CPK ;
            } else {
                $hasil['total'] = 0;
                $hasil['junior'] = 0;
                $hasil['senior'] = 0;
                $hasil['foreman'] = 0;
                $hasil['staffWh'] = 0;
                $hasil['leader'] = 0;
                $hasil['other'] = 0;
                $hasil['CPK'] = 0 ;
            }
            return $hasil;
        }
        public function manpower_($jml_junior,$jml_senior,$lamaKontrak,$uio,$mileage)
        {
            $no2 = 1; 
            $limit2 = $lamaKontrak + 1; 
            $seniorSub = 0 ; 
            $juniorSub = 0 ; 
            $allMech = 0;
            $model_junior = Manpower::findByAttributes(['kategory'=>'junior']);
            $model_senior = Manpower::findByAttributes(['kategory'=>'senior']);
        }
        public function preventive($unit_type,$lamaKontrak,$diskon,$mileage)
        {
            if ($unit_type !== null || $unit_type !== '' &&  $lamaKontrak > 0 && $mileage > 0){
                $diskon_new = $diskon? $diskon : null;
                $limit = $lamaKontrak + 1;
                $mileage = round($mileage);
                for ($y = 1; $y < $limit; $y++) {
                    $i[$y] = 0;
                    $j[$y] = $i[$y];
                }
                $criteria = new CDbCriteria();
                $criteria->with = array('part','unit');
                $criteria->addCondition('unit.unit_id = '.$unit_type);
                $criteria->addCondition('part.category = "preventive" ');
                $criteria->addCondition('id_dealer = '.Yii::app()->user->dealer );
                $model = Partbytype::model()->with('unit','part')->byPartName()->findAll($criteria);
                $total = 0;
                foreach($model as $row){
                    $harga = $row->part->pricelist;
                    if($diskon_new != null){
                        $new_price = $row->part->pricelist * ((100-$diskon_new)/100) * $row->qty;
                    } else {
                        $new_price = $row->part->pricelist * $row->qty;
                    }
                    $freq1 = $mileage / $row->kminterval;
                    $y1 = $new_price * $freq1 * $row->probabilitas;
                    if ($lamaKontrak > 1) {
                        $p = [];
                        for($c = 1 ; $c < $limit ; $c++){
                            if($c !== 1){
                                $p[$c] = $p[$c-1] + (($p[$c-1] / 100) * 10);
                            } else {
                                $p[$c] = $y1;
                            }
                            
                        }
                        $p_total = 0;
                        foreach($p as $r_p){
                            $p_total += $r_p;
                        }
                        
                    } else {
                        $p_total = $y1;
                    }
                    $total += $p_total;
                }
                    
                $hasil['total'] = $total;
                $hasil['data'] = $model;
        } else {
                $hasil['total'] = 0;
                $hasil['data'] = [];
        }
            
            return $hasil;
        }
        public function periodic($unit_type,$lamaKontrak,$diskon,$mileage)
        {
            if ($unit_type !== null || $unit_type !== '' &&  $lamaKontrak > 0 && $mileage > 0){
                $diskon_new = $diskon? $diskon : null;
                $limit = $lamaKontrak + 1;
                $mileage = round($mileage);
                for ($y = 1; $y < $limit; $y++) {
                    $i[$y] = 0;
                    $j[$y] = $i[$y];
                }
                $criteria = new CDbCriteria();
                $criteria->with = array('part','unit');
                $criteria->addCondition('unit.unit_id = '.$unit_type);
                $criteria->addCondition('part.category = "periodic" ');
                $criteria->addCondition('id_dealer = '.Yii::app()->user->dealer);
                $model = Partbytype::model()->with('unit','part')->byPartName()->findAll($criteria);
                $total = 0;
                foreach($model as $row){
                    $harga = $row->part->pricelist;
                    if($diskon_new != null){
                        $new_price = $row->part->pricelist * ((100-$diskon_new)/100) * $row->qty;
                    } else {
                        $new_price = $row->part->pricelist * $row->qty;
                    }
                    $freq1 = $mileage / $row->kminterval;
                    $y1 = $new_price * $freq1 * $row->probabilitas;
                    if ($lamaKontrak > 1) {
                        $p = [];
                        for($c = 1 ; $c < $limit ; $c++){
                            if($c !== 1){
                                $p[$c] = $p[$c-1] + (($p[$c-1] / 100) * 10);
                            } else {
                                $p[$c] = $y1;
                            }
                            
                        }
                        $p_total = 0;
                        foreach($p as $r_p){
                            $p_total += $r_p;
                        }
                        
                    } else {
                        $p_total = $y1;
                    }
                    $total += $p_total;
                }
                
                $hasil['total'] = $total;
                $hasil['data'] = $model;
            } else {
                $hasil['total'] = 0;
                $hasil['data'] = [];
            }
            return $hasil;
        }
        public function actionSaveinvoice()
        {
            $invoice = new Invoice;
            $invoice->id_invoice = 1;
            $invoice->tgl = date('Ymd');
            $invoice->id_cust = 1;
            $invoice->id_user = 1;
            if($invoice->save()){
                echo 'berhasil';
            } else {
                echo 'gagal';
            }
        }
        public function actionPrintInvoice()
        {
            $id_invoice = Yii::app()->request->getQuery('invID') ? Yii::app()->request->getQuery('invID') : null;
            
            $invoice = Invoice::model()->with('invDet.unit','customer')->findByPk($id_invoice);
            $invoiceDet = InvoiceDet::model()->with('unit')->findAllByAttributes(['id_invoice'=>$id_invoice]);
            $invoiceDetPart = InvoiceDetPart::model()->with('partbytype','part')->findAllByAttributes(['id_invoice'=>$id_invoice]);
            $kontrak = InvoiceDet::model()->with('unit')->findByAttributes(['id_invoice'=>$id_invoice])->kontrak;
            $_SESSION['type_qty'] = count($invoiceDet);
            if(isset($_SESSION['invId'])){
                $_SESSION['invId'] = $_SESSION['invId'];
            } else {
                $_SESSION['invId'] = $invoice->id_invoice;
            }
            $lamaKontrak = 0;
            $unit_qty = 0;
            $jarakTempuh = 0;
            $workDay = 0;
            $km_day = 0;
            if (count($invoiceDet) > 0){
                foreach($invoiceDet as $invDet){
                    if ($invDet->kontrak > $lamaKontrak){
                        $lamaKontrak = $invDet->kontrak;
                    }
                    
                    $unit_qty += $invDet->unit_qty;
                    if ($invDet->work_day > $workDay){
                        $workDay = $invDet->work_day;
                    }
                    if ($invDet->km_day > $km_day){
                        $km_day += $invDet->km_day;    
                    }
                }
                $jarakTempuh = $km_day * $workDay;
            }
            $unitQty = $unit_qty;

            $this->render('invoiceDet',['invoiceData'=>$invoice,'invoiceDet'=>$invoiceDet,'kontrak'=>$lamaKontrak,'unit_qty' => $unitQty,
                            'mileage' => $jarakTempuh]);
            
        }
        public function actionDestroyinvoice()
        {
            $session = Yii::app()->session;
            $session['invId'] = null;
            $session['type_qty'] = null;
            $session['customer'] = null;
            $this->redirect(Yii::app()->baseUrl.'/Cpk/default/cpkCalculate');
        }
        public function actionImportFromExcel()
        {  
            $this->render('importPart');
        }
        public function actionImportInvestment()
        {  
            $this->render('importInvestment');
        }
        public function actionImportDataPart()
        {
            $file_mimes = array('application/octet-stream', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet','application/wps-office.xlsx');
 
            if(isset($_FILES['excel_data']['name']) && in_array($_FILES['excel_data']['type'], $file_mimes)) {
 
                $arr_file = explode('.', $_FILES['excel_data']['name']);
                $extension = end($arr_file);

                if('csv' == $extension) {
                    $reader = new PhpOffice\PhpSpreadsheet\Reader\Csv();
                } else {
                    $reader = new PhpOffice\PhpSpreadsheet\Reader\Xlsx();
                }

                $spreadsheet = $reader->load($_FILES['excel_data']['tmp_name']);

                $sheetData = $spreadsheet->getActiveSheet()->toArray();
                $success_count = 0;
	            for($i = 1;$i < count($sheetData);$i++)
	            {
                    $part = MasterPart::model()->findByAttributes(['part_no'=>trim($sheetData[$i]['1'])]);
                    if (!isset($part)){
                        $part = new MasterPart();
                    }
                    $part->part_name = $sheetData[$i]['0'];
                    $part->part_no = trim($sheetData[$i]['1']);
                    $part->pricelist = trim($sheetData[$i]['2']);
                    $part->category = trim($sheetData[$i]['3']);
                    $save_part = $part->save();
                    if($save_part){
                        $success_count +=1;
                    }
                }
                Yii::app()->user->setFlash('success','Import Data Part Selesai');
                $this->redirect(Yii::app()->baseUrl.'/Cpk/default/importFromExcel');
                if ($success_count > 0){
                    

                } 
            }
                       
        }
        public function actionImportInvestmentData()
        {
            $file_mimes = array('application/octet-stream', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet','application/wps-office.xlsx');
 
            if(isset($_FILES['excel_data']['name']) && in_array($_FILES['excel_data']['type'], $file_mimes)) {
 
                $arr_file = explode('.', $_FILES['excel_data']['name']);
                $extension = end($arr_file);

                if('csv' == $extension) {
                    $reader = new PhpOffice\PhpSpreadsheet\Reader\Csv();
                } else {
                    $reader = new PhpOffice\PhpSpreadsheet\Reader\Xlsx();
                }

                $spreadsheet = $reader->load($_FILES['excel_data']['tmp_name']);

                $sheetData = $spreadsheet->getActiveSheet()->toArray();
                $success_count = 0;
	            for($i = 1;$i < count($sheetData);$i++)
	            {
                    $part = new Investment();
                    $part->inv_name = $sheetData[$i]['0'];
                    $part->part_no = $sheetData[$i]['1'];
                    $part->pricelist = $sheetData[$i]['2'];
                    $part->category = $sheetData[$i]['3'];
                    $save_part = $part->save();
                    if($save_part){
                        $success_count +=1;
                    }
                }
                $this->redirect(Yii::app()->baseUrl.'/Cpk/default/importFromExcel');
                if ($success_count > 0){
                    Yii::app()->user->setFlash('success','Import Data Part Selesai');
                }   
            }      
        }

    
    public function actionEditCost()
    {
        $id_invoice = Yii::app()->request->getQuery('invID') ? Yii::app()->request->getQuery('invID') : null;
        if ($id_invoice !== null){
            $invoice = Invoice::model()->findByAttributes(['id_invoice' => $id_invoice]);
            $this->render('editCost',['invoice' => $invoice]);
        }
    }

    public function actionEditCostInvoice()
    {
        $id_invoice = Yii::app()->request->getQuery('invID') ? Yii::app()->request->getQuery('invID') : null;
        if ($id_invoice !== null){
            $mechSenior = isset($_POST['senior']) ? $_POST['senior'] : 0;
            $mechJunior = isset($_POST['junior']) ? $_POST['junior'] : 0 ;
            $mechForeman = isset($_POST['foreman']) ? $_POST['foreman'] : 0;
            $mechStaffWh = isset($_POST['staffWh']) ? $_POST['staffWh'] : 0;
            $mechLeader = isset($_POST['leader']) ? $_POST['leader'] : 0;
            $mechOther = isset($_POST['other']) ? $_POST['other'] : 0;
            $customer = isset($_POST['customer']) ? $_POST['customer'] : null ;
            $investment = isset($_POST['inv']) ? $_POST['inv'] : null ;
            $lamaKontrak = 0;
            $unit_qty = 0;
            $jarakTempuh = 0;
            $workDay = 0;
            $km_day = 0;
            $invoice = Invoice::model()->findByAttributes(['id_invoice' => $id_invoice]);
            $invoiceDet = InvoiceDet::model()->findAllByAttributes(['id_invoice'=>$id_invoice]);
            
            if (count($invoiceDet) > 0){
                foreach($invoiceDet as $invDet){
                    if ($invDet->kontrak > $lamaKontrak){
                        $lamaKontrak = $invDet->kontrak;
                    }
                    $unit_qty += $invDet->unit_qty;
                    if ($invDet->work_day > $workDay){
                        $workDay = $invDet->work_day;
                    }
                    if ($invDet->km_day > $km_day){
                        $km_day += $invDet->km_day;    
                    }
                }
            }
        
            $jarakTempuh = $km_day != 0 && $workDay != 0 ? $km_day * $workDay : 0;
            
            
            $unitQty = $unit_qty;
            $manpower = $this->manpower($mechJunior,$mechSenior,$mechForeman,$mechStaffWh,$mechLeader,$mechOther,$lamaKontrak,$unitQty,$jarakTempuh);
            $invTotal = isset($investment) && count($investment) > 0 ? $this->investment($investment) : 0;
            
            
            $invoice->labor_cost = $manpower['total'];
            $invoice->invest_cost = $invTotal;
            $invoice->jml_senior = $mechSenior;
            $invoice->jml_junior = $mechJunior;
            $invoice->jml_foreman = $mechForeman;
            $invoice->jml_staff = $mechStaffWh;
            $invoice->jml_leader = $mechLeader;
            $invoice->jml_other = $mechOther;
            $invSelected = isset($investment) && count($investment) > 0 ? $investment : [];
            $invoice->inv_selected = CJSON::encode($invSelected);
            $save_invoice = $invoice->save();

            if ($save_invoice){
                $this->recalculateInvoiceCost($id_invoice);
                $this->redirect(Yii::app()->baseUrl.'/Cpk/penawaran');
            }
        }
    }

    public function recalculateInvoiceCost($id_invoice)
    {
        $id_invoice = Yii::app()->request->getQuery('invID') ? Yii::app()->request->getQuery('invID') : null;
        if ($id_invoice !== null){
            $invoice = Invoice::model()->findByAttributes(['id_invoice'=>$id_invoice]);
            $invoiceDet = InvoiceDet::model()->findAllByAttributes(['id_invoice'=>$id_invoice]);
            $lamaKontrak = 0;
            $unit_qty = 0;
            $jarakTempuh = 0;
            $workDay = 0;
            $km_day = 0;
            $mechJunior = $invoice->jml_junior;
            $mechSenior = $invoice->jml_senior;
            $mechForeman = $invoice->jml_foreman;
            $mechStaffWh = $invoice->jml_staff;
            $mechLeader = $invoice->jml_leader;
            $mechOther = $invoice->jml_other;
            $invoiceDet = InvoiceDet::model()->findAllByAttributes(['id_invoice'=>$id_invoice]);
            if (count($invoiceDet) > 0){
                foreach($invoiceDet as $invDet){
                    if ($invDet->kontrak > $lamaKontrak){
                        $lamaKontrak = $invDet->kontrak;
                    }
                    $unit_qty += $invDet->unit_qty;
                    if ($invDet->work_day > $workDay){
                        $workDay = $invDet->work_day;
                    }
                    if ($invDet->km_day > $km_day){
                        $km_day += $invDet->km_day;    
                    }
                }
                $jarakTempuh = $km_day * $workDay;
            }
            $unitQty = $unit_qty;
            $manpower = $this->manpower($mechJunior,$mechSenior,$mechForeman,$mechStaffWh,$mechLeader,$mechOther,$lamaKontrak,$unitQty,$jarakTempuh);
            $invoice->labor_cost = $manpower['total'];
            $invoice->jml_senior = $mechSenior;
            $invoice->jml_junior = $mechJunior;
            $invoice->jml_foreman = $mechForeman;
            $invoice->jml_staff = $mechStaffWh;
            $invoice->jml_leader = $mechLeader;
            $invoice->jml_other = $mechOther;
            $save_invoice = $invoice->save();
        }
        
    }

    public function actionGetUnit()
	{
		$criteria=new CDbCriteria;
		$criteria->select = "unit_id,CONCAT(unit_name,' ','(',unit_type,')') as unit_name";
		$search = Yii::app()->request->getQuery('search') ? Yii::app()->request->getQuery('search') : null;
		if($search){
			$criteria->addCondition('unit_name like "%'.urldecode($search).'%" ','OR');
			$criteria->addCondition('unit_type like "%'.$search.'%" ','OR');
		}
		$criteria->order = 'unit_name ASC';
		$criteria->limit = 10;
		$unit = Unitmaster::model()->findAll($criteria);
		$hasil = array();
		
		foreach($unit as $row){
			$hasil[] = ['id'=>$row->unit_id,'text'=>$row->unit_name];
		}
		echo CJSON::encode($hasil);
    }
    
    public function actionGetCustomer()
	{
		$criteria=new CDbCriteria;
		$criteria->select = "id_cust,CONCAT(cust_name,' ','(',phone,')') as cust_name";
        $search = Yii::app()->request->getQuery('search') ? Yii::app()->request->getQuery('search') : null;
        $criteria->addCondition('id_dealer ='.Yii::app()->user->dealer,'AND');
		if ($search){
			$criteria->addCondition('cust_name like "%'.urldecode($search).'%" ','OR');
			$criteria->addCondition('phone like "%'.$search.'%" ','OR');
        }
        
		$criteria->order = 'cust_name ASC';
		$criteria->limit = 10;
		$unit = Customer::model()->findAll($criteria);
		$hasil = array();
		
		foreach($unit as $row){
			$hasil[] = ['id'=>$row->id_cust,'text'=>$row->cust_name];
		}
		echo CJSON::encode($hasil);
	}
        
        
}