<?php

class MasterPartController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations

		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	//Hak akses untuk menu MasterPart
	public function accessRules()
	{
            $level = isset(Yii::app()->user->level) ? Yii::app()->user->level : 'c';
		return array(

			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','index','delete'),
				'expression'=>'Yii::app()->user->level =="superr" ',
			),
                        array('deny',  // deny all users
				'expression'=>'Yii::app()->user->isGuest ',
			),
			array('deny',  // deny all users
				'expression'=> '"'.$level.'" !== "super" ',
			),
                        
		);
	}
	//Hak Akses
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	//Function untuk Detail MasterPart
	public function actionView($id)
	{
		$data = MasterPart::model()->findByPk($id);
		$this->render('view',array(
			'model'=>$data,
		));
	}
	// Function Untuk Detail Master Part
	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */

	//Function Untuk Menampilkan Create Master Part

	public function actionCreate()
	{
		$model=new MasterPart;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		//Ketika Dibuka menggunakan method GET maka akan menampilkan form create Master Part
		//Ketika Mengirim Request Menggunakan POST maka akan masuk ke inser Master Part
		if(isset($_POST['MasterPart']))
		{
			$model->attributes=$_POST['MasterPart'];
			//Validasi data Nama dan Nomer Part 
			//ketika nama atau nomer part sudah ada maka akan di kembalikan ke form update Master Part
			$partData = MasterPart::model()->findAllByAttributes(['part_no'=>$model->part_no]);
			$partNameData = MasterPart::model()->findAllByAttributes(['part_name' => $model->part_name]);
			if (count($partNameData) > 0 || count($partData) > 0){
				Yii::app()->user->setFlash('error', "error");
				if (count($partNameData) > 0){
					Yii::app()->user->setFlash('part_name', "<strong>Nama Part</strong> sudah ada");	
				}
				if (count($partData) > 0){
					Yii::app()->user->setFlash('part_no', "<strong>Nomor Part</strong> sudah ada");	
				}
				$this->redirect(Yii::app()->request->urlReferrer);
			}
			//Save Data Master Part
			if($model->save()){
						//Ketika Data Lolos Validasi Model MasterPart Maka data akan dimasukan ke database dan ,akan redirect ke detail MasterPart
                            Yii::app()->user->setFlash('success', "Data berhasil disimpan");
                            $this->redirect(array('view','id'=>$model->part_id));
                        } else {
						//Ketika Data Gagal Validasi Model MasterPart maka akan di kembalikan ke form create MasterPart dan akan muncul Message Error
                            Yii::app()->user->setFlash('error', "error");
                            if($model->part_name == null){
                                Yii::app()->user->setFlash('part_name', "<strong>Nama Part</strong> tidak boleh kosong");
                            }
                            if($model->part_no == null){
                                Yii::app()->user->setFlash('part_no', "<strong>Nomor Part</strong> tidak boleh kosong");
                            }
                            if($model->pricelist == null){
                                Yii::app()->user->setFlash('pricelist', "<strong>Harga Part</strong> tidak boleh kosong");
                            }
                            if($model->km1th == null){
                                Yii::app()->user->setFlash('km1th', "<strong>Kilometer/tahun</strong> tidak boleh kosong");
                            }
                            
                            
                            $this->redirect(Yii::app()->request->urlReferrer);
                        }
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}
	//Function Create Master Part

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */

	// Function Ini Digunakan Update Master Part
	
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		//Ketika Dibuka menggunakan method GET maka akan menampilkan form create Master Part
		//Ketika Mengirim Request Menggunakan POST maka akan masuk ke update Master Part
		if(isset($_POST['MasterPart']))
		{
			$model->attributes=$_POST['MasterPart'];
			if($model->save()){
				//Ketika Data Lolos Validasi Model MasterPart Maka data akan dimasukan ke database dan ,akan redirect ke detail MasterPart
                                Yii::app()->user->setFlash('success', "Data berhasil diubah");
					$this->redirect(array('view','id'=>$model->part_id));
                        } else {
							//Ketika Data Gagal Validasi Model MasterPart maka akan di kembalikan ke form edit MasterPart dan akan muncul Message Error
                            Yii::app()->user->setFlash('error', "error");
                            if($model->part_name == null){
                                Yii::app()->user->setFlash('part_name', "<strong>Nama Part</strong> tidak boleh kosong");
                            }
                            if($model->part_no == null){
                                Yii::app()->user->setFlash('part_no', "<strong>Nomor Part</strong> tidak boleh kosong");
                            }
                            if($model->pricelist == null){
                                Yii::app()->user->setFlash('pricelist', "<strong>Harga Part</strong> tidak boleh kosong");
                            }
                            $this->redirect(Yii::app()->request->urlReferrer);
                        }
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	//Function Delete MasterPart
	public function actionDelete($id)
	{
		//memanggil function load model untuk mencari data part berdasarkan id dan menghapus data part;
		$this->loadModel($id)->delete();
                Yii::app()->user->setFlash('success', "Data berhasil dihapus");
		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
                        
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
	}

	/**
	 * Lists all models.
	 */

	//Function List Data Part
	public function actionIndex()
	{
			//inisialisasi criteria database
            $criteria = new CDbCriteria();
			$search = Yii::app()->request->getQuery('search') ? Yii::app()->request->getQuery('search') : null;
			//cek jika ada filter search data
			$sortBy = Yii::app()->request->getQuery('sortBy') ? Yii::app()->request->getQuery('sortBy') : null;
			//cek jika ada sorting data berdasarkan kolom
			$sortType = Yii::app()->request->getQuery('sortType') ? Yii::app()->request->getQuery('sortType') : 'asc';
			//cek tipe sorting data antara ascending atau descending
			$category = Yii::app()->request->getQuery('category') ? Yii::app()->request->getQuery('category') : null;
			//cek filter category part

            if($search){
				//jika ada filter search maka data akan di cari di semua kolom pada tabel masterpart
                $criteria->addCondition('part_name like "%'.urldecode($search).'%" ','OR');
                $criteria->addCondition('part_no like "%'.$search.'%" ','OR');
                $criteria->addCondition('t.pricelist like "%'.$search.'%" ','OR');
			}
			if($sortBy !== null){
				//jika ada sorting data maka akan menambahkan ordering data
                $criteria->order = $sortBy.' '.$sortType;
			}
			if ($category !== null){
				//jika ada filter cateogry maka category akan dicari
				$criteria->addCondition("category = '".$category."'","AND");
			}
			//menghitung jumlah data Hasil Filter 
            $count = MasterPart::model()->with('partbytype','invoiceDetPart')->count($criteria);
            //membuat page dengan plugin bawaan yii
            $pages = new CPagination($count);
            $pages->pageSize=10;
            $pages->applyLimit($criteria);
            
            
            //mencari list data part berdasarkan kriteria yang ditentukan
			$data = MasterPart::model()->with('partbytype','invoiceDetPart')->findAll($criteria);
			//mengirim data ke view index
            $this->render('index',array(
				'data'=>$data,
                'pages'=>$pages,
				'search'=>$search,
				'category'=>$category
       	    ));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new MasterPart('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['MasterPart']))
			$model->attributes=$_GET['MasterPart'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return MasterPart the loaded model
	 * @throws CHttpException
	 */
	//function untuk mencari data spesifik berdasarkan primary key
	public function loadModel($id)
	{
		$model=MasterPart::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
	//

	/**
	 * Performs the AJAX validation.
	 * @param MasterPart $model the model to be validated
	 */
	
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='master-part-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
