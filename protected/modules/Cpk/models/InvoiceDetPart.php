<?php

/**
 * This is the model class for table "invoice_det_part".
 *
 * The followings are the available columns in table 'invoice_det_part':
 * @property integer $id
 * @property integer $id_invoice
 * @property integer $unit_id
 * @property integer $part_id
 * @property string $qty
 * @property integer $pricelist
 * @property integer $kminterval
 * @property integer $mileage
 * @property double $frek_ganti
 * @property string $probability
 */
class InvoiceDetPart extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'invoice_det_part';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_invoice, unit_id, part_id, qty, pricelist, kminterval, mileage, frek_ganti, probability', 'required'),
			array('id,id_invoice, unit_id, part_id, pricelist, kminterval', 'numerical', 'integerOnly'=>true),
			array('frek_ganti', 'numerical'),
			array('qty, probability', 'length', 'max'=>11),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_invoice, unit_id, part_id, qty, pricelist, kminterval, mileage, frek_ganti, probability', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'inv'=> array(self::BELONGS_TO,'Invoice','id_invoice'),
                    'partbytype'=>array(self::BELONGS_TO,'Partbytype','unit_id'),
                    'part'=>array(self::BELONGS_TO,'MasterPart','part_id')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_invoice' => 'Id Invoice',
			'unit_id' => 'Unit',
			'part_id' => 'Part',
			'qty' => 'Qty',
			'pricelist' => 'Pricelist',
			'kminterval' => 'Kminterval',
			'mileage' => 'Mileage',
			'frek_ganti' => 'Frek Ganti',
			'probability' => 'Probability',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_invoice',$this->id_invoice);
		$criteria->compare('unit_id',$this->unit_id);
		$criteria->compare('part_id',$this->part_id);
		$criteria->compare('qty',$this->qty,true);
		$criteria->compare('pricelist',$this->pricelist);
		$criteria->compare('kminterval',$this->kminterval);
		$criteria->compare('mileage',$this->mileage);
		$criteria->compare('frek_ganti',$this->frek_ganti);
		$criteria->compare('probability',$this->probability,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return InvoiceDetPart the static model class
	 */
	public function scopes() {
		return array(
			'byCategory' => array('order' => 'part.category ASC'),
			'byPartName' => array('order' => 'part.part_name ASC')
		);
	}
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
