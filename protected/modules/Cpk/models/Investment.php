<?php

/**
 * This is the model class for table "investment".
 *
 * The followings are the available columns in table 'investment':
 * @property integer $inv_id
 * @property string $inv_item
 * @property integer $inv_price
 * @property integer $inv_qty
 * @property string $inv_category
 */
class Investment extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'investment';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('inv_item, inv_price, inv_qty, inv_category', 'required'),
			array('inv_price, inv_qty', 'numerical', 'integerOnly'=>true),
			array('inv_item', 'length', 'max'=>100),
			array('inv_category', 'length', 'max'=>10),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('inv_id, inv_item, inv_price, inv_qty, inv_category', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'dealer'=>array(self::BELONGS_TO , 'MasterDealer','id_dealer'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'inv_id' => 'Inv',
			'inv_item' => 'Inv Item',
			'inv_price' => 'Inv Price',
			'inv_qty' => 'Inv Qty',
			'inv_category' => 'Inv Category',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('inv_id',$this->inv_id);
		$criteria->compare('inv_item',$this->inv_item,true);
		$criteria->compare('inv_price',$this->inv_price);
		$criteria->compare('inv_qty',$this->inv_qty);
		$criteria->compare('inv_category',$this->inv_category,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Investment the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
