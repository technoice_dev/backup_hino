<?php

/**
 * This is the model class for table "kendaraan".
 *
 * The followings are the available columns in table 'kendaraan':
 * @property integer $no_kendaraan
 * @property string $tipe_kendaraan
 * @property string $km_kendaraan
 */
class Kendaraan extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'kendaraan';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('no_kendaraan, tipe_kendaraan, km_kendaraan', 'required'),
			array('no_kendaraan', 'numerical', 'integerOnly'=>true),
			array('tipe_kendaraan, km_kendaraan', 'length', 'max'=>150),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('no_kendaraan, tipe_kendaraan, km_kendaraan', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'no_kendaraan' => 'No Kendaraan',
			'tipe_kendaraan' => 'Tipe Kendaraan',
			'km_kendaraan' => 'Km Kendaraan',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('no_kendaraan',$this->no_kendaraan);
		$criteria->compare('tipe_kendaraan',$this->tipe_kendaraan,true);
		$criteria->compare('km_kendaraan',$this->km_kendaraan,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Kendaraan the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
