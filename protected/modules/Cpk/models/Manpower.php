<?php

/**
 * This is the model class for table "manpower".
 *
 * The followings are the available columns in table 'manpower':
 * @property integer $mp_id
 * @property integer $bsc_sallary
 * @property integer $pph
 * @property integer $laundry
 * @property integer $transport
 * @property integer $meal
 * @property integer $medical
 * @property integer $mobile_phone
 * @property integer $bpjs
 * @property integer $thr
 * @property integer $uni_safety
 * @property integer $hardship
 * @property integer $overtime
 * @property string $kategory
 * @property integer $other_pay
 * @property integer $id_dealer
 */
class Manpower extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'manpower';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('bsc_sallary, pph, laundry, transport, meal, medical, mobile_phone, bpjs, thr, uni_safety, hardship, overtime, kategory, id_dealer', 'required'),
			array('bsc_sallary, pph, laundry, transport, meal, medical, mobile_phone, bpjs, thr, uni_safety, hardship, overtime, other_pay, id_dealer', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('mp_id, bsc_sallary, pph, laundry, transport, meal, medical, mobile_phone, bpjs, thr, uni_safety, hardship, overtime, kategory, other_pay, id_dealer', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'dealer'=>array(self::BELONGS_TO,'MasterDealer','id_dealer'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'mp_id' => 'Mp',
			'bsc_sallary' => 'Bsc Sallary',
			'pph' => 'Pph',
			'laundry' => 'Laundry',
			'transport' => 'Transport',
			'meal' => 'Meal',
			'medical' => 'Medical',
			'mobile_phone' => 'Mobile Phone',
			'bpjs' => 'Bpjs',
			'thr' => 'Thr',
			'uni_safety' => 'Uniform + Safety',
			'hardship' => 'Hardship',
			'overtime' => 'Overtime',
			'kategory' => 'Kategory',
			'other_pay' => 'Other Pay',
			'id_dealer' => 'Id Dealer',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('mp_id',$this->mp_id);
		$criteria->compare('bsc_sallary',$this->bsc_sallary);
		$criteria->compare('pph',$this->pph);
		$criteria->compare('laundry',$this->laundry);
		$criteria->compare('transport',$this->transport);
		$criteria->compare('meal',$this->meal);
		$criteria->compare('medical',$this->medical);
		$criteria->compare('mobile_phone',$this->mobile_phone);
		$criteria->compare('bpjs',$this->bpjs);
		$criteria->compare('thr',$this->thr);
		$criteria->compare('uni_safety',$this->uni_safety);
		$criteria->compare('hardship',$this->hardship);
		$criteria->compare('overtime',$this->overtime);
		$criteria->compare('kategory',$this->kategory,true);
		$criteria->compare('other_pay',$this->other_pay);
		$criteria->compare('id_dealer',$this->id_dealer);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Manpower the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
