<?php

/**
 * This is the model class for table "partbytype".
 *
 * The followings are the available columns in table 'partbytype':
 * @property integer $typemtc_id
 * @property integer $unit_id
 * @property integer $part_id
 * @property integer $qty
 * @property integer $kminterval
 * @property string $probabilitas
 * @property double $frek_ganti
 * @property integer $mileage
 */
class Partbytype extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'partbytype';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('unit_id, part_id, qty, kminterval, probabilitas', 'required'),
			array('unit_id, part_id, kminterval', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('typemtc_id, unit_id, part_id, qty, kminterval, probabilitas', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'part'=>array(self::BELONGS_TO,'MasterPart','part_id'),
					'unit'=>array(self::BELONGS_TO,'Unitmaster','unit_id'),
					'dealer'=>array(self::BELONGS_TO , 'MasterDealer','id_dealer')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'typemtc_id' => 'Typemtc',
			'unit_id' => 'Unit',
			'part_id' => 'Part',
			'qty' => 'Qty',
			'kminterval' => 'Kminterval',
			'probabilitas' => 'Probabilitas',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('typemtc_id',$this->typemtc_id);
		$criteria->compare('unit_id',$this->unit_id);
		$criteria->compare('part_id',$this->part_id);
		$criteria->compare('qty',$this->qty);
		$criteria->compare('kminterval',$this->kminterval);
		$criteria->compare('probabilitas',$this->probabilitas,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	public function scopes() {
		return array(
			'byCategory' => array('order' => 'part.category ASC'),
			'byPartName' => array('order' => 'part.part_name ASC')
		);
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Partbytype the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
