<?php
/* @var $this MasterPartController */
/* @var $model MasterPart */

$this->breadcrumbs=array(
	'Master Parts'=>array('index'),
	$model->part_id=>array('view','id'=>$model->part_id),
	'Update',
);


?>

<h3>Ubah data part <?php echo $model->part_id; ?></h3>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>