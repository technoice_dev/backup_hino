<?php
/* @var $this MasterPartController */
/* @var $data MasterPart */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('part_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->part_id), array('view', 'id'=>$data->part_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('part_name')); ?>:</b>
	<?php echo CHtml::encode($data->part_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('part_no')); ?>:</b>
	<?php echo CHtml::encode($data->part_no); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pricelist')); ?>:</b>
	<?php echo CHtml::encode($data->pricelist); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('km1th')); ?>:</b>
	<?php echo CHtml::encode($data->km1th); ?>
	<br />


</div>