<?php
/* @var $this MasterPartController */
/* @var $model MasterPart */

$this->breadcrumbs=array(
	'Master Parts'=>array('index'),
	$model->part_id,
);

?>
<h1>View MasterPart</h1>
<a href="<?= Yii::app()->baseUrl ;?>/Cpk/masterPart" class="btn btn-primary">List Part</a>
<br>
<?php
    foreach(Yii::app()->user->getFlashes() as $key => $message) {
?>
<div class="alert alert-<?=$key; ?>" role="alert">
  <?=$message; ?>
</div>
<?php } ?>
<br>
<div class="card">
    <div class="card-body">
        <table class="table">
            <tr>
                <td>ID Part</td>
                <td><?= $model->part_id ?></td>
            </tr>
            <tr>
                <td>Nama Part</td>
                <td><?= $model->part_name ?></td>
            </tr>
            <tr>
                <td>Nomor Part</td>
                <td><?= $model->part_no ?></td>
            </tr>
            <tr>
                <td>Category</td>
                <td><?= $model->category ?></td>
            </tr>
            <tr>
                <td>Harga Part</td>
                <td><?= number_format($model->pricelist,0,',','.') ?></td>
            </tr>
        </table>
    </div>
</div>
