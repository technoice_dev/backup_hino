<?php
/* @var $this InvPowerController */
/* @var $model InvPower */

$this->breadcrumbs=array(
	'Inv Powers'=>array('index'),
	$model->inv_id,
);

$this->menu=array(
	array('label'=>'List InvPower', 'url'=>array('index')),
	array('label'=>'Create InvPower', 'url'=>array('create')),
	array('label'=>'Update InvPower', 'url'=>array('update', 'id'=>$model->inv_id)),
	array('label'=>'Delete InvPower', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->inv_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage InvPower', 'url'=>array('admin')),
);
?>

<h1>View Mechanic</h1>
<a href="<?= Yii::app()->baseUrl ;?>/Cpk/invPower" class="btn btn-primary">Investment Power List</a>
<br>
<?php
    foreach(Yii::app()->user->getFlashes() as $key => $message) {
?>
<div class="alert alert-<?=$key; ?>" role="alert">
  <?=$message; ?>
</div>
<?php } ?>
<br>
<div class="card">
    <div class="card-body">
        <table class="table">
            <tr>
                <td>Investment ID</td>
                <td><?= $model->inv_id ?></td>
            </tr>
            <tr>
                <td>Investment Item</td>
                <td><?= $model->inv_item ?></td>
            </tr>
            <tr>
                <td>Invesment Price</td>
                <td><?= $model->inv_price ?></td>
            </tr>
            <tr>
                <td>Investment Qty</td>
                <td><?= $model->inv_qty ?></td>
            </tr>
        </table>
    </div>
</div>
