

<?php
/* @var $this MasterPartController */
/* @var $model MasterPart */
/* @var $form CActiveForm */
?>
<style type="text/css">
    .invalid{
        outline-color: red;
    }
</style>
<div class="card">
    <div class="card-body">
        <div class="form">
            <div class="col-sm-5">
                <?php $form=$this->beginWidget('CActiveForm', array(
                       'id'=>'inv-power-form',
                        // Please note: When you enable ajax validation, make sure the corresponding
                        // controller action is handling ajax validation correctly.
                        // There is a call to performAjaxValidation() commented in generated controller code.
                        // See class documentation of CActiveForm for details on this.
                        'enableAjaxValidation'=>false,
                )); ?>

                    <?php if(Yii::app()->user->getFlash('error')){
                        echo '<div class="alert alert-danger" role="alert">';
                        echo '<button type="button" class="close" data-dismiss="alert" aria-label="Close">';
                        echo '<span aria-hidden="true">&times;</span>';
                        echo '</button>';
                        foreach(Yii::app()->user->getFlashes() as $key => $value){
                            echo $value.'</br>';
                        }
                        
                        echo '</div>';
                        
                    } ?>
                    
                    
                    
                    <?php echo $form->errorSummary($model); ?>
                    
                    <div class="row">
                        <?php echo $form->labelEx($model,'Investment ID'); ?>
                        <?php echo $form->textField($model,'inv_id',array('size'=>60,'maxlength'=>150,'class'=>'form-control','id'=>'nik')); ?>
                        <?php echo $form->error($model,'inv_id'); ?>
                    </div>

                    <div class="row">
                        <?php echo $form->labelEx($model,'Investment Item'); ?>
                        <?php echo $form->textField($model,'inv_item',array('size'=>20,'maxlength'=>20,'class'=>'form-control','id'=>'nama')); ?>
                        <?php echo $form->error($model,'inv_item'); ?>
                    </div>

                    <div class="row">
                        <?php echo $form->labelEx($model,'Investment Price'); ?>
                        <?php echo $form->textField($model,'inv_price',array('size'=>60,'maxlength'=>150,'class'=>'form-control','id'=>'alamat')); ?>
                        <?php echo $form->error($model,'inv_price'); ?>
                    </div>

                    <div class="row">
                        <?php echo $form->labelEx($model,'Investment Qty'); ?>
                        <?php echo $form->textField($model,'inv_qty',array('size'=>60,'maxlength'=>150,'class'=>'form-control','id'=>'no_telp')); ?>
                        <?php echo $form->error($model,'inv_qty'); ?>
                    </div>
		
                    <br>    
                    <div class="row buttons">
                        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class'=>'btn btn-primary')); ?>
                    </div>

                <?php $this->endWidget(); ?>
            </div>
        </div><!-- form -->
    </div>
</div>
