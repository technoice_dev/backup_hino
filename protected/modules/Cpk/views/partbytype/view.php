<?php
/* @var $this MasterPartController */
/* @var $model MasterPart */

$this->breadcrumbs=array(
	'Part By Type'=>array('index'),
	$model->typemtc_id,
);

?>
<h1>View Part By Type</h1>
<a href="<?= Yii::app()->baseUrl ;?>/Cpk/partbytype" class="btn btn-primary">List Part</a>
<br>
<?php
    foreach(Yii::app()->user->getFlashes() as $key => $message) {
?>
<div class="alert alert-<?=$key; ?>" role="alert">
  <?=$message; ?>
</div>
<?php } ?>
<br>
<div class="card">
    <div class="card-body">
        <table class="table">
            <tr>
                <td>ID</td>
                <td><?= $model->typemtc_id ?></td>
            </tr>
            <tr>
                <td>Unit</td>
                <td><?= (isset($model->unit->unit_name) ? $model->unit->unit_name : '') ?></td>
            </tr>
            <tr>
                <td>Part</td>
                <td><?= (isset($model->part->part_name) ? $model->part->part_name : '') ?></td>
            </tr>
            <tr>
                <td>Qty</td>
                <td><?= $model->qty ?></td>
            </tr>
            <tr>
                <td>Kilometer Interval</td>
                <td><?= $model->kminterval ?></td>
            </tr>
            <tr>
                <td>Probabilitas</td>
                <td><?= $model->probabilitas ?></td>
            </tr>
        </table>
    </div>
</div>
