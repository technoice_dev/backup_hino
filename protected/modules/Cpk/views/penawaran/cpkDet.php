<div class="row">
    <h2>Perhitungan CPK</h2>
</div>
<style type="text/css">
     .table-bordered{
        border:none;
        
    }
</style>
<div class="row">
    <div class="col">
    </div>
</div>
<div class="row">
    <div class="panel panel-default col-sm-8">
        <div class="panel-body">
            <table class="table table-bordered ">
                <tr>
                    <td>Jumlah Unit</td>
                    <td>: <?= $jml_unit?></td>
                </tr>
                <tr>
                    <td>Diskon</td>
                    <td>: <?= $diskon?></td>
                </tr>
                <tr>
                    <td>Working Days</td>
                    <td>: <?= $days?></td>
                </tr>
                <tr>
                    <td>Mileage</td>
                    <td>: <?= $mileage?></td>
                </tr>
                <tr>
                    <td>Kontrak</td>
                    <td>: <?= $lamaKontrak?></td>
                </tr>
            </table>
        </div>
    </div>
</div>

<div class="row">
    <div class="panel panel-default col-sm-6">
        <div class="panel-body">
            <table class="table table-bordered ">
                <thead>
                    <tr>
                        <th>Item</th>
                        <th>Sub Total Cost</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Preventive Maintenance Part</td>
                        <td><?= number_format($preventive)?></td>
                    </tr>
                    <tr>
                        <td>Repair Part</td>
                        <td><?= number_format($periodic)?></td>
                    </tr>
                    <tr>
                        <td>Total Maintenance Part</td>
                        <td><?= number_format($partTotal)?></td>
                    </tr>
                    <tr>
                        <td>CPK</td>
                        <td><?= number_format($partCpk)?></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class=" col col-sm-3"></div>
    <div class="col-sm-6">
        <div class="row">
            <div class="panel panel-default">
                <div class="panel-body">
                    <table class="table table-bordered ">
                        <thead>
                            <tr>
                                <th>Item</th>
                                <th>Sub Total Cost</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Tool & Equipment</td>
                                <td><?= number_format($invTotal)?></td>
                            </tr>
                            <tr>
                                <td>CPK</td>
                                <td><?= number_format($invCpk)?></td>
                                
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="panel panel-default">
                <div class="panel-body">
                <table class="table table-bordered ">
                        <thead>
                            <tr>
                                <th>Senior</th>
                                <th>Junior</th>
                                <th>Foreman</th>
                                <th>Staff W/H</th>
                                <th>Leader</th>
                                <th>Other</th>
                                <th>Total Cost</th>
                                <th>CPK</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><?=$seniorMechanic;?></td>
                                <td><?=$juniorMechanic?></td>
                                <td><?=$jumlahForeman?></td>
                                <td><?=$jumlahStaffWh?></td>
                                <td><?=$jumlahLeader?></td>
                                <td><?=$jumlahOther?></td>
                                <td><?=number_format($manpower) ?></td>
                                <td><?=number_format($manpower/($mileage*12*$lamaKontrak*$jml_unit)) ?></td>
                            </tr>
                            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    
</div>
<div class="row">
    <div class="panel panel-default">
        <div class="panel-body">
            <?php $c = 1; 
            for($l = 1 ; $l<$lamaKontrak+1 ; $l++){
                $z[$l] = 0;
                $s[$l] = $z[$l];
                $m[$l] = $z;[$l];
                
            }
            for($i = 1 ; $i<=$lamaKontrak;$i++){ ?>
            <h3>Tahun Ke-<?=$i?></h3>
            <table class="table ">
                <thead>
                    <tr>
                        <th>Part Name</th>
                        <th>Part Number</th>
                        <th>Qty</th>
                        <th>Pricelist</th>
                        <th>Price After Discount</th>
                        <th>Interval Change Km</th>
                        <th>Asumsi Mileage</th>
                        <th>Frek</th>
                        <th>Probability</th>
                        <th>Harga Tahun <?=$i ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php $j = 1; foreach($invDetPart as $row){
                        $frek_ganti = $row->kminterval / ($mileage * 12);
                        $upPrice = $row->part->category == 'periodic' ? 1.1 : 1.08;
                        $j++;

                            if($i > 1){
                                $k = $i - 1;
                                $m[$i][$j] = $m[$k][$j] * $upPrice;
                                
                            } else {
                                $m[$i][$j] = $row->part->pricelist;
                                
                            }
                            $final_price = ($m[$i][$j] - (($m[$i][$j]/100) * $diskon))* $row->qty * $row->probability * $frek_ganti;
                        ?>
                    <tr>
                        <td><?=$row->part->part_name?></td>
                        <td><?=$row->part->part_no?></td>
                        <td><?=$row->qty?></td>
                        <td><?= number_format($m[$i][$j])?></td>
                        <td><?= number_format($m[$i][$j] - (($m[$i][$j]/100) * $diskon) )?></td>
                        <td><?= number_format($row->kminterval)?></td>
                        <td><?= number_format($mileage*12)?></td>
                        <td><?= round($frek_ganti,1)?></td>
                        <td><?=$row->probability?></td>
                        <td><?= number_format($final_price) ?></td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
            <?php $c++; } ?>
        </div>
    </div>
</div>
<div class="row">
    <?= Yii::app()->user->getFlash('save_inv') ?>
</div>
<script>
function checkCb(){
    var inv = document.getElementById('inv').checked;
    var prdc = document.getElementById('prdc').checked;
    var pvt = document.getElementById('pvt').checked;
    var mpw = document.getElementById('mpw').checked;
    var invCost = "<?= round($invTotal)?>";
    var prdcCost = "<?= round($periodic)?>";
    var pvtCost = "<?= round($preventive)?>";
    var mpwCost = "<?= round(0)?>";
    if(inv == true ){
        document.getElementById('invCost').value =  invCost;
    } else {
        document.getElementById('invCost').value =  0;
    }
    if(prdc == true){
        document.getElementById('prdcCost').value = prdcCost;
    } else {
        document.getElementById('prdcCost').value = 0;
    }
    if(pvt == true){
        document.getElementById('pvtCost').value = pvtCost;
    } else {
        document.getElementById('pvtCost').value = 0;
    }
    if(mpw == true){
        document.getElementById('mpwCost').value = mpwCost;
    } else {
        document.getElementById('mpwCost').value = 0;
    }
}
document.querySelector('td.cbClass');
</script>
