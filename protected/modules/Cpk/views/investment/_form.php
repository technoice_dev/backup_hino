

<?php
/* @var $this MasterPartController */
/* @var $model MasterPart */
/* @var $form CActiveForm */
?>
<style type="text/css">
    .invalid{
        outline-color: red;
    }
</style>
<div class="card">
    <div class="card-body">
        <div class="form">
            
                <?php $form=$this->beginWidget('CActiveForm', array(
                       'id'=>'investment-form',
                        // Please note: When you enable ajax validation, make sure the corresponding
                        // controller action is handling ajax validation correctly.
                        // There is a call to performAjaxValidation() commented in generated controller code.
                        // See class documentation of CActiveForm for details on this.
                        'enableAjaxValidation'=>false,
                )); ?>

                    <?php if(Yii::app()->user->getFlash('error')){
                        echo '<div class="alert alert-danger" role="alert">';
                        echo '<button type="button" class="close" data-dismiss="alert" aria-label="Close">';
                        echo '<span aria-hidden="true">&times;</span>';
                        echo '</button>';
                        foreach(Yii::app()->user->getFlashes() as $key => $value){
                            echo $value.'</br>';
                        }
                        
                        echo '</div>';
                        
                    } ?>
                    
                    
                    
                    <?php echo $form->errorSummary($model); ?>
                    

                    <div class="form-group row">
                        <?php echo $form->labelEx($model,'Investment Item',array('class'=>'col-sm-2 col-form-label')); ?>
                        <div class="col-sm-6">
                            <?php echo $form->textField($model,'inv_item',array('size'=>60,'maxlength'=>150,'class'=>'form-control','id'=>'inv_id')); ?>
                        </div>
                    </div>

                    <div class="form-group row">
                        <?php echo $form->labelEx($model,'Investment Price',array('class'=>'col-sm-2 col-form-label')); ?>
                        <div class="col-sm-6">
                            <?php echo $form->textField($model,'inv_price',array('class'=>'form-control numeric-field','type'=>'number')); ?>
                        </div>
                    </div>

                    <div class="form-group row">
                        <?php echo $form->labelEx($model,'Investment Qty',array('class'=>'col-sm-2 col-form-label')); ?>
                        <div class="col-sm-6">
                            <?php echo $form->numberField($model,'inv_qty',array('class'=>'form-control','type'=>'number'));?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <?php echo $form->labelEx($model,'Investment Category',array('class'=>'col-sm-2 col-form-label')); ?>
                        <div class="col-sm-6">
                            <?php echo $form->dropDownList($model,'inv_category',array('general'=>'General','measuring'=>'Measuring','power'=>'Power','others'=>'Others','safety'=>'Safety','sst_dutro'=>'Sst Dutro','sst_ranger'=>'Sst Ranger'),array('class'=>'form-control')); ?>
                        </div>
                    </div>
                    <br>    
                    <div class="form-group row buttons">
                        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class'=>'btn btn-primary')); ?>
                    </div>

                <?php $this->endWidget(); ?>
            
        </div><!-- form -->
    </div>
</div>