<?php
/* @var $this InvMeasuringController */
/* @var $model InvMeasuring */

$this->breadcrumbs=array(
	'Investment'=>array('index'),
	$model->inv_id=>array('view','id'=>$model->inv_id),
	'Update',
);


?>

<h1>Update Investment <?php echo $model->inv_id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>