<?php
/* @var $this MasterPartController */
/* @var $model MasterPart */

$this->breadcrumbs=array(
	'Manpower'=>array('index'),
	$model->mp_id,
);

?>
<h1>View Manpower</h1>
<a href="<?= Yii::app()->baseUrl ;?>/Cpk/manpower" class="btn btn-primary">List Manpower</a>
<br>
<?php
    foreach(Yii::app()->user->getFlashes() as $key => $message) {
?>
<div class="alert alert-<?=$key; ?>" role="alert">
  <?=$message; ?>
</div>
<?php } ?>
<br>
<div class="card">
    <div class="card-body">
        <table class="table">
            <tr>
                <td>Manpower ID</td>
                <td><?= $model->mp_id ?></td>
            </tr>
            <tr>
                <td>Bsc Salary</td>
                <td><?= number_format($model->bsc_sallary) ?></td>
            </tr>
            <tr>
                <td>PPh</td>
                <td><?= number_format($model->pph) ?></td>
            </tr>
            <tr>
                <td>Laundry</td>
                <td><?= number_format($model->laundry) ?></td>
            </tr>
            <tr>
                <td>Transport</td>
                <td><?= number_format($model->transport) ?></td>
            </tr>
            <tr>
                <td>Meal</td>
                <td><?= number_format($model->meal) ?></td>
            </tr>
            <tr>
                <td>Medical</td>
                <td><?= number_format($model->medical) ?></td>
            </tr>
            <tr>
                <td>Mobile Phone</td>
                <td><?= number_format($model->mobile_phone) ?></td>
            </tr>
            <tr>
                <td>BPJS</td>
                <td><?= number_format($model->bpjs) ?></td>
            </tr>
            <tr>
                <td>THR</td>
                <td><?= number_format($model->thr) ?></td>
            </tr>
            <tr>
                <td>Uniform + Safety</td>
                <td><?= number_format($model->uni_safety) ?></td>
            </tr>
            <tr>
                <td>Hardship</td>
                <td><?= number_format($model->hardship) ?></td>
            </tr>
            <tr>
                <td>Overtime</td>
                <td><?= number_format($model->overtime) ?></td>
            </tr>
            <tr>
                <td>Overtime</td>
                <td><?= number_format($model->other_pay) ?></td>
            </tr>
            <tr>
                <td>Category</td>
                <td><?= $model->kategory ?></td>
            </tr>
        </table>
    </div>
</div>
