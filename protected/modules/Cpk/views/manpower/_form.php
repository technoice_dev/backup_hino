<?php
?>
<style type="text/css">
    .invalid{
        outline-color: red;
    }
</style>
<div class="card">
    <div class="card-body">
        <div class="form">
            
                <?php $form=$this->beginWidget('CActiveForm', array(
                       'id'=>'manpower-form',
                        // Please note: When you enable ajax validation, make sure the corresponding
                        // controller action is handling ajax validation correctly.
                        // There is a call to performAjaxValidation() commented in generated controller code.
                        // See class documentation of CActiveForm for details on this.
                        'enableAjaxValidation'=>false,
                )); ?>

                    <?php if(Yii::app()->user->getFlash('error')){
                        echo '<div class="alert alert-danger" role="alert">';
                        echo '<button type="button" class="close" data-dismiss="alert" aria-label="Close">';
                        echo '<span aria-hidden="true">&times;</span>';
                        echo '</button>';
                        foreach(Yii::app()->user->getFlashes() as $key => $value){
                            echo $value.'</br>';
                        }
                        
                        echo '</div>';
                        
                    } ?>
                    
                    
                    
                    <?php echo $form->errorSummary($model); ?>
                    
                    <div class="form-group row">
                        <?php echo $form->labelEx($model,'Bsc Salary',array('class'=>'col-sm-2 col-form-label')); ?>
                        <div class="col-sm-6">
                            <?php echo $form->textField($model,'bsc_sallary',array('size'=>60,'maxlength'=>100,'class'=>'form-control numeric-field','id'=>'part_name','type'=>'number')); ?>
                        </div>
                        
                    </div>

                    <div class="form-group row">
                        <?php echo $form->labelEx($model,'Pph',array('class'=>'col-sm-2 col-form-label')); ?>
                        <div class="col-sm-6">
                            <?php echo $form->textField($model,'pph',array('size'=>20,'maxlength'=>20,'class'=>'form-control numeric-field','id'=>'part_no','type'=>'number')); ?>
                        </div>
                    </div>

                    <div class="form-group row">
                        <?php echo $form->labelEx($model,'Laundry',array('class'=>'col-sm-2 col-form-label')); ?>
                        <div class="col-sm-6">
                            <?php echo $form->textField($model,'laundry',array('class'=>'form-control numeric-field','id'=>'pricelist','type'=>'number')); ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <?php echo $form->labelEx($model,'Transport',array('class'=>'col-sm-2 col-form-label')); ?>
                        <div class="col-sm-6">
                            <?php echo $form->textField($model,'transport',array('class'=>'form-control numeric-field','id'=>'km1th','type'=>'number')); ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <?php echo $form->labelEx($model,'Meal',array('class'=>'col-sm-2 col-form-label','type'=>'number')); ?>
                        <div class="col-sm-6">
                            <?php echo $form->textField($model,'meal',array('class'=>'form-control numeric-field')); ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <?php echo $form->labelEx($model,'Medical',array('class'=>'col-sm-2 col-form-label','type'=>'number')); ?>
                        <div class="col-sm-6">
                            <?php echo $form->textField($model,'medical',array('class'=>'form-control numeric-field')); ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <?php echo $form->labelEx($model,'Mobile Phone',array('class'=>'col-sm-2 col-form-label')); ?>
                        <div class="col-sm-6">
                            <?php echo $form->textField($model,'mobile_phone',array('class'=>'form-control numeric-field','type'=>'number')); ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <?php echo $form->labelEx($model,'BPJS',array('class'=>'col-sm-2 col-form-label')); ?>
                        <div class="col-sm-6">
                            <?php echo $form->textField($model,'bpjs',array('class'=>'form-control numeric-field','type'=>'number')); ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <?php echo $form->labelEx($model,'THR',array('class'=>'col-sm-2 col-form-label','type'=>'number')); ?>
                        <div class="col-sm-6">
                            <?php echo $form->textArea($model,'thr',array('class'=>'form-control numeric-field','type'=>'number')); ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <?php echo $form->labelEx($model,'Uniform + Safety',array('class'=>'col-sm-2 col-form-label')); ?>
                        <div class="col-sm-6">
                            <?php echo $form->textField($model,'uni_safety',array('class'=>'form-control numeric-field','type'=>'number')); ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <?php echo $form->labelEx($model,'Hardship',array('class'=>'col-sm-2 col-form-label')); ?>
                        <div class="col-sm-6">
                            <?php echo $form->textField($model,'hardship',array('class'=>'form-control numeric-field','type'=>'number')); ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <?php echo $form->labelEx($model,'Overtime',array('class'=>'col-sm-2 col-form-label')); ?>
                        <div class="col-sm-6">
                            <?php echo $form->textField($model,'overtime',array('class'=>'form-control numeric-field','type'=>'number')); ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <?php echo $form->labelEx($model,'Other Pay',array('class'=>'col-sm-2 col-form-label')); ?>
                        <div class="col-sm-6">
                            <?php echo $form->textField($model,'other_pay',array('class'=>'form-control numeric-field','type'=>'number')); ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <?php echo $form->labelEx($model,'Category',array('class'=>'col-sm-2 col-form-label')); ?>
                        <div class="col-sm-6">
                            <?php echo $form->dropDownList($model,'kategory',array('senior'=>'Senior','junior'=>'Junior','staff'=>'Staff Wh','foreman'=>'Foreman','leader'=>'Leader','other'=>'Other'),array('class'=>'form-control')); ?>
                        </div>
                    </div>
                    <br>    
                    <div class="form-group row buttons">
                        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class'=>'btn btn-primary')); ?>
                    </div>
                <?php $this->endWidget(); ?>
            
        </div><!-- form -->
    </div>
</div>
