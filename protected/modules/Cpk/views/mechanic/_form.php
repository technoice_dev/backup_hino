

<?php
/* @var $this MasterPartController */
/* @var $model MasterPart */
/* @var $form CActiveForm */
?>
<style type="text/css">
    .invalid{
        outline-color: red;
    }
</style>
<div class="card">
    <div class="card-body">
        <div class="form">
            <div class="col-sm-5">
                <?php $form=$this->beginWidget('CActiveForm', array(
                       'id'=>'mechanic-form',
                        // Please note: When you enable ajax validation, make sure the corresponding
                        // controller action is handling ajax validation correctly.
                        // There is a call to performAjaxValidation() commented in generated controller code.
                        // See class documentation of CActiveForm for details on this.
                        'enableAjaxValidation'=>false,
                )); ?>

                    <?php if(Yii::app()->user->getFlash('error')){
                        echo '<div class="alert alert-danger" role="alert">';
                        echo '<button type="button" class="close" data-dismiss="alert" aria-label="Close">';
                        echo '<span aria-hidden="true">&times;</span>';
                        echo '</button>';
                        foreach(Yii::app()->user->getFlashes() as $key => $value){
                            echo $value.'</br>';
                        }
                        
                        echo '</div>';
                        
                    } ?>
                    
                    
                    
                    <?php echo $form->errorSummary($model); ?>
                    
                    <div class="row">
                        <?php echo $form->labelEx($model,'NIK'); ?>
                        <?php echo $form->textField($model,'nik',array('size'=>60,'maxlength'=>150,'class'=>'form-control','id'=>'nik')); ?>
                        <?php echo $form->error($model,'nik'); ?>
                    </div>

                    <div class="row">
                        <?php echo $form->labelEx($model,'Nama'); ?>
                        <?php echo $form->textField($model,'nama',array('size'=>20,'maxlength'=>20,'class'=>'form-control','id'=>'nama')); ?>
                        <?php echo $form->error($model,'nama'); ?>
                    </div>

                    <div class="row">
                        <?php echo $form->labelEx($model,'Alamat'); ?>
                        <?php echo $form->textField($model,'alamat',array('size'=>60,'maxlength'=>150,'class'=>'form-control','id'=>'alamat')); ?>
                        <?php echo $form->error($model,'alamat'); ?>
                    </div>

										<div class="row">
                        <?php echo $form->labelEx($model,'No Telp'); ?>
                        <?php echo $form->textField($model,'no_telp',array('size'=>60,'maxlength'=>150,'class'=>'form-control','id'=>'no_telp')); ?>
                        <?php echo $form->error($model,'no_telp'); ?>
										</div>
										
										<div class="row">
                        <?php echo $form->labelEx($model,'Jabatan'); ?>
                        <?php echo $form->textField($model,'jabatan',array('size'=>60,'maxlength'=>150,'class'=>'form-control','id'=>'jabatan')); ?>
                        <?php echo $form->error($model,'jabatan'); ?>
                    </div>
                    <br>    
                    <div class="row buttons">
                        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class'=>'btn btn-primary')); ?>
                    </div>

                <?php $this->endWidget(); ?>
            </div>
        </div><!-- form -->
    </div>
</div>
