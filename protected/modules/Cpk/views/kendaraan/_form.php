
<style type="text/css">
    .invalid{
        outline-color: red;
    }
</style>
<div class="card">
    <div class="card-body">
        <div class="form">
            <div class="col-sm-5">
                <?php $form=$this->beginWidget('CActiveForm', array(
                       'id'=>'master-part-form',
                        // Please note: When you enable ajax validation, make sure the corresponding
                        // controller action is handling ajax validation correctly.
                        // There is a call to performAjaxValidation() commented in generated controller code.
                        // See class documentation of CActiveForm for details on this.
                        'enableAjaxValidation'=>false,
                )); ?>

                    <?php if(Yii::app()->user->getFlash('error')){
                        echo '<div class="alert alert-danger" role="alert">';
                        echo '<button type="button" class="close" data-dismiss="alert" aria-label="Close">';
                        echo '<span aria-hidden="true">&times;</span>';
                        echo '</button>';
                        foreach(Yii::app()->user->getFlashes() as $key => $value){
                            echo $value.'</br>';
                        }
                        
                        echo '</div>';
                        
                    } ?>
                    
                    
                    
                    <?php echo $form->errorSummary($model); ?>
                    
                    <div class="row">
                        <?php echo $form->labelEx($model,'No Kendaraan'); ?>
                        <?php echo $form->textField($model,'no_kendaraan',array('size'=>60,'maxlength'=>150,'class'=>'form-control','id'=>'no_kendaraan')); ?>
                        <?php echo $form->error($model,'no_kendaraan'); ?>
                    </div>

                    <div class="row">
                        <?php echo $form->labelEx($model,'Tipe Kendaraan'); ?>
                        <?php echo $form->textField($model,'tipe_kendaraan',array('size'=>20,'maxlength'=>20,'class'=>'form-control','id'=>'tipe_kendaraan')); ?>
                        <?php echo $form->error($model,'tipe_kendaraan'); ?>
                    </div>

                    <div class="row">
                        <?php echo $form->labelEx($model,'KM Kendaraan'); ?>
                        <?php echo $form->textField($model,'km_kendaraan',array('size'=>60,'maxlength'=>150,'class'=>'form-control','id'=>'km_kendaraan')); ?>
                        <?php echo $form->error($model,'km_kendaraan'); ?>
                    </div>

                    <br>    
                    <div class="row buttons">
                        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class'=>'btn btn-primary')); ?>
                    </div>

                <?php $this->endWidget(); ?>
            </div>
        </div><!-- form -->
    </div>
</div>
