<?php
/* @var $this KendaraanController */
/* @var $data Kendaraan */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('no_kendaraan')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->no_kendaraan), array('view', 'id'=>$data->no_kendaraan)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tipe_kendaraan')); ?>:</b>
	<?php echo CHtml::encode($data->tipe_kendaraan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('km_kendaraan')); ?>:</b>
	<?php echo CHtml::encode($data->km_kendaraan); ?>
	<br />


</div>