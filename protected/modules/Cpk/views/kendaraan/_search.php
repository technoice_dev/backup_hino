<style type="text/css">
	.invalid {
		outline-color: red;
	}
</style>
<div class="card">
	<div class="card-body">
		<div class="form">
			<div class="col-sm-5">
				<?php $form = $this->beginWidget('CActiveForm', array(
					'action' => Yii::app()->createUrl($this->route),
					'method' => 'get',
				)); ?>

				<?php if (Yii::app()->user->getFlash('error')) {
					echo '<div class="alert alert-danger" role="alert">';
					echo '<button type="button" class="close" data-dismiss="alert" aria-label="Close">';
					echo '<span aria-hidden="true">&times;</span>';
					echo '</button>';
					foreach (Yii::app()->user->getFlashes() as $key => $value) {
						echo $value . '</br>';
					}

					echo '</div>';
				} ?>



				<?php echo $form->errorSummary($model); ?>

				<div class="row">
					<?php echo $form->labelEx($model, 'No Kendaraan'); ?>
					<?php echo $form->textField($model, 'no_kendaraan', array('size' => 60, 'maxlength' => 150, 'class' => 'form-control', 'id' => 'no_kendaraan')); ?>
					<?php echo $form->error($model, 'no_kendaraan'); ?>
				</div>

				<div class="row">
					<?php echo $form->labelEx($model, 'Tipe Kendaraan'); ?>
					<?php echo $form->textField($model, 'tipe_kendaraan', array('size' => 20, 'maxlength' => 20, 'class' => 'form-control', 'id' => 'tipe_kendaraan')); ?>
					<?php echo $form->error($model, 'tipe_kendaraan'); ?>
				</div>

				<div class="row">
					<?php echo $form->labelEx($model, 'KM Kendaraan'); ?>
					<?php echo $form->textField($model, 'km_kendaraan', array('size' => 60, 'maxlength' => 150, 'class' => 'form-control', 'id' => 'km_kendaraan')); ?>
					<?php echo $form->error($model, 'km_kendaraan'); ?>
				</div>

				<br>
				<div class="row buttons">
					<?php echo CHtml::submitButton('Search'); ?>
				</div>

				<?php $this->endWidget(); ?>
			</div>
		</div><!-- form -->
	</div>
</div>