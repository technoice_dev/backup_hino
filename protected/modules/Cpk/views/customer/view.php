<?php
/* @var $this MasterPartController */
/* @var $model MasterPart */

$this->breadcrumbs=array(
	'Customer'=>array('index'),
	$model->id_cust,
);

?>
<h1>View Customer</h1>
<a href="<?= Yii::app()->baseUrl ;?>/Cpk/customer" class="btn btn-primary">List Customer</a>
<br>
<?php
    foreach(Yii::app()->user->getFlashes() as $key => $message) {
?>
<div class="alert alert-<?=$key; ?>" role="alert">
  <?=$message; ?>
</div>
<?php } ?>
<br>
<div class="card">
    <div class="card-body">
        <table class="table">
            <tr>
                <td>ID Customer</td>
                <td><?= $model->id_cust ?></td>
            </tr>
            <tr>
                <td>Customer Name</td>
                <td><?= $model->cust_name ?></td>
            </tr>
            <tr>
                <td>Phone</td>
                <td><?= $model->phone ?></td>
            </tr>
            <tr>
                <td>Addres</td>
                <td><?= $model->addres ?></td>
            </tr>
        </table>
    </div>
</div>
