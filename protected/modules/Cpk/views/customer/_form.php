<?php
?>
<style type="text/css">
    .invalid{
        outline-color: red;
    }
</style>
<div class="card">
    <div class="card-body">
        <div class="form">
            
                <?php $form=$this->beginWidget('CActiveForm', array(
                       'id'=>'customer-form',
                        // Please note: When you enable ajax validation, make sure the corresponding
                        // controller action is handling ajax validation correctly.
                        // There is a call to performAjaxValidation() commented in generated controller code.
                        // See class documentation of CActiveForm for details on this.
                        'enableAjaxValidation'=>false,
                )); ?>

                    <?php if(Yii::app()->user->getFlash('error')){
                        echo '<div class="alert alert-danger" role="alert">';
                        echo '<button type="button" class="close" data-dismiss="alert" aria-label="Close">';
                        echo '<span aria-hidden="true">&times;</span>';
                        echo '</button>';
                        foreach(Yii::app()->user->getFlashes() as $key => $value){
                            echo $value.'</br>';
                        }
                        
                        echo '</div>';
                        
                    } ?>
                    
                    
                    
                    <?php echo $form->errorSummary($model); ?>
                    
                    <div class="form-group row">
                        <?php echo $form->labelEx($model,'Customer Name',array('class'=>'col-sm-2 col-form-label')); ?>
                        <div class="col-sm-6">
                            <?php echo $form->textField($model,'cust_name',array('size'=>60,'maxlength'=>100,'class'=>'form-control','id'=>'part_name')); ?>
                        </div>
                        
                    </div>

                    <div class="form-group row">
                        <?php echo $form->labelEx($model,'Customer Address',array('class'=>'col-sm-2 col-form-label')); ?>
                        <div class="col-sm-6">
                            <?php echo $form->textArea($model,'addres',array('size'=>150,'maxlength'=>500,'class'=>'form-control','id'=>'part_no')); ?>
                        </div>
                    </div>

                    <div class="form-group row">
                        <?php echo $form->labelEx($model,'Phone',array('class'=>'col-sm-2 col-form-label')); ?>
                        <div class="col-sm-6">
                            <?php echo $form->numberField($model,'phone',array('class'=>'form-control','id'=>'pricelist', 'type'=>'number')); ?>
                        </div>
                    </div>
                    <br>    
                    <div class="form-group row buttons">
                        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class'=>'btn btn-primary')); ?>
                    </div>

                <?php $this->endWidget(); ?>
            
        </div><!-- form -->
    </div>
</div>
