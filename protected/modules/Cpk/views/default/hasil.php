<div class="row">
    <h2>Perhitungan CPK</h2>
</div>
<style type="text/css">
    .table-bordered{
        border:none;
        
    }
</style>
<?php 
$session = Yii::app()->session;
if($session['invId'] !== null ){
    
    
    $type = $session['type_qty']? $session['type_qty'] : null;
    
}

    
?>
<div class="row">
    <div class="col">
    <form action="<?= Yii::app()->baseUrl; ?>/Cpk/default/cetakInvoice" method="post" id="cpk-cost-value">
        <input type="hidden" name="unit" value="<?= $jml_unit?>">
        <input type="hidden" name="tipe" value="<?= $tipe?>">
        <input type="hidden" name="diskon" value="<?= $diskon?>">
        <input type="hidden" name="mileage" value="<?= $mileage?>">
        <input type="hidden" name="kontrak" value="<?= $lamaKontrak?>">
        <input type="hidden" name="preventive" value="0" id="pvtCost">
        <input type="hidden" name="periodic" value="0" id="prdcCost">
        <input type="hidden" name="inv" value="0" id="invCost">
        <input type="hidden" name="senior" value="<?= $seniorMechanic?>">
        <input type="hidden" name="junior" value="<?= $juniorMechanic?>">
        <input type="hidden" name="foreman" value="<?= $jumlahForeman?>">
        <input type="hidden" name="staffWh" value="<?= $jumlahStaffWh?>">
        <input type="hidden" name="leader" value="<?= $jumlahLeader?>">
        <input type="hidden" name="other" value="<?= $jumlahOther?>">
        <input type="hidden" name="manpower" value="0" id="mpwCost">
        <input type="hidden" name="day" value="<?= $days?>">
        <input type="hidden" name="customer" value="<?= $customer?>">
        <input type="hidden" name="km_day" value="<?= $kmPerHari?>">
        <br>
        <button type="submit" class="btn btn-primary">Tambahkan</button>
        <br>
    </form>
    </div>
    <div class=" col">
        <a class="btn btn-info" href="<?= Yii::app()->baseUrl; ?>/Cpk/default/cpkCalculate">Ulangi Simulasi</a>
    </div>
    <?php

    if($session['invId'] !== null) {?>
    <div class="col">
        <a href="<?= Yii::app()->baseUrl; ?>/Cpk/default/printInvoice?invID=<?= $session['invId']?>" class="btn btn-success">Print Invoice</a>
        
    </div>
    <?php } ?>
</div>
<div class="row">
    <div class="panel panel-default col-sm-8">
        <div class="panel-body">
            <table class="table table-bordered ">
                <tr>
                    <td>Jumlah Unit</td>
                    <td>: <?= $jml_unit?></td>
                </tr>
                <tr>
                    <td>Diskon</td>
                    <td>: <?= $diskon?></td>
                </tr>
                <tr>
                    <td>Working Days</td>
                    <td>: <?= $days?></td>
                </tr>
                <tr>
                    <td>Mileage</td>
                    <td>: <?= $mileage?></td>
                </tr>
                <tr>
                    <td>Kontrak</td>
                    <td>: <?= $lamaKontrak?></td>
                </tr>
            </table>
        </div>
    </div>
    <div><span style="color: red;" class="message-error"></span></div>
</div>

<div class="row">
    <div class="panel panel-default col-sm-6">
        <div class="panel-body">
            <table class="table table-bordered ">
                <thead>
                    <tr>
                        <th>Item</th>
                        <th>Sub Total Cost</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Preventive Maintenance Part</td>
                        <td><?= number_format($preventive)?></td>
                        <td style="border:none;"class="cbCost" ><input class="cost-cb" type="checkbox" id="pvt" value="pvt" onclick="checkCb();"></td>
                    </tr>
                    <tr>
                        <td>Repair Part</td>
                        <td><?= number_format($periodic)?></td>
                        <td style="border:none;" class="cbCost" ><input type="checkbox" class="cost-cb" id="prdc" value="prdc" onclick="checkCb();"></td>
                    </tr>
                    <tr>
                        <td>Total Maintenance Part</td>
                        <td><?= number_format($partTotal)?></td>
                    </tr>
                    <tr>
                        <td>CPK</td>
                        <td><?= number_format($partCpk)?></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="row">
    
</div>
<div class="row">
    <div class="panel panel-default">
        <div class="panel-body">
            <?php $total_part = 0; $c = 1; 
            for($l = 1 ; $l<$lamaKontrak ; $l++){
                $z[$l] = 0;
                $s[$l] = $z[$l];
                $m[$l] = $z;[$l];
                
            }
            for($i = 1 ; $i<=$lamaKontrak;$i++){ ?>
            <h3>Tahun Ke-<?=$i?></h3>
            <table class="table ">
                <thead>
                    <tr>
                        <th>Part Name</th>
                        <th>Part Number</th>
                        <th>Qty</th>
                        <th>Pricelist</th>
                        <th>Price After Discount</th>
                        <th>Interval Change Km</th>
                        <th>Asumsi Mileage</th>
                        <th>Frek</th>
                        <th>Probability</th>
                        <th>Harga Tahun <?=$i ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php $j = 1;$total_harga = 0; foreach($dataPeriodic as $row){
                        $j++;
                        $frek_ganti = ($mileage*12)/ $row->kminterval;
                            if($i > 1){
                                $k = $i - 1;
                                $m[$i][$j] = $m[$k][$j] * 1.1;
                                
                            } else {
                                $m[$i][$j] = $row->part->pricelist;
                                
                            }
                            $final_price = ($m[$i][$j] - (($m[$i][$j]/100) * $diskon))* $row->qty * $row->probabilitas * $frek_ganti;
                            $total_harga += $final_price;
                        ?>
                    <tr>
                        <td><?=$row->part->part_name?></td>
                        <td><?=$row->part->part_no?></td>
                        <td><?=$row->qty?></td>
                        <td><?= number_format($m[$i][$j])?></td>
                        <td><?= number_format($m[$i][$j] - (($m[$i][$j]/100) * $diskon)  )?></td>
                        <td><?= number_format($row->kminterval)?></td>
                        <td><?= number_format($mileage*12)?></td>
                        <td><?= round($frek_ganti,2)?></td>
                        <td><?=$row->probabilitas?></td>
                        <td><?= number_format($final_price)  ?></td>
                    </tr>
                    <?php } ?>
                    <?php foreach($dataPreventive as $row){
                            $frek_ganti = ($mileage*12) / $row->kminterval;
                            $j++;
                            if($i > 1){
                                $k = $i - 1;
                                $m[$i][$j] = $m[$k][$j] * 1.1;
                                
                                } else {
                                    $m[$i][$j] = $row->part->pricelist;
                                    
                                }
                                $final_price_per = ($m[$i][$j] - (($m[$i][$j]/100) * $diskon))* $row->qty * $row->probabilitas * $frek_ganti;
                                $total_harga += $final_price_per;
                            ?>
                        <tr>
                            <td><?=$row->part->part_name?></td>
                            <td><?=$row->part->part_no?></td>
                            <td><?=$row->qty?></td>
                            <td><?= number_format($m[$i][$j])?></td>
                            <td><?= number_format($m[$i][$j] - (($m[$i][$j]/100) * $diskon) )?></td>
                        <td><?= number_format($row->kminterval)?></td>
                        <td><?= number_format($mileage*12)?></td>
                        <td><?= number_format($frek_ganti,2)?></td>
                        <td><?=$row->probabilitas?></td>
                        <td><?= number_format($final_price_per) ?></td>
                    </tr>
                    
                    <?php } ?>
                    <tr>
                        <td colspan="9">Total Harga Tahun ke <?= $i;?></td>
                        <td><?= number_format(round($total_harga)) ?></td>
                    </tr>
                </tbody>
            </table>
            <?php $total_part += $total_harga; $c++; } ?>
        </div>
    </div>
</div>
<div class="row">
    <?= Yii::app()->user->getFlash('save_inv') ?>
</div>
<script>
function checkCb(){
    var prdc = document.getElementById('prdc').checked;
    var pvt = document.getElementById('pvt').checked;
    var invCost = "<?= round($invTotal)?>";
    var prdcCost = "<?= round($periodic)?>";
    var pvtCost = "<?= round($preventive)?>";
    var mpwCost = "<?= round($manpower['total'])?>";
    if(prdc == true){
        document.getElementById('prdcCost').value = prdcCost;
    } else {
        document.getElementById('prdcCost').value = 0;
    }
    if(pvt == true){
        document.getElementById('pvtCost').value = pvtCost;
    } else {
        document.getElementById('pvtCost').value = 0;
    }
}
document.querySelector('td.cbClass');
</script>
