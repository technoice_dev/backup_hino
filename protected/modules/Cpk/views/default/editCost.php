<form action="<?= Yii::app()->baseUrl?>/Cpk/default/editCostInvoice?invID=<?=$_GET['invID']?>" method="POST">
    <div class="form">
        <div class="panel panel-default col-md-12">
            <div class="panel-body">
                <div class="form-group row">
                    <div class="col col-md-2">
                        <label>Senior Mechanic</label>
                    </div>
                    <div class="col col-md-9">
                        <div class="input-group">
                            <div class="input-group-area">
                                <input type="number" class="form-control" name="senior" value="<?= $invoice->jml_senior?>">
                            </div>
                            <div class="input-group-icon">Orang</div>
                        </div>
                        <span id="error-senior" style="color:red;"></span>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col col-md-2">
                        <label>Junior Mechanic</label>
                    </div>
                    <div class="col col-md-9">
                        <div class="input-group">
                            <div class="input-group-area">
                                <input type="number" class="form-control" name="junior" value="<?= $invoice->jml_junior?>">
                            </div>
                            <div class="input-group-icon">Orang</div>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col col-md-2">
                        <label>Foreman</label>
                    </div>
                    <div class="col col-md-9">    
                        <div class="input-group">
                            <div class="input-group-area">
                                <input type="number" class="form-control" name="foreman" value="<?= $invoice->jml_foreman?>">
                            </div>
                            <div class="input-group-icon">Orang</div>
                        </div>
                        <span id="error-foreman" style="color:red;"></span>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col col-md-2">
                        <label>Staff W/H</label>
                    </div>
                    <div class="col col-md-9">
                        <div class="input-group">
                            <div class="input-group-area">
                                <input type="number" class="form-control" name="staffWh" value="<?= $invoice->jml_staff?>">
                            </div>
                            <div class="input-group-icon">Orang</div>
                        </div>
                        <span id="error-staff" style="color:red;"></span>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col col-md-2">
                        <label>Leader</label>
                    </div>
                    <div class="col col-md-9">
                        <div class="input-group">
                            <div class="input-group-area">
                                <input type="number" class="form-control" name="leader" value="<?= $invoice->jml_leader?>">
                            </div>
                            <div class="input-group-icon">Orang</div>
                        </div>
                        <span id="error-leader" style="color:red;"></span>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col col-md-2">
                        <label>Other</label>
                    </div>
                    <div class="col col-md-9">    
                        <div class="input-group">
                            <div class="input-group-area">
                                <input type="number" class="form-control" name="other" value="<?= $invoice->jml_other?>">
                            </div>
                            <div class="input-group-icon">Orang</div>
                        </div>
                        <span id="error-disc" style="color:red;"></span>
                    </div>
                </div>
                <div class="card-header">
                    <label>Investment</label>
                    <span id="error-inv" style="color:red;"></span>
                </div>
                <div class="panel panel-default col-md-17">
                    <div class="panel-body">
                        <div class="form-group row">
                            <div class="col col-md-15" id="inv-list">
                                <div class="col col-md-6">
                                <?php
                                    $investment = json_decode($invoice->inv_selected);
                                ?>
                                    <input type="checkbox" name="inv[]" value="general" <?= isset($investment) && in_array('general',$investment) ? 'checked' : '' ?>>
                                    <label>Investment General</label>
                                    <br>
                                    <input type="checkbox" name="inv[]" value="measuring" <?= isset($investment) && in_array('measuring',$investment) ? 'checked' : '' ?>>
                                    <label>Investment Measuring</label>
                                    <br>
                                    <input type="checkbox" name="inv[]" value="others" <?= isset($investment) && in_array('others',$investment) ? 'checked' : '' ?>>
                                    <label>Investment Others</label>
                                    <br>
                                </div>
                                <div class="col col-md-6">
                                    <input type="checkbox" name="inv[]" value="power" <?= isset($investment) && in_array('power',$investment) ? 'checked' : '' ?>>
                                    <label>Investment Power</label>
                                    <br>
                                    <input type="checkbox" name="inv[]" value="safety" <?= isset($investment) && in_array('safety',$investment) ? 'checked' : '' ?>>
                                    <label>Investment Safety</label>
                                    <br>
                                    <input type="checkbox" name="inv[]" value="sst_dutro" <?= isset($investment) && in_array('sst_dutro',$investment) ? 'checked' : '' ?>>
                                    <label>Investment Sst Dutro</label>
                                    <br>
                                    <input type="checkbox" name="inv[]" value="sst_ranger" <?= isset($investment) && in_array('sst_ranger',$investment) ? 'checked' : '' ?>>
                                    <label>Investment Sst Ranger</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div>
            <button type="submit" class="btn btn-success">Submit</button>
        </div>
    </div>
</form>