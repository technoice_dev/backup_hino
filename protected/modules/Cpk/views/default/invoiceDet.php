<style type="text/css">
    @page {
        size: 7in 9in;
        margin: 10mm 16mm 10mm 16mm;
    }
    td{
        padding:2px;
        border-style: solid;
        border-width: 1px;
    }
    th{
        padding:2px;
        border-style: solid;
        border-width: 1px;
    }
    .divider{
       width:30px;
       height:30px;
    }
    @media print{
        .print-btn{
            visibility:hidden;
        }
    }
</style>
<div class="row">
        <center><h1>Penawaran</h1></center>
        <hr>
        <?php $unitList = ''; foreach($invoiceDet as $key => $val) {
            
                if($key > 0){
                    $unitList .= ',';
                }
                $unitList .= $val->unit->unit_name;
            }?>
        <p><?= strtoupper($invoiceData->dealer->dealer_name) ?><br>
        PENAWARAN FULL MAINTENANCE CONTENT (FMC) UNTUK <?= $invoiceData->customer->cust_name?><br>
        HINO <?=$unitList ?>
        <hr>
        <table width="100%">
        <tr style="margin: inherit;">
            <td align="center" colspan="<?= isset($_SESSION['type_qty']) ? $_SESSION['type_qty']*4+2 : null ?>"> <h4><strong>PENAMBAHAN LOKASI FMC HINO-<?= strtoupper($invoiceData->customer->cust_name) ?></strong></h4></td>
        </tr>
        <tr>
            <td colspan="<?= isset($_SESSION['type_qty']) ? $_SESSION['type_qty']+1 : null ?>" style="border:inherit" height ="20px"></td>
        </tr>
        <tr>
            <td >Name</td>
            <td colspan="<?= isset($_SESSION['type_qty']) ? $_SESSION['type_qty']*4+2 : null ?>"><?= $invoiceData->customer->cust_name?></td>
        </tr>
        <tr>
            <td >Phone</td>
            <td colspan="<?= isset($_SESSION['type_qty']) ? $_SESSION['type_qty']*4+2 : null ?>"><?= $invoiceData->customer->phone?></td>
        </tr>
        <tr>
            <td >Invoice ID</td>
            <td colspan="<?= isset($_SESSION['type_qty']) ? $_SESSION['type_qty']*4+2 : null ?>"><?= $invoiceData->id_invoice?></td>
        </tr>
        <tr>
            <td >Invoice Date</td>
            <td colspan="<?= isset($_SESSION['type_qty']) ? $_SESSION['type_qty']*4+2 : null ?>"><?= $invoiceData->tgl?></td>
        </tr>
        <tr>
            <td colspan="<?= isset($_SESSION['type_qty']) ? $_SESSION['type_qty']+1 : null ?>" style="border:inherit" height ="20px"></td>
        </tr>
        <tr>
            <td>Model</td>
            <?php foreach($invoiceDet as $row1){ ?>
                <td colspan="2"><?= isset($row1->unit->unit_name) ? $row1->unit->unit_name : '' ?></td>
            <?php } ?>
        </tr>
    
        <tr>
            <td>Km Per Hari</td>
            <?php foreach($invoiceDet as $row2) {?>
            <td><?= number_format($row2->km_day)?></td>
            <td>Km Per Day</td>
            <?php } ?>
        </tr>
        <tr>
            <td>Hari Kerja</td>
            <?php foreach($invoiceDet as $row3) {?>
            <td><?= $row3->work_day?></td>
            <td>Day</td>
            <?php } ?>
        </tr>
        <tr>
            <td>Km Per Bulan</td>
            <?php foreach($invoiceDet as $row4) {?>
            <td><?= number_format($row4->km_day * $row4->work_day)?></td>
            <td>Km/Month</td>
            <?php } ?>
        </tr>
        <tr>
            
            <td>Km Per Tahun</td>
            <?php foreach($invoiceDet as $row5) {?>
            <td><?= number_format((($row5->km_day * $row5->work_day) * 12))?></td>
            <td>Km/year</td>
            <?php } ?>
        </tr>
        <tr>
            <td colspan="8" style="border:inherit" height ="20px"></td>
        </tr>
        <tr>
            <td>Jumlah Kendaraan</td>
            <?php foreach($invoiceDet as $row6) {?>
            <td><?= $row6->unit_qty?></td>
            <td>Unit</td>
            <?php } ?>
        </tr>
        <tr>
            <td>Internal Service</td>
            <?php foreach($invoiceDet as $row7) {?>
            <td><?= number_format($row7->partbytype->kminterval) ?></td>
            <td>Km</td>
            <?php } ?>
        </tr>
        <tr>
            <td>Total Km Selama <?= $kontrak?> Tahun</td>
            <?php foreach($invoiceDet as $row8) {?>
            <td><?= number_format((($row8->km_day * $row8->work_day) * 12)* $row8->kontrak) ?></td>
            <td>Km <?= $row8->kontrak?> Tahun</td>
            <?php } ?>
        </tr>
        <tr>
            <td colspan="<?= isset($_SESSION['type_qty']) ? $_SESSION['type_qty']*5+1 : null ?>">Note:The Price Exclude Tax(PPn 10%)</td>
        </tr>
    </table>
</div>
<div class="row">
    <div class="divider"></div>
</div>

<table width="100%">
        <tr style="margin: inherit;">
           <td align="center"> <h4><strong>BIAYA MAINTENANCE DAN REPAIR</strong></h4></td>
        </tr>
        </table>
        <br>
<div class="row">
    <table width="100%"  >
        <tr>
            <th rowspan="3">No</th>
            <th rowspan="3">Model Kendaraan</th>
        </tr>
        <tr>
            <th colspan="4" style="text-align:center">Cost Per Kilometer</th>
        </tr>
        <tr>
            <th>Part Preventive Manitenance & Periodical Repair</th>
            <th>Overhaul</th>
            <th>Labour</th>
            <th>Investment</th>
        </tr>
        <?php $c = 1; foreach($invoiceDet as $row) {?>
        <tr>
            <td><?= $c++?></td>
            <td><?= isset($row->unit->unit_name) ? $row->unit->unit_name : ''?></td>
            <td><?= ceil(($row->preventive_cost + $row->periodic_cost)/(12*$row->kontrak*($row->km_day*$row->work_day))) ?></td>
            <td>by Actual</td>
            <td><?= ceil($invoiceData->labor_cost / (12*$kontrak*($mileage)*$unit_qty))?></td>
            <td><?= ceil($invoiceData->invest_cost / (12*$kontrak*($mileage)*$unit_qty))?></td>
        </tr>
        <?php } ?>
    </table>
</div>
        <script>
            function printEvent(){
                window.print();
                window.location = '<?= Yii::app()->baseUrl?>/Cpk/default/destroyInvoice'; 
            }
        </script>
        <div class="row">
            <br>
            <button class="btn btn-primary print-btn" onclick="printEvent();">Print</button> 
        </div>
   
