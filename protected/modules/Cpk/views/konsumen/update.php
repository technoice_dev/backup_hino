<?php
/* @var $this KonsumenController */
/* @var $model Konsumen */

$this->breadcrumbs=array(
	'Konsumens'=>array('index'),
	$model->id_konsumen=>array('view','id'=>$model->id_konsumen),
	'Update',
);

$this->menu=array(
	array('label'=>'List Konsumen', 'url'=>array('index')),
	array('label'=>'Tambah Konsumen', 'url'=>array('create')),
	array('label'=>'Lihat Konsumen', 'url'=>array('view', 'id'=>$model->id_konsumen)),
	array('label'=>'Atur Konsumen', 'url'=>array('admin')),
);
?>

<h1>Update Konsumen <?php echo $model->id_konsumen; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>