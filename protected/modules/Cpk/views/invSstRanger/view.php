<?php
/* @var $this InvSstRangerController */
/* @var $model InvSstRanger */

$this->breadcrumbs=array(
	'Inv SstRangers'=>array('index'),
	$model->inv_id,
);

$this->menu=array(
	array('label'=>'List InvSstRanger', 'url'=>array('index')),
	array('label'=>'Create InvSstRanger', 'url'=>array('create')),
	array('label'=>'Update InvSstRanger', 'url'=>array('update', 'id'=>$model->inv_id)),
	array('label'=>'Delete InvSstRanger', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->inv_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage InvSstRanger', 'url'=>array('admin')),
);
?>

<h1>View Mechanic</h1>
<a href="<?= Yii::app()->baseUrl ;?>/Cpk/invSstRanger" class="btn btn-primary">Investment SstRanger List</a>
<br>
<?php
    foreach(Yii::app()->user->getFlashes() as $key => $message) {
?>
<div class="alert alert-<?=$key; ?>" role="alert">
  <?=$message; ?>
</div>
<?php } ?>
<br>
<div class="card">
    <div class="card-body">
        <table class="table">
            <tr>
                <td>Investment ID</td>
                <td><?= $model->inv_id ?></td>
            </tr>
            <tr>
                <td>Investment Item</td>
                <td><?= $model->inv_item ?></td>
            </tr>
            <tr>
                <td>Invesment Price</td>
                <td><?= $model->inv_price ?></td>
            </tr>
            <tr>
                <td>Investment Qty</td>
                <td><?= $model->inv_qty ?></td>
            </tr>
        </table>
    </div>
</div>
