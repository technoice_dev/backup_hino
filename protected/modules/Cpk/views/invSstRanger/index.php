<?php
/* @var $this InvSstRangerController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Inv SstRangers',
);

$this->menu=array(
	array('label'=>'Create InvSstRanger', 'url'=>array('create')),
	array('label'=>'Manage InvSstRanger', 'url'=>array('admin')),
);
?>


<h1>Investment SstRanger</h1>

<?php
    foreach(Yii::app()->user->getFlashes() as $key => $message) {
?>
<div class="alert alert-<?=$key; ?>" role="alert">
  <?=$message; ?>
</div>
<br>
<?php } ?>
<div class="row">
    <div class="container">
        <div class="col pull-left">
            <a class="btn btn-primary" href="<?= Yii::app()->baseUrl ;?>/Cpk/invSstRanger/create" align="right">Tambah</a>
        </div>
        <div class="col pull-right">
            <form class="form-inline" action="<?= Yii::app()->baseUrl ;?>/Cpk/invSstRanger/index" method="get">
                <div class="form-group mx-sm-3 mb-2">
                    <input type="text" class="form-control" name="search" placeholder="Cari" value="<?= $search ?>">
                </div>
                <button type="submit" class="btn btn-primary mb-2">Cari</button>
            </form>
        </div>
        <br>
    </div>
    
</div>
<br>



<div class="row">
    <div class="container">
        <table class="table table-stripped table-hover table-bordered">
            <thead>
                <tr>
                    <td style="text-align:center;">Invesment ID</td>
                    <td style="text-align:center;">Investment Item</td>
                    <td style="text-align:center;">Investment Price</td>
                    <td style="text-align:left;">Investment Qty</td>
                    <td style="text-align:center;">Action</td>
                </tr>
            </thead>
            <tbody>
                <?php foreach($data as $row) {?>
                    <tr>
                        <td style="text-align:center;"><?= $row->inv_id?></td>
                        <td><?= $row->inv_item?></td>
                        <td style="text-align:center;"><?= $row->inv_price?></td>
                        <td style="text-align:left;"><?= $row->inv_qty?></td>
                        <td style="text-align:center;"><a href="<?= Yii::app()->baseUrl ;?>/Cpk/invSstRanger/update/id/<?= $row->inv_id?>" class="btn btn-primary btn-sm">Ubah</a> 
                            <a href="<?= Yii::app()->baseUrl ;?>/Cpk/invSstRanger/delete/id/<?= $row->inv_id?>" class="btn btn-danger btn-sm">Hapus</a> 
                            <a href="<?= Yii::app()->baseUrl ;?>/Cpk/invSstRanger/view/id/<?= $row->inv_id?>" class="btn btn-info btn-sm">View</a></td>
                    </tr>
            
                <?php } ?>
            </tbody>
        </table>
        <?php $this->widget('CLinkPager', array('pages' => $pages)) ?>
    </div>
</div>


