<?php
/* @var $this MasterPartController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'User',
);


?>

<div class="row">
    <h2>Daftar User</h2>
</div>
<?php
    foreach(Yii::app()->user->getFlashes() as $key => $message) {
?>
<div class="row">
    <div class="alert alert-<?=$key; ?>" role="alert">
        <?=$message; ?>
    </div>
</div>
<br>
<?php } ?>
<div class="row">
    <div class="">
        <div class="col pull-left">
            <a class="btn btn-primary" href="<?= Yii::app()->baseUrl ;?>/Cpk/user/create" align="right">Tambah</a>
        </div>
        <div class="col pull-right">
            <form class="form-inline" action="<?= Yii::app()->baseUrl ;?>/Cpk/user/index" method="get">
                <div class="form-group mx-sm-3 mb-2">
                    <input type="text" class="form-control" name="search" placeholder="Cari" value="<?= $search ?>">
                </div>
                <button type="submit" class="btn btn-primary mb-2">Cari</button>
            </form>
        </div>
        <br>
    </div>
    
</div>
<br>

<div class="modal" id="delete-modal" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <center><h4 class="modal-title">Delete Confirmation</h4></center>
            </div>
            <div class="modal-body">
                <input type="hidden" value="" id="deleted-id">
                <center>
                    <p>Anda yakin ingin menghapus data ?</p>
                    <p>
                        <button id="delete-data" class="btn btn-danger btn-sm">Ya</button>
                        <button id="cancel-btn" data-dismiss="modal" class="btn btn-warning btn-sm">Tidak</button>
                    </p>
                </center>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="">
        <table class="table table-stripped table-hover table-bordered">
            <thead>
                <tr>
                    <?php
                        $url = isset($_GET['search'])? Yii::app()->baseUrl.'/Cpk/user?search='.$_GET['search'].'&' : Yii::app()->baseUrl.'/Cpk/user?';
                        $sortByUsername = (isset($_GET['sortBy']) ? $_GET['sortBy'] : ($_GET['sortBy'] = '')) == 'username' ?  $_GET['sortBy'] : 'username';
                        $sortByLevel = (isset($_GET['sortBy']) ? $_GET['sortBy'] : ($_GET['sortBy'] = '')) == 'level' ?  $_GET['sortBy'] : 'level';
                        $sortByEmail = (isset($_GET['sortBy']) ? $_GET['sortBy'] : ($_GET['sortBy'] = '')) == 'email' ?  $_GET['sortBy'] : 'email';
                        $sortByDealer = (isset($_GET['sortBy']) ? $_GET['sortBy'] : ($_GET['sortBy'] = '')) == 'dealer.dealer_name' ?  $_GET['sortBy'] : 'dealer.dealer_name';
                        $sortType = isset($_GET['sortType']) ? ($_GET['sortType'] == 'asc'? 'desc': 'asc') : 'asc';
                    ?>
                    <td style="text-align:center;">No</td>
                    <td style="text-align:center;"><a href="<?= $url.'sortBy='.$sortByUsername.'&sortType='.$sortType?>">Username</a></td>
                    <td><a href="<?= $url.'sortBy='.$sortByLevel.'&sortType='.$sortType?>">Level</a></td>
                    <td><a href="<?= $url.'sortBy='.$sortByEmail.'&sortType='.$sortType?>">Email</a></td>
                    <td><a href="<?= $url.'sortBy='.$sortByDealer.'&sortType='.$sortType?>">Dealer</a></td>
                    <td style="text-align:center;">Action</td>
                </tr>
            </thead>
            <tbody>
                <?php foreach($data as $key => $row) {?>
                    <tr>
                        <td style="text-align:center;"><?= ($pages->currentPage * $pages->pageSize)  + $key + 1   ?></td>
                        <td><?= $row->username?></td>
                        <td><?= $row->level?></td>
                        <td><?= $row->email?></td>
                        <td style="text-align:right;"><?= isset($row->dealer->dealer_name) ? $row->dealer->dealer_name : '' ?></td>
                        <td style="text-align:center;"><a href="<?= Yii::app()->baseUrl ;?>/Cpk/user/update/id/<?= $row->id_user?>" class="btn btn-primary btn-sm">Ubah</a> 
                            <button data-val="<?= $row->id_user?>" class="btn btn-danger btn-sm del-button">Hapus</button> 
                            <a href="<?= Yii::app()->baseUrl ;?>/Cpk/user/view/id/<?= $row->id_user?>" class="btn btn-info btn-sm">View</a></td>
                    </tr>
            
                <?php } ?>
            </tbody>
        </table>
        <?php $this->widget('CLinkPager', array('pages' => $pages)) ?>
    </div>
</div>
<script>
$('table tbody tr td .del-button').click(function(){
    var data_id = $(this).attr('data-val');
    $('#deleted-id').val(data_id);
    $('#delete-modal').modal('show');
});
$('#delete-data').click(function(){
    var delete_data = $('#deleted-id').val();
    var delete_url = '<?= Yii::app()->baseUrl ;?>/Cpk/user/delete/id/'+delete_data;
    window.location.href = delete_url;
});

</script>

