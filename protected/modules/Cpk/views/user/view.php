<?php
/* @var $this MasterPartController */
/* @var $model MasterPart */

$this->breadcrumbs=array(
	'User'=>array('index'),
	$model->id_user,
);

?>
<h1>View User</h1>
<a href="<?= Yii::app()->baseUrl ;?>/Cpk/user" class="btn btn-primary">List User</a>
<br>
<?php
    foreach(Yii::app()->user->getFlashes() as $key => $message) {
?>
<div class="alert alert-<?=$key; ?>" role="alert">
  <?=$message; ?>
</div>
<?php } ?>
<br>
<div class="card">
    <div class="card-body">
        <table class="table">
            <tr>
                <td>Username</td>
                <td><?= $model->username ?></td>
            </tr>
            <tr>
                <td>Level</td>
                <td><?= $model->level ?></td>
            </tr>
            <tr>
                <td>Email</td>
                <td><?= $model->email ?></td>
            </tr>
            <tr>
                <td>Dealer</td>
                <td><?= $model->id_dealer ?></td>
            </tr>
        </table>
    </div>
</div>
