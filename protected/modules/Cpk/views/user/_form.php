<?php
/* @var $this MasterPartController */
/* @var $model MasterPart */
/* @var $form CActiveForm */
?>
<style type="text/css">
    .invalid{
        outline-color: red;
    }
</style>
<div class="card">
    <div class="card-body">
        <div class="form">
            
                <?php $form=$this->beginWidget('CActiveForm', array(
                       'id'=>'user-form',
                        // Please note: When you enable ajax validation, make sure the corresponding
                        // controller action is handling ajax validation correctly.
                        // There is a call to performAjaxValidation() commented in generated controller code.
                        // See class documentation of CActiveForm for details on this.
                        'enableAjaxValidation'=>false,
                )); ?>

                    <?php if(Yii::app()->user->getFlash('error')){
                        echo '<div class="alert alert-danger" role="alert">';
                        echo '<button type="button" class="close" data-dismiss="alert" aria-label="Close">';
                        echo '<span aria-hidden="true">&times;</span>';
                        echo '</button>';
                        foreach(Yii::app()->user->getFlashes() as $key => $value){
                            echo $value.'</br>';
                        }
                        
                        echo '</div>';
                        
                    } ?>
                    
                    
                    
                    <?php echo $form->errorSummary($model); ?>
                    
                    <div class="form-group row">
                        <?php echo $form->labelEx($model,'Username',array('class'=>'col-sm-2 col-form-label')); ?>
                        <div class="col-sm-6">
                            <?php echo $form->textField($model,'username',array('size'=>60,'maxlength'=>100,'class'=>'form-control','id'=>'part_name')); ?>
                        </div>
                        
                    </div>

                    <div class="form-group row">
                        <?php echo $form->labelEx($model,'Password',array('class'=>'col-sm-2 col-form-label')); ?>
                        <div class="col-sm-6">
                            <?php echo $form->passwordField($model,'password',array('size'=>20,'maxlength'=>20,'class'=>'form-control','id'=>'part_no')); ?>
                        </div>
                    </div>

                    <div class="form-group row">
                        <?php echo $form->labelEx($model,'Level',array('class'=>'col-sm-2 col-form-label')); ?>
                        <div class="col-sm-6">
                            <?php echo $form->dropDownList($model,'level',array('super'=>'Super','dealer'=>'Dealer','sales'=>'Sales'),array('class'=>'form-control','id'=>'pricelist')); ?>
                        </div>
                    </div>

                    <div class="form-group row">
                        <?php echo $form->labelEx($model,'Email',array('class'=>'col-sm-2 col-form-label')); ?>
                        <div class="col-sm-6">
                            <?php echo $form->emailField($model,'email',array('class'=>'form-control','id'=>'km1th')); ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <?php echo $form->labelEx($model,'Dealer',array('class'=>'col-sm-2 col-form-label')); ?>
                        <div class="col-sm-6">
                            <?php echo $form->dropDownList($model,'id_dealer',$optionDealer,array('class'=>'form-control','id'=>'id_dealer')); ?>
                        </div>
                    </div>
                    <br>    
                    <div class="form-group row buttons">
                        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class'=>'btn btn-primary')); ?>
                    </div>

                <?php $this->endWidget(); ?>
            
        </div><!-- form -->
    </div>
</div>
