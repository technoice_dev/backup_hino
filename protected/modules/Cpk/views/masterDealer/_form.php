
<?php
/* @var $this MasterPartController */
/* @var $model MasterPart */
/* @var $form CActiveForm */
?>
<style type="text/css">
    .invalid{
        outline-color: red;
    }
</style>
<div class="card">
    <div class="card-body">
        <div class="form">
            
                <?php $form=$this->beginWidget('CActiveForm', array(
                       'id'=>'master-part-form',
                        // Please note: When you enable ajax validation, make sure the corresponding
                        // controller action is handling ajax validation correctly.
                        // There is a call to performAjaxValidation() commented in generated controller code.
                        // See class documentation of CActiveForm for details on this.
                        'enableAjaxValidation'=>false,
                )); ?>

                    <?php if(Yii::app()->user->getFlash('error')){
                        echo '<div class="alert alert-danger" role="alert">';
                        echo '<button type="button" class="close" data-dismiss="alert" aria-label="Close">';
                        echo '<span aria-hidden="true">&times;</span>';
                        echo '</button>';
                        foreach(Yii::app()->user->getFlashes() as $key => $value){
                            echo $value.'</br>';
                        }
                        
                        echo '</div>';
                        
                    } ?>
                    
                    
                    
                    <?php echo $form->errorSummary($model); ?>
                    
                    <div class="form-group row">
                        <?php echo $form->labelEx($model,'Dealer ID',array('class'=>'col-sm-2 col-form-label')); ?>
                        <div class="col-sm-6">
                            <?php echo $form->textField($model,'dealer_id',array('size'=>60,'maxlength'=>100,'class'=>'form-control','id'=>'part_name')); ?>
                        </div>
                        
                    </div>

                    <div class="form-group row">
                        <?php echo $form->labelEx($model,'Dealer Name',array('class'=>'col-sm-2 col-form-label')); ?>
                        <div class="col-sm-6">
                            <?php echo $form->textField($model,'dealer_name',array('size'=>20,'maxlength'=>20,'class'=>'form-control','id'=>'part_no')); ?>
                        </div>
                    </div>

                    <div class="form-group row">
                        <?php echo $form->labelEx($model,'ID Cabang',array('class'=>'col-sm-2 col-form-label')); ?>
                        <div class="col-sm-6">
                            <?php echo $form->textField($model,'cabang_id',array('class'=>'form-control','id'=>'pricelist')); ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <?php echo $form->labelEx($model,'Facility',array('class'=>'col-sm-2 col-form-label')); ?>
                        <div class="col-sm-6">
                            <?php echo $form->textField($model,'facility',array('class'=>'form-control','id'=>'km1th')); ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <?php echo $form->labelEx($model,'Dealer Type',array('class'=>'col-sm-2 col-form-label')); ?>
                        <div class="col-sm-6">
                            <?php echo $form->textField($model,'dealer_type',array('class'=>'form-control')); ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <?php echo $form->labelEx($model,'Serial Code',array('class'=>'col-sm-2 col-form-label')); ?>
                        <div class="col-sm-6">
                            <?php echo $form->textField($model,'serial_code',array('class'=>'form-control')); ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <?php echo $form->labelEx($model,'Main Dealer',array('class'=>'col-sm-2 col-form-label')); ?>
                        <div class="col-sm-6">
                            <?php echo $form->textField($model,'main_dealer',array('class'=>'form-control')); ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <?php echo $form->labelEx($model,'Region ID',array('class'=>'col-sm-2 col-form-label')); ?>
                        <div class="col-sm-6">
                            <?php echo $form->textField($model,'region_id',array('class'=>'form-control')); ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <?php echo $form->labelEx($model,'Address',array('class'=>'col-sm-2 col-form-label')); ?>
                        <div class="col-sm-6">
                            <?php echo $form->textArea($model,'address',array('class'=>'form-control')); ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <?php echo $form->labelEx($model,'No Phone',array('class'=>'col-sm-2 col-form-label')); ?>
                        <div class="col-sm-6">
                            <?php echo $form->textField($model,'no_phone',array('class'=>'form-control')); ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <?php echo $form->labelEx($model,'Fax',array('class'=>'col-sm-2 col-form-label')); ?>
                        <div class="col-sm-6">
                            <?php echo $form->textField($model,'fax',array('class'=>'form-control')); ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <?php echo $form->labelEx($model,'Establishment Date',array('class'=>'col-sm-2 col-form-label')); ?>
                        <div class="col-sm-6">
                            <?php //echo $form->textField($model,'establishment_date',array('class'=>'form-control'));
                                    $this->widget('zii.widgets.jui.CJuiDatePicker',array(
                                    'model'=>$model,
                                    'attribute'=>'establishment_date',
                                    'value' => $model->establishment_date,
                                    'options'=>array(
                                        'dateFormat' => 'yy-mm-dd',
                                        'showAnim'=>'slideDown',
                                    ),
                                    'htmlOptions'=>array(
                                        'class'=>'form-control',
                                        'size'=>'25',
                                        'style'=>''
                                    ),
                                ));
                            ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <?php echo $form->labelEx($model,'Outlet Name',array('class'=>'col-sm-2 col-form-label')); ?>
                        <div class="col-sm-6">
                            <?php echo $form->textField($model,'outlet_name',array('class'=>'form-control')); ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <?php echo $form->labelEx($model,'Sales Prefix',array('class'=>'col-sm-2 col-form-label')); ?>
                        <div class="col-sm-6">
                            <?php echo $form->textField($model,'sales_prefix',array('class'=>'form-control')); ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <?php echo $form->labelEx($model,'Website',array('class'=>'col-sm-2 col-form-label')); ?>
                        <div class="col-sm-6">
                            <?php echo $form->textField($model,'website',array('class'=>'form-control')); ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <?php echo $form->labelEx($model,'Region ID HCS',array('class'=>'col-sm-2 col-form-label')); ?>
                        <div class="col-sm-6">
                            <?php echo $form->textField($model,'region_id_hcs',array('class'=>'form-control')); ?>
                        </div>
                    </div>
                    <br>    
                    <div class="form-group row buttons">
                        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class'=>'btn btn-primary')); ?>
                    </div>

                <?php $this->endWidget(); ?>
            
        </div><!-- form -->
    </div>
</div>
