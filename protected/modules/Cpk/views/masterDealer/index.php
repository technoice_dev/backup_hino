<?php
/* @var $this MasterPartController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Master Dealer',
);


?>

<div class="row">
    <h2>Daftar Dealer</h2>
</div>
<?php
    foreach(Yii::app()->user->getFlashes() as $key => $message) {
?>
<div class="row">
    <div class="alert alert-<?=$key; ?>" role="alert">
        <?=$message; ?>
    </div>
</div>
<br>
<?php } ?>
<div class="row">
    <div class="">
        <div class="col pull-left">
            <a class="btn btn-primary" href="<?= Yii::app()->baseUrl ;?>/Cpk/masterDealer/create" align="right">Tambah</a>
        </div>
        <div class="col pull-right">
            <form class="form-inline" action="<?= Yii::app()->baseUrl ;?>/Cpk/masterDealer/index" method="get">
                <div class="form-group mx-sm-3 mb-2">
                    <input type="text" class="form-control" name="search" placeholder="Cari" value="<?= $search ?>">
                </div>
                <button type="submit" class="btn btn-primary mb-2">Cari</button>
            </form>
        </div>
        <br>
    </div>
</div>
<br>
<div class="modal" id="delete-modal" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <center><h4 class="modal-title">Delete Confirmation</h4></center>
            </div>
            <div class="modal-body">
                <input type="hidden" value="" id="deleted-id">
                <center>
                    <p>Anda yakin ingin menghapus Manpower ?</p>
                    <p>
                        <button id="delete-data" class="btn btn-danger btn-sm">Ya</button>
                        <button id="cancel-btn" data-dismiss="modal" class="btn btn-warning btn-sm">Tidak</button>
                    </p>
                </center>
            </div>
        </div>
    </div>
</div>


<div class="modal" id="delete-modal" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <center><h4 class="modal-title">Delete Confirmation</h4></center>
            </div>
            <div class="modal-body">
                <input type="hidden" value="" id="deleted-id">
                <center>
                    <p>Anda yakin ingin menghapus data ?</p>
                    <p>
                        <button id="delete-data" class="btn btn-danger btn-sm">Ya</button>
                        <button id="cancel-btn" class="btn btn-warning btn-sm">Tidak</button>
                    </p>
                </center>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="table-responsive">
        <table class="table table-stripped table-hover table-bordered">
            <thead>
                <tr>
                <?php
                    $url = isset($_GET['search'])? Yii::app()->baseUrl.'/Cpk/masterDealer?search='.$_GET['search'].'&' : Yii::app()->baseUrl.'/Cpk/masterDealer?';
                    $sortByDealerName = (isset($_GET['sortBy']) ? $_GET['sortBy'] : ($_GET['sortBy'] = '')) == 'dealer_name' ?  $_GET['sortBy'] : 'dealer_name';
                    $sortByCabang = (isset($_GET['sortBy']) ? $_GET['sortBy'] : ($_GET['sortBy'] = '')) == 'cabang_id' ?  $_GET['sortBy'] : 'cabang_id';
                    $sortByFacility = (isset($_GET['sortBy']) ? $_GET['sortBy'] : ($_GET['sortBy'] = '')) == 'facility' ?  $_GET['sortBy'] : 'facility';
                    $sortByDealerType = (isset($_GET['sortBy']) ? $_GET['sortBy'] : ($_GET['sortBy'] = '')) == 'dealer_type' ?  $_GET['sortBy'] : 'dealer_type';
                    $sortBySerialCode = (isset($_GET['sortBy']) ? $_GET['sortBy'] : ($_GET['sortBy'] = '')) == 'serial_code' ?  $_GET['sortBy'] : 'serial_code';
                    $sortByRegion = (isset($_GET['sortBy']) ? $_GET['sortBy'] : ($_GET['sortBy'] = '')) == 'region_id' ?  $_GET['sortBy'] : 'region_id';
                    $sortByFax = (isset($_GET['sortBy']) ? $_GET['sortBy'] : ($_GET['sortBy'] = '')) == 'fax' ?  $_GET['sortBy'] : 'fax';
                    $sortByNoTelp = (isset($_GET['sortBy']) ? $_GET['sortBy'] : ($_GET['sortBy'] = '')) == 'no_phone' ?  $_GET['sortBy'] : 'no_phone';
                    $sortByEstablishedDate = (isset($_GET['sortBy']) ? $_GET['sortBy'] : ($_GET['sortBy'] = '')) == 'establishment_date' ?  $_GET['sortBy'] : 'establishment_date';
                    $sortByOutletName = (isset($_GET['sortBy']) ? $_GET['sortBy'] : ($_GET['sortBy'] = '')) == 'outlet_name' ?  $_GET['sortBy'] : 'sales_prefix';
                    $sortBySalesPrefix = (isset($_GET['sortBy']) ? $_GET['sortBy'] : ($_GET['sortBy'] = '')) == 'sales_prefix' ?  $_GET['sortBy'] : 'sales_prefix';
                    $sortByWebsite = (isset($_GET['sortBy']) ? $_GET['sortBy'] : ($_GET['sortBy'] = '')) == 'website' ?  $_GET['sortBy'] : 'website';
                    $sortType = isset($_GET['sortType']) ? ($_GET['sortType'] == 'asc'? 'desc': 'asc') : 'asc';
                ?>
                    <td style="text-align:center;">No</td>
                    <td><a href="<?= $url.'sortBy='.$sortByDealerName.'&sortType='.$sortType?>">Dealer Name</a></td>
                    <td><a href="<?= $url.'sortBy='.$sortByCabang.'&sortType='.$sortType?>">Cabang ID</a></td>
                    <td style="text-align:right;"><a href="<?= $url.'sortBy='.$sortByFacility.'&sortType='.$sortType?>">Facility</a></td>
                    <td style="text-align:right;"><a href="<?= $url.'sortBy='.$sortByDealerType.'&sortType='.$sortType?>">Dealer Type</a></td>
                    <td><a href="<?= $url.'sortBy='.$sortBySerialCode.'&sortType='.$sortType?>">Serial Code</a></td>
                    <td><a href="<?= $url.'sortBy='.$sortByRegion.'&sortType='.$sortType?>">Region</a></td>
                    <td><a href="<?= $url.'sortBy='.$sortByFax.'&sortType='.$sortType?>">Fax</a></td>
                    <td><a href="<?= $url.'sortBy='.$sortByNoTelp.'&sortType='.$sortType?>">No Telp</a></td>
                    <td>Fax</td>
                    <td><a href="<?= $url.'sortBy='.$sortByEstablishedDate.'&sortType='.$sortType?>">Established Date</a></td>
                    <td><a href="<?= $url.'sortBy='.$sortByOutletName.'&sortType='.$sortType?>">Outlet Name</a></td>
                    <td><a href="<?= $url.'sortBy='.$sortBySalesPrefix.'&sortType='.$sortType?>">Sales Prefik</a></td>
                    <td><a href="<?= $url.'sortBy='.$sortByWebsite.'&sortType='.$sortType?>">Website</a></td>
                    <td>Region</td>
                    <td style="text-align:center;">Action</td>
                </tr>
            </thead>
            <tbody>
                <?php foreach($data as $key => $row) {?>
                    <tr>
                        <td style="text-align:center;"><?= ($pages->currentPage * $pages->pageSize)  + $key + 1  ?></td>
                        <td><?= $row->dealer_name?></td>
                        <td><?= $row->cabang_id?></td>
                        <td style="text-align:right;"><?= $row->facility?></td>
                        <td style="text-align:right;"><?= $row->dealer_type?></td>
                        <td><?= $row->serial_code?></td>
                        <td><?= $row->region_id?></td>
                        <td><?= $row->main_dealer?></td>
                        <td><?= $row->no_phone?></td>
                        <td><?= $row->fax?></td>
                        <td><?= $row->establishment_date?></td>
                        <td><?= $row->outlet_name?></td>
                        <td><?= $row->sales_prefix?></td>
                        <td><?= $row->website?></td>
                        <td><?= $row->region_id_hcs?></td>
                        <td style="text-align:center;"><a href="<?= Yii::app()->baseUrl ;?>/Cpk/masterDealer/update/id/<?= $row->id?>" class="btn btn-primary btn-xs">Ubah</a> 
                            <button data-val="<?= $row->id?>" class="btn btn-danger btn-sm del-button">Hapus</button> 
                            <a href="<?= Yii::app()->baseUrl ;?>/Cpk/masterDealer/view/id/<?= $row->id?>" class="btn btn-info btn-xs">View</a></td>
                    </tr>
            
                <?php } ?>
            </tbody>
        </table>
        <?php $this->widget('CLinkPager', array('pages' => $pages)) ?>
    </div>
</div>
<script>
$('table tbody tr td .del-button').click(function(){
    var data_id = $(this).attr('data-val');
    $('#deleted-id').val(data_id);
    $('#delete-modal').modal('show');
});
$('#delete-data').click(function(){
    var delete_data = $('#deleted-id').val();
    var delete_url = '<?= Yii::app()->baseUrl ;?>/Cpk/masterDealer/delete/id/'+delete_data;
    window.location.href = delete_url;
});

</script>

