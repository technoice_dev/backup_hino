<?php
/* @var $this MasterDealerController */
/* @var $model MasterDealer */

$this->breadcrumbs=array(
	'Master Dealers'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List MasterDealer', 'url'=>array('index')),
	array('label'=>'Create MasterDealer', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#master-dealer-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Master Dealers</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'master-dealer-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'dealer_id',
		'dealer_name',
		'cabang_id',
		'facility',
		'dealer_type',
		/*
		'serial_code',
		'main_dealer',
		'region_id',
		'address',
		'no_phone',
		'fax',
		'establishment_date',
		'outlet_name',
		'sales_prefix',
		'website',
		'region_id_hcs',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
