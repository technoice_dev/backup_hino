<?php
/* @var $this MasterPartController */
/* @var $model MasterPart */

$this->breadcrumbs=array(
	'Master Dealer'=>array('index'),
	$model->id,
);

?>
<h1>View Dealer</h1>
<a href="<?= Yii::app()->baseUrl ;?>/Cpk/masterDealer" class="btn btn-primary">List Dealer</a>
<br>
<?php
    foreach(Yii::app()->user->getFlashes() as $key => $message) {
?>
<div class="alert alert-<?=$key; ?>" role="alert">
  <?=$message; ?>
</div>
<?php } ?>
<br>
<div class="card">
    <div class="card-body">
        <table class="table">
            <tr>
                <td>Dealer ID</td>
                <td><?= $model->dealer_id ?></td>
            </tr>
            <tr>
                <td>Dealer Name</td>
                <td><?= $model->dealer_name ?></td>
            </tr>
            <tr>
                <td>Cabang ID</td>
                <td><?= $model->cabang_id ?></td>
            </tr>
            <tr>
                <td>Facility</td>
                <td><?= $model->facility ?></td>
            </tr>
            <tr>
                <td>Dealer Type</td>
                <td><?= $model->dealer_type ?></td>
            </tr>
            <tr>
                <td>Serial Code</td>
                <td><?= $model->serial_code ?></td>
            </tr>
            <tr>
                <td>Main Dealer</td>
                <td><?= $model->main_dealer ?></td>
            </tr>
            <tr>
                <td>Region ID</td>
                <td><?= $model->region_id ?></td>
            </tr>
            <tr>
                <td>Address</td>
                <td><?= $model->address ?></td>
            </tr>
            <tr>
                <td>No Phone</td>
                <td><?= $model->no_phone ?></td>
            </tr>
            <tr>
                <td>Fax</td>
                <td><?= $model->fax ?></td>
            </tr>
            <tr>
                <td>Dealer Type</td>
                <td><?= $model->dealer_type ?></td>
            </tr>
            <tr>
                <td>Establishment Date</td>
                <td><?= $model->dealer_type ?></td>
            </tr>
            <tr>
                <td>Outlet Name</td>
                <td><?= $model->outlet_name ?></td>
            </tr>
            <tr>
                <td>Sales Prefix</td>
                <td><?= $model->sales_prefix ?></td>
            </tr>
            <tr>
                <td>Website</td>
                <td><?= $model->website ?></td>
            </tr>
            <tr>
                <td>Region ID HCS</td>
                <td><?= $model->region_id_hcs ?></td>
            </tr>
            
        </table>
    </div>
</div>
