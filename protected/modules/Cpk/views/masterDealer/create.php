<?php
/* @var $this MasterDealerController */
/* @var $model MasterDealer */

$this->breadcrumbs=array(
	'Master Dealers'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List MasterDealer', 'url'=>array('index')),
	array('label'=>'Manage MasterDealer', 'url'=>array('admin')),
);
?>

<h1>Create MasterDealer</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>