<?php
/* @var $this InvOthersController */
/* @var $model InvOthers */

$this->breadcrumbs=array(
	'Inv Others'=>array('index'),
	$model->inv_id=>array('view','id'=>$model->inv_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List InvOthers', 'url'=>array('index')),
	array('label'=>'Create InvOthers', 'url'=>array('create')),
	array('label'=>'View InvOthers', 'url'=>array('view', 'id'=>$model->inv_id)),
	array('label'=>'Manage InvOthers', 'url'=>array('admin')),
);
?>

<h1>Update InvOthers <?php echo $model->inv_id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>